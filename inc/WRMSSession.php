<?php
/**
* Session handling class and associated functions
*
* This subpackage provides some functions that are useful around web
* application session management.
*
* The class is intended to be as lightweight as possible while holding
* all session data in the database:
*  - Session hash is not predictable.
*  - No clear text information is held in cookies.
*  - Passwords are generally salted SHA1 hashes, but individual users may
*    have plain text passwords set by an administrator.
*  - Temporary passwords are supported.
*  - Logout is supported
*  - "Remember me" cookies are supported, and will result in a new
*    Session for each browser session.
*
* @package   WRMS
* @subpackage   WRMSSession
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst .Net Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/

/**
* All session data is held in the database.
*/
require_once('PgQuery.php');
require_once('AccessControl.php');

/**
* @global resource $session
* @name $session
* The session object is global.
*/
$session = 1;  // Fake initialisation

// The Session object uses some (optional) configurable SQL to load
// the records related to the logged-on user...  (the where clause gets added).
// It's very important that someone not be able to externally control this,
// so we make it a function rather than a variable.
/**
* @todo Make this a defined constant
*/
function local_session_sql() {
  $sql = <<<EOSQL
SELECT session.*, usr.*, organisation.*
        FROM session JOIN usr USING(user_no) LEFT JOIN organisation USING (org_code)
EOSQL;
  return $sql;
}

/**
* We extend the AWL Session class.
*/
require_once('Session.php');

Session::_CheckLogout();

/**
* A class for creating and holding session information.
*
* @package   wrms
*/
class WRMSSession extends Session
{

  public $acl = false;
  public $unstructured_data = null;
  public $safe_mode = false;


  /**
  * Create a new WRMSSession object.
  *
  * We create a Session and extend it with some additional useful WRMS
  * related information.
  *
  * @param string $sid A session identifier.
  */
  function WRMSSession( $sid="" ) {
    $this->Session($sid);
    $this->config_data = '';

    if (!$this->acl) $this->acl = AccessControl::from_roles(get_role_permissions(), array("Guest"));

    // Check for cross-site request
    $req_prefix = '~^https?://' . preg_quote($_SERVER['HTTP_HOST'], '~') . '/~';
    if (strtoupper($_SERVER["REQUEST_METHOD"] != 'GET') && (!$_SERVER['HTTP_REFERER'] || !preg_match($req_prefix, $_SERVER['HTTP_REFERER'])) && !isset($_SERVER['HTTP_X_NON_BROWSER'])) {
      // Suspected cross-site request forgery POST - use guest acl
      $this->safe_mode = true;
    }
  }


  /**
  * Checks whether a user is allowed to do something.
  *
  * @param string $whatever The role we want to know if the user has.
  * @param string second role ...
  * @return boolean Whether or not the user has all specified roles.
  */
  function AllowedTo () {
    if ( ! $this->logged_in ) return false;

    $actions = func_get_args();
    foreach ($actions as $action) {
      /** Centralised recognition of more specific permissions */
      switch( $action ) {
        case 'see_other_orgs':
          if (! ((isset($this->roles['Support']) && $this->roles['Support']) || (isset($this->roles['Admin']) && $this->roles['Admin']) )) return false;
          break;

        default:
          /** Fall back to older style of looking at the roles directly. */
          if (! (isset($this->roles[$action]) && $this->roles[$action] ) ) return false;
      }
    }

    return true;
  }

  /**
  * Checks whether a user can do any of a range of things.
  *
  * @param string $whatever The role we want to know if the user has.
  * @param string second role ...
  * @return boolean Whether or not the user has any of the specified roles.
  */
  function AllowedAny() {
    $actions = func_get_args();
    foreach ($actions as $action) {
      if ($this->AllowedTo($action)) return true;
    }
    return false;
  }

  // Return the acl to be used by this session for this request.
  function getACL() {
    if ($this->safe_mode) return AccessControl::from_roles(get_role_permissions(), array("Guest"));

    return $this->acl;
  }

  function can() {
    $acl = $this->getACL();

    $actions = func_get_args();
    if (count($actions) == 1 and is_array($actions[0])) $actions = $actions[0];
    foreach ($actions as $action) {
      if (!$acl->allowed($action)) return false;
    }
    return true;
  }

  function can_userdata($action, $userdata) {
    return $this->getACL()->allowed($action, $userdata);
  }

  /**
  * Checks whether this user is an admin
  *
  * @return boolean Whether or not the logged in user is a banker
  */
  function IsAdmin() {
    return ( $this->logged_in && $this->AllowedTo("Admin") );
  }

  /**
   * Overridden login function to check that the user is using the strongest
   * password hash format that we support
   *
   * @override
   * @see awl/inc/Session#Login($username, $password, $authenticated)
   */
  function Login( $username, $password, $authenticated = false ) {
      $res = parent::Login($username, $password, $authenticated);

      if ( !$authenticated && $this->just_logged_in && !$this->login_failed ) {
          $sql = "SELECT user_no, password FROM usr WHERE lower(username) = ? AND active";
          $qry = new PgQuery($sql, strtolower($username));
          $qry->Exec( 'LoginHashUpgrade', __LINE__, __FILE__ );
          if ($qry->rows == 1) {
              $usr = $qry->Fetch(true);
              $hash_needs_upgrade = false;

              // Detect the password hash format
              if ( preg_match('/^\*\*.+$/', $usr['password']) ) {
                  // plaintext password, definitely need to upgrade
                  $hash_needs_upgrade = true;
              }
              else if ( preg_match('/^\*(.+)\*{[A-Z]+}.+$/', $usr['password']) ) {
                  // already using SSHA hash - this is the most secure at the moment. do nothing.
              }
              else if ( preg_match('/^\*(.+)\*.+$/', $usr['password']) ) {
                  // md5 password, we'll upgrade if we can do SSHA
                  if ( function_exists("session_salted_sha1") ) {
                      $hash_needs_upgrade = true;
                  }
              }
          }

          if ( $hash_needs_upgrade ) {
              // re-store their hash using the strongest hash we have
              $newhash = (function_exists("session_salted_sha1")
                   ? session_salted_sha1($password)
                   : (function_exists('session_salted_md5')
                       ? session_salted_md5($password)
                       : md5($password)
                     )
                 );

              // Quickly test this new hash to make sure it will work (safety)
              if (! session_validate_password( $password, $newhash ) ) {
                  // Some failure in the hash algorithm...
                  dbg_error_log( "LoginHashUpgrade", "LoginHashUpgrade: Hash failed on loopback test!" );
                  return $res;
              }

              $sql = "UPDATE usr SET password = ? WHERE user_no = ?";
              $qry = new PgQuery( $sql, $newhash, $usr['user_no'] );
              if ( $qry->Exec( 'LoginHashUpgrade', __LINE__, __FILE__ ) ) {
                  dbg_error_log( "LoginHashUpgrade", "LoginHashUpgrade: Successfully upgraded hash format for $username" );
              }
          }
      }

      return $res;
  }


  /**
  * Returns a value for user_no which is within the legal values for this user,
  * using a POST value or a GET value if available and allowed, otherwise using
  * this user's value.
  *
  * @return int The sanitised value of user_no
  */
  function SanitisedUserNo( ) {
    $user_no = 0;
    if ( ! $this->logged_in ) return $user_no;

    $user_no = intval(isset($_POST['user_no']) ? $_POST['user_no'] : $_GET['user_no'] );
    if ( ! ($this->AllowedTo("Support") || $this->AllowedTo("Admin")) ) {
      $user_no = $this->user_no;
    }
    if ( $user_no == 0 ) $user_no = $this->user_no;
    return $user_no;
  }


/**
* Internal function used to get the user's system roles from the database.
*/
  function GetSystemRoles() {
    $this->system_roles = array();
    $this->system_codes = array();
    $qry = new PgQuery( 'SELECT system_usr.system_id, role, system_code FROM system_usr JOIN work_system USING (system_id) WHERE user_no = ? ', $this->user_no );
    if ( $qry->Exec('Session::GetRoles') && $qry->rows > 0 )
    {
      while( $role = $qry->Fetch() )
      {
        $this->system_roles[$role->system_id] = $role->role;
        $this->system_codes[$role->system_id] = $role->system_code;
      }
    }
  }


/**
* Internal function used to assign the session details to a user's new session.
* @param object $u The user+session object we (probably) read from the database.
*/
  function AssignSessionDetails( $u ) {
    parent::AssignSessionDetails( $u );
    $this->GetSystemRoles();
    if (intval($this->user_no) > 0) {
      if ($this->just_logged_in) {
        $this->LoadACL();
      } else {
        $acl = json_decode($this->acl, true);
        $this->acl = new AccessControl($acl);
      }
    } else {
      $this->acl = new AccessControl(array());
    }
  }

/**
* Checks that this user is logged in, and presents a login screen if they aren't.
*
* The function can optionally confirm whether they are a member of one of a list
* of groups, and deny access if they are not a member of any of them.
*
* @param string $groups The list of groups that the user must be a member of one of to be allowed to proceed.
* @return boolean Whether or not the user is logged in and is a member of one of the required groups.
*/
  function LoginRequired( $groups = "" ) {
    global $c, $session, $main_menu, $sub_menu, $tab_menu;

    if ( $this->logged_in && $groups == "" ) return;
    if ( ! $this->logged_in ) {
      $c->messages[] = "You must log in to use this system.";
      $c->page_title = "Log in please";
      include_once("page-header.php");
      if ( function_exists("local_index_not_logged_in") ) {
        local_index_not_logged_in();
      }
      else {
        echo <<<EOHTML
<h1>Log On Please</h1>
<p>For access to the $c->system_name you should log on with
the username and password that have been issued to you.</p>

<p>If you would like to request access, please e-mail $c->admin_email.</p>
EOHTML;
        echo $this->RenderLoginPanel();
      }
    }
    else {
      $valid_groups = split(",", $groups);
      foreach( $valid_groups AS $k => $v ) {
        if ( $this->AllowedTo($v) ) return;
      }
      $c->messages[] = "You are not authorised to use this function.";
      include_once("page-header.php");
    }

    include("page-footer.php");
    exit;
  }

  function LoadACL() {
    $roles = array_keys($this->roles);

    $this->acl = AccessControl::from_roles(get_role_permissions(), array_merge($roles, array("Guest", "User", "User_" . $this->username)));

    $qry = new PgQuery( 'UPDATE session SET acl = ? WHERE session_id = ?', json_encode($this->acl->get_permissions_list()), $this->session_id );
    $qry->exec();
  }

  function SetUnstructuredData($id, $data) {
    // id is ignored - store in this session
    $qry = new PgQuery( 'UPDATE session SET unstructured_data = ? WHERE session_id = ? ', $data, $this->session_id );
    if ( $qry->Exec('Session::SaveUnstructured')) {
      $this->unstructured_data = $data;
      return true;
    } else {
      return false;
    }
  }

  function GetUnstructuredData($id) {
    // id is ignored - get from this session
    return $this->unstructured_data;
  }

  function DestroyUnstructuredData($id) {
    // id is ignored - clear this session's unstructured data
    $this->unstructured_data = null;
    return true;
  }
}

$session = new WRMSSession();
$session->_CheckLogin();

/*
* Mapping between WRMS session table and PHP-style sessions.
*
* Requires session auto-start to be off.
*/

function wrms_session_nop() {
  return true;
}

if ( !isset($session->org_code) ) $session->org_code = null;

session_set_save_handler("wrms_session_nop", "wrms_session_nop", array($session, "GetUnstructuredData"), array($session, "SetUnstructuredData"), array($session, "DestroyUnstructuredData"), "wrms_session_nop");

session_start();

