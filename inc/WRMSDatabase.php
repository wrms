<?php

require_once('lazyquery.php');

/**
* Convert an interval string from PostgreSQL to a float of hours.
*
* This takes an interval expressed in HH:MM:SS form and converts it
* to a number of hours as a floating point decimal.
*/
function pg_interval_to_hours($d) {
  list($hours, $minutes, $seconds) = explode(':', $d);
  $minutes += $seconds / 60.0;
  $hours += $minutes / 60.0;
  return $hours;
}

/**
* This is a wrapper for a PDO database designed to simplify data access.
*
* It integrates automatic permissions checking for simple accesses.
*/
class WRMSDatabase {

  private $_db;

  function __construct($db) {
    $this->_db = $db;
    $this->warn_time = 1;
  }

  /**
  * This provides a series of callable methods with prefixes 'get_', 'add_',
  * 'update_' and 'delete_' for each table. These wrap the get, set, insert
  * and delete methods, using the content after the prefix as a table name.
  */
  function __call($method, $args) {
    $actions = array("add", "update", "delete", "get");
    foreach ($actions as $action) {
      $prefix = $action . '_';
      if (strncmp($prefix, $method, strlen($prefix)) == 0) {
        $type = substr($method, strlen($prefix));
        array_unshift($args, $type);
        return call_user_func_array(array($this, $action), $args);
      }
    }
    return call_user_func_array(array($this->_db, $method), $args);
  }

  /**
  * Attempts to roll back the current transaction.
  *
  * Ignores errors such as not being in a transaction.
  */
  function attemptRollBack() {
    try {
      $this->_db->rollBack();
    } catch (Exception $nya) {
      # Do nothing
    }
  }

  /**
  * This creates a LazyQuery backed by this database with the given initial table.
  *
  * Checks the permission db/{table}/get unless force is set to true.
  * @param string $table The name of the table to get records from.
  * @param boolean $force If set to true this skips permission checks.
  * @returns LazyQuery
  */
  function get($table, $force = false, $conditions = array()) {
    if (!$force) assert_permitted_userdata("db/$table/get", $conditions);
    $q = new LazyQuery($this->_db, $table);
    foreach ($conditions as $attr => $val) {
      if (is_numeric($attr)) {
        $q = $q->filter($val);
      } else {
        $q = $q->filter_eq($attr, $val);
      }
    }
    return $q;
  }

  /**
  * This updates records meeting a set of conditions with a new set of values.
  *
  * Conditions and updates are both provided as an array. Entries with string
  * keys will treat the key as a field name and the value as the value of that
  * field, which will be escaped unless parameter $escape is set to false. Entries
  * with a numeric key (i.e. given without an explicit key) will be treated as
  * a raw SQL condition or assignment.
  *
  * Checks the permission db/{table}/update unless force is set to true.
  * An array of userdata is provided to the permission check, including
  * the value conditions prefixed with 'old_' and the value updates prefixed
  * with 'new_'.
  * @param string $table The name of the table to update records in.
  * @param array $conditions The conditions identifying the rows to be updated.
  * @param array $updates The changes to be made to these rows.
  * @param boolean $force If set to true this skips permission checks.
  * @param boolean $escape If set to false this avoids automatic escaping of values.
  */
  function update($table, $conditions, $updates, $force = false, $escape = true) {
    if (!$force) {
      $attrs = array();
      foreach ($conditions as $col => $val) if (!is_numeric($col)) $attrs["old_$col"] = $val;
      foreach ($updates as $col => $val) if (!is_numeric($col)) $attrs["new_$col"] = $val;
      assert_permitted_userdata("db/$table/update", $attrs);
    }

    $query = "UPDATE $table SET ";
    $set_pieces = array();
    foreach ($updates as $field => $value) {
      if (is_numeric($field)) {
        $set_pieces[] = "$value";
      } else {
        if ($escape) $value = $this->quote($value);
        $set_pieces[] = "$field = $value";
      }
    }
    $query .= join(', ', $set_pieces);

    if (count($conditions) > 0) {
      $query .= " WHERE ";
      $cond_pieces = array();
      foreach ($conditions as $field => $value) {
        if (is_numeric($field)) {
          $cond_pieces[] = "($value)";
        } else {
          if ($escape) $value = $this->quote($value);
          $cond_pieces[] = "($field = $value)";
        }
      }
      $query .= join(' AND ', $cond_pieces);
    }

    return $this->exec($query);
  }

  /**
  * This deletes records meeting a set of conditions.
  *
  * Conditions are provided as an array. Entries with string keys will treat the
  * key as a field name and the value as the value of that field, which will be
  * escaped unless parameter $escape is set to false. Entries with a numeric key
  * (i.e. given without an explicit key) will be treated as a raw SQL condition.
  *
  * Checks the permission db/{table}/delete unless force is set to true.
  * The provided conditions are passed to this permissions check as userdata.
  * @param string $table The name of the table to delete records from.
  * @param array $conditions The conditions identifying the rows to be updated.
  * @param boolean $force If set to true this skips permission checks.
  * @param boolean $escape If set to false this avoids automatic escaping of values.
  */
  function delete($table, $conditions, $force = false, $escape = true) {
    if (!$force) {
      assert_permitted_userdata("db/$table/delete", $conditions);
    }

    $query = "DELETE FROM $table";

    if (count($conditions) > 0) {
      $query .= " WHERE ";
      $cond_pieces = array();
      foreach ($conditions as $field => $value) {
        if (is_numeric($field)) {
          $cond_pieces[] = "($value)";
        } else {
          if ($escape) $value = $this->quote($value);
          $cond_pieces[] = "($field = $value)";
        }
      }
      $query .= join(' AND ', $cond_pieces);
    }

    return $this->exec($query);
  }

  /**
  * This adds an entry in to a table.
  *
  * Attributes are provided as an array with fields as the keys and
  * values as the values. Unless $escape is set to false the values
  * will be escaped for safe database insertion.
  *
  * Checks the permission db/{table}/insert unless force is set to true.
  * The provided conditions are passed to this permissions check as userdata.
  * @param string $table The name of the table to add a record to.
  * @param array $data The data to be contained in this row.
  * @param boolean $force If set to true this skips permission checks.
  * @param boolean $escape If set to false this avoids automatic escaping of values.
  */
  function add($table, $data, $force = false, $escape = true) {
    if (!$force) assert_permitted_userdata("db/$table/insert", $data);

    $fields = join(', ', array_keys($data));
    if ($escape) {
      $values = join(', ', array_map(array($this, 'quote'), array_values($data)));
    } else {
      $values = join(', ', array_values($data));
    }
    $query = "INSERT INTO $table ($fields) VALUES ($values)";
    return $this->exec($query);
  }

  /**
  * Quote a value for use in the underlying database.
  */
  function quote($s) {
    $q = new LazyQuery($this->_db, "");
    return $q->quote($s);
  }

  /**
  * Remove unsafe characters from an identifier.
  */
  function clean_identifier($s) {
    return preg_replace('/[^\w.]/', '', $s);
  }

  /**
  * This executes a query on the database and returns a PDO result.
  *
  * This wrapper function handles the capture and logging of errors whether the
  * database reports them through the exception or errorInfo methods.
  */
  function exec($query) {
    global $c;
    if ( isset($debuggroups['querystring']) || isset($c->dbg['querystring']) || isset($c->dbg['ALL']) ) {
      dbg_error_log('LOG-DBGQ', preg_replace('/\s+/', ' ', $query));
    }

    $start = microtime(true);
    $result = null;
    try {
      $result = $this->_db->exec($query);

      $errors = $this->_db->errorInfo();
      if ($errors[2]) {
        throw new DatabaseError($errors[2]);
      }
    } catch (Exception $e) {
      dbg_error_log("LOG-QF", $query);
      $errorinfo = $this->_db->errorInfo();
      dbg_error_log("LOG-QF", $e->getMessage());
      throw $e;
    }
    $duration = microtime(true) - $start;
    if ($duration > $this->warn_time) {
      dbg_error_log("LOG-SQ", "Took: %2.06lf for %s.", $duration, $query);
    }

    return $result;
  }

  function query($query) {
    global $c;
    if ( isset($debuggroups['querystring']) || isset($c->dbg['querystring']) || isset($c->dbg['ALL']) ) {
      dbg_error_log('LOG-DBGQ', preg_replace('/\s+/', ' ', $query));
    }

    $args = func_get_args();
    array_shift($args);

    $start = microtime(true);
    $result = null;
    $statement = null;
    try {
      $statement = $this->_db->prepare($query);

      if (!$statement->execute($args)) {
        $errorinfo = $statement->errorInfo();
        throw new DatabaseError($errorinfo[2]);
      }
    } catch (Exception $e) {
      dbg_error_log("LOG-QF", $query);
      dbg_error_log("LOG-QF", $e->getMessage());
      throw $e;
    }
    $duration = microtime(true) - $start;
    if ($duration > $this->warn_time) {
      dbg_error_log("LOG-SQ", "Took: %2.06lf for %s.", $duration, $query);
    } elseif ( isset($debuggroups['querystring']) || isset($c->dbg['querystring']) || isset($c->dbg['ALL']) ) {
      dbg_error_log('LOG-DBGQ', "Query took %2.06lf seconds.", $duration, $query);
    }

    return new QueryResult($statement);
  }
}

if (!isset($database)) {
  if (isset($c->db_pdo_string) && $c->db_pdo_string !== null) {
    $database = new WRMSDatabase(new PDO($c->db_pdo_string));
  } elseif ( isset($c->pg_connect) && is_array($c->pg_connect) ) {
    foreach( $c->pg_connect AS $dbsettings ) {
      try {
        $database = new WRMSDatabase(new PDO("pgsql:{$dbsettings}"));
        break;
      } catch (PDOException $e) {
        // do nothing
      }
    }
  }
  if (!$database) {
    echo <<<EOERRMSG
      <html><head><title>Database Connection Failure</title></head><body>
      <h1>Database Error</h1>
      <h3>Could not connect to PostgreSQL database</h3>
      </body>
      </html>
EOERRMSG;
  }
}