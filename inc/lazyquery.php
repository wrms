<?php

class DatabaseError extends Exception {
}

/**
  LazyQuery represents a partially complete SQL query.

  It can be used to compose queries which may be shared and specialised
  across different modules.

  Functions for generating subqueries return a new query
  without changing the old one. This allows sharing of
  queries without strange and difficult-to-trace problems
  happening.
*/
class LazyQuery implements IteratorAggregate {
  private $_table_portion;
  private $_filters = array();
  private $_having = array();
  private $_order = array();
  private $_grouping = array();
  private $_extra_fields = array();
  private $_offset = 0, $_limit = -1;
  private $_ex_lock= false, $_distinct = false;
  public $warn_time = 5.0;
  public $query_name;

  private $_db;

  function __construct($database, $first_table) {
    $this->_db = $database;
    $this->_table_portion = $first_table;

    $this->query_name = "q" . substr(md5(microtime()),0,10);
  }

  /**
  * Clone this query, generating a new unique ID.
  *
  * This is called when the object is cloned using the clone keyword.
  * @return LazyQuery
  */
  function __clone() {
    $_result = false;
    $this->query_name = "q" . substr(md5(microtime()),0,10);
  }

  /**
  * Add a condition or consitions and return the resulting query.
  *
  * This method accepts a series of SQL conditions as arguments
  * and returns a new LazyQuery with these conditions added.
  *
  * All conditions must be true for a record to be included.
  * @return LazyQuery
  */
  function filter() {
    $fargs = func_get_args();
    $args = array();
    foreach ($fargs as $a) {
      if (is_array($a)) {
        $args = array_merge($args, $a);
      } else {
        array_push($args, $a);
      }
    }

    $copy = clone $this;
    $copy->_filters = array_merge($this->_filters, $args);
    return $copy;
  }

  /**
  * Filter based on the equality of a field to a constant.
  *
  * This takes a field name and a value and adds a condition forcing
  * the field to be equal to that value, returning the resulting query.
  *
  * The value passed in will be quoted to be safe for use in the database.
  * @param string $field The name of the field to check. Not escaped.
  * @param mixed $value The value the field should have to be included.
  * @return LazyQuery
  */
  function filter_eq($field, $value = null) {
    if ($value === null && is_array($field)) {
      $conditions = array();
      foreach ($field as $f => $v) {
        $quoted = $this->quote($v);
        $conditions[] = "$f = $quoted";
      }
      return $this->filter($conditions);
    } else {
      $quoted = $this->quote($value);
      return $this->filter("$field = $quoted");
    }
  }

  /**
  * This is a more flexible version of filter_eq allowing any operator.
  *
  * This works the same way as filter_eq but accepts the operator to use
  * such as '<', '>=', '~=', etc as the second parameter.
  * @param string $field The name of the field to check. Not escaped.
  * @param string $op The operator to use for comparison. Not escaped.
  * @param mixed $value The fixed value to compare the field contents to.
  * @return LazyQuery
  */
  function filter_op($field, $op, $value) {
    $quoted = $this->quote($value);
    return $this->filter("$field $op $quoted");
  }

  /**
  * This checks whether any of a series of conditions are true.
  *
  * This accepts a series of conditions as arguments and returns a
  * new LazyQuery including only records where at least one of the
  * conditions matches.
  * @return LazyQuery
  */
  function filter_any() {
    $args = func_get_args();

    $bargs = array();
    foreach ($args as $a) $bargs[] = "($a)";

    $copy = clone $this;
    $copy->_filters[] = join(' OR ', $bargs);
    return $copy;
  }

  /**
  * This checks whether a field is a member of a given set.
  *
  * This takes a field name and a list of possible values. It then
  * filters based on the field content being in this list and returns
  * a new LazyQuery.
  *
  * @param string $field The field to check the value of.
  * @param array $list The list of values to be included.
  * @param boolean $quote Whether to quote the contents of the list. Defaults to true.
  * @return LazyQuery
  */
  function filter_in($field, $list, $quote = true) {
    if ($list instanceof LazyQuery && is_string($quote)) {
      return $this->filter("$field IN (" . $list->toSQL($quote) . ")");
    } elseif (is_string($list)) {
      return $this->filter("$field IN (" . $list . ")");
    } else {
      if ($quote) {
        $safe_vals = array();
        foreach ($list as $value) $safe_vals[] = $this->quote($value);
      } else {
        $safe_vals = $list;
      }
      return $this->filter("$field IN (" . join(', ', $safe_vals) . ")");
    }
  }

  /**
  * This quotes a value for use in the attached database.
  *
  * Mostly for internal use but can also be used externally.
  * @return string
  */
  function quote($datum) {
    if (is_string($datum)) {
      return $this->_db->quote($datum);
    } elseif (is_numeric($datum)) {
      return strval($datum);
    } elseif (is_bool($datum)) {
      return ($datum) ? 'TRUE' : 'FALSE';
    } else {
      return $this->_db->quote(syck_dump($datum));
    }
  }

  /**
  * Group this query on a series of fields to apply aggregate functions.
  *
  * This pretty much maps directly to the SQL GROUP clause. Any previous
  * groupings in this query will be replaced by the new call.
  * @return LazyQuery
  */
  function group() {
    $args = func_get_args();

    $copy = clone $this;
    $copy->_grouping = $args;
    return $copy;
  }

  /**
  * Filter records after grouping.
  *
  * This is similar to the filter method but while filter operates before
  * records are grouped and aggregated this operates afterwards.
  * @return LazyQuery
  */
  function having() {
    $args = func_get_args();

    $copy = clone $this;
    $copy->_having = array_merge($this->_having, $args);
    return $copy;
  }

  /**
  * Sort the results based on a series of fields.
  *
  * This accepts a series of results and sorts the results by them.
  * The sorting happens first by the left-hand field.
  * @return LazyQuery
  */
  function sort() {
    $args = func_get_args();

    $copy = clone $this;
    $copy->_order = array_unique(array_merge($args, $this->_order));
    return $copy;
  }

  /**
  * Removes sorting from a query.
  *
  * This is sometimes necessary when using get_distinct.
  */
  function unsort() {
    $copy = clone $this;
    $copy->_order = array();
    return $copy;
  }

  /**
  * This produces a copy of the query which will gain an exclusive lock on its rows.
  *
  * This is for use in locking and transactions (not otherwise explicity supported)
  * @return LazyQuery
  */
  function for_update() {
    $copy = clone $this;
    $copy->_ex_lock = true;
    return $copy;
  }

  /**
  * This extracts a range from the results given a start and end index.
  *
  * Subsequent calls will extract a range from within the existing range.
  *
  * @param integer $offset The start offset
  * @param integer $end The end offset
  * @return LazyQuery
  */
  function range($offset, $end) {
    $limit = $end - $offset + 1;

    $copy = clone $this;
    $copy->_offset += $offset;
    if ($copy->_limit < 0) {
      $copy->_limit = $limit;
    } else {
      $copy->_limit = min($copy->_limit - $offset, $limit);
    }
    return $copy;
  }

  /**
  * This adds a join to another table or query.
  *
  * This joins on a table to the current result. It accepts several ways of joining.
  * This requires either just a single key (joins with a USING condition), two keys
  * (joins with an ON a = b condition) or no keys (leaves join conditions to
  * filters).
  *
  * If a query is provided as the other relation its unique identifier (public
  * variable field_name) will be used as the ALIAS of the query. Fields can be
  * referenced using this as a prefix. To make joining easier, if the second
  * key begins with a full stop this alias will be prepended to it.
  *
  * @param mixed other A table name or another query to join against.
  * @param string $leftkey The name of a field in the current results to use in joining or false for none.
  * @param string $rightkey The name of a field in the other relation to use in joining or false for none.
  * @param string $join_type The type of join to use. Defaults to "INNER".
  * @return LazyQuery
  */
  function join($other, $leftkey = false, $rightkey = false, $join_type = "INNER") {
    $copy = clone $this;

    if ($other instanceof LazyQuery) {
      $joined_query = $other->query_name;
      $other = "({$other->toSQL()}) AS {$other->query_name}";
    }
    $copy->_table_portion .= " $join_type JOIN $other";

    if ($leftkey) {
      if ($rightkey) {
        if (isset($joined_query) && substr($rightkey,0,1) == '.') {
          $rightkey = $joined_query . $rightkey;
        }
        $copy->_table_portion .= " ON $leftkey = $rightkey";
      } else {
        if (is_array($leftkey)) {
          $leftkey_b = array();
          foreach ($leftkey as $c) $leftkey_b[] = "($c)";

          $copy->_table_portion .= " ON " . join(' AND ', $leftkey_b);
        } else {
          if (isset($joined_query)) throw new QueryError("Joining against a query using field equality requires two fully qualified field names.");
          $copy->_table_portion .= " USING ($leftkey)";
        }
      }
    }
    return $copy;
  }

  /**
  * Natural join against a table.
  *
  * @param string $other The table to join against.
  * @return LazyQuery
  */
  function natural_join($other) {
    return $this->join($other, null, null, "NATURAL");
  }

  /**
  * Left outer join against another relation.
  *
  * @param string $leftkey The name of a field in the current results to use in joining or false for none.
  * @param string $rightkey The name of a field in the other relation to use in joining or false for none.
  * @return LazyQuery
  */
  function left_join($other, $lk = false, $rk = false) {
    return $this->join($other, $lk, $rk, "LEFT OUTER");
  }

  /**
  * Right outer join against another relation.
  *
  * @param string $leftkey The name of a field in the current results to use in joining or false for none.
  * @param string $rightkey The name of a field in the other relation to use in joining or false for none.
  * @return LazyQuery
  */
  function right_join($other, $lk = false, $rk = false) {
    return $this->join($other, $lk, $rk, "RIGHT OUTER");
  }

  /**
  * Full outer join against another relation.
  *
  * @param string $leftkey The name of a field in the current results to use in joining or false for none.
  * @param string $rightkey The name of a field in the other relation to use in joining or false for none.
  * @return LazyQuery
  */
  function full_outer_join($other, $lk = false, $rk = false) {
    return $this->join($other, $lk, $rk, "FULL OUTER");
  }

  /**
  * This generates an EXISTS condition as a string.
  *
  * This will return a condition as SQL text checking that this
  * query would return a result.
  * @return string
  */
  function exists($query) {
    $query = $this->toSQL();
    return "EXISTS ($query)";
  }

  /**
  * Convert this query to SQL.
  *
  * This accepts a series of field names as arguments and will return
  * a SQL text query to retrieve these fields with all conditions, joins
  * and the rest of this query applied.
  * @return string
  */
  function toSQL() {
    $args = func_get_args();
    $query = "SELECT ";

    if ($this->_distinct) $query .= "DISTINCT ";

    # Decide what to select
    if (count($args) > 0) {
      # Get contents of $args
      $query .= join(', ', array_unique(array_merge($args, $this->_extra_fields)));
    } else {
      # Get all available
      if (count($this->_grouping) > 0) {
        $query .= join(', ', array_unique(array_merge($this->_grouping, $this->_extra_fields)));
      } else {
        $query .= join(', ', array_unique(array_merge(array("*"), $this->_extra_fields)));
      }
    }

    $query .= " FROM ";
    $query .= $this->_table_portion;

    if (count($this->_filters) > 0) {
      $query .= " WHERE ";
      $first = true;
      foreach ($this->_filters as $filter) {
        if (!$first) $query .= " AND ";
        $first = false;
        $query .= '(' . $filter . ')';
      }
    }

    if (count($this->_grouping) > 0) {
      $query .= " GROUP BY ";
      $query .= join(', ', $this->_grouping);
    }

    if (count($this->_having) > 0) {
      $query .= " HAVING ";
      $first = true;
      foreach ($this->_having as $filter) {
        if (!$first) $query .= " AND ";
        $first = false;
        $query .= '(' . $filter . ')';
      }
    }

    if (count($this->_order) > 0) {
      $query .= " ORDER BY ";
      $query .= join(', ', $this->_order);
    }

    if ($this->_limit > 0) {
      $query .= " OFFSET {$this->_offset} LIMIT {$this->_limit}";
    }

    if ($this->_ex_lock) $query .= " FOR UPDATE";

    return $query;
  }

  /**
  * This will run the query and return an iterator over the results.
  *
  * It can be passed a series of field names and will retrieve these.
  * If no fields are passed this will attempt to retrieve all fields it can.
  * @return QueryResult
  */
  function get() {
    $args = func_get_args();
    $query = call_user_func_array(array($this, 'toSQL'), $args);

    return new QueryResult($this->exec_query($query));
  }

  /**
  * This is equivalent to get but will ensure that each result only appears once.
  * @return QueryResult
  */
  function get_distinct() {
    $args = func_get_args();

    $copy = clone $this;
    $copy->_distinct = true;

    $query = call_user_func_array(array($copy, 'toSQL'), $args);

    return new QueryResult($this->exec_query($query));
  }

  /**
  * This is similar to get but returns results as an array.
  * @return array
  */
  function get_list($field) {
    $result = array();
    $records = $this->get($field);

    foreach ($records as $record) {
      $result[] = $record[$field];
    }

    return $result;
  }

  /**
  * This gets a series of records (one per key) and returns them as an associative array.
  *
  * It expects a key field which should be unique over the query and a
  * series of fields to retrieve. It returns an associative array with each key
  * value as a key and the associated record as the value.
  * @param string $key The field to use as the key
  * @param array $fields The fields to include in the records
  * @return array
  */
  function get_assoc($key, $fields) {
    $result = array();
    $records = call_user_func_array(array($this, 'get'), $fields);

    foreach ($records as $record) {
      $result[$record[$key]] = $record;
    }

    return $result;
  }

  /**
  * This gets a series of records (many per key) and returns them as an associative array.
  *
  * It expects a key field which may be shared between records and a
  * series of fields to retrieve. It returns an associative array with each key
  * value as a key and a list of associated records as the value.
  * @param string $key The field to use as the key
  * @param array $fields The fields to include in the records
  * @return array
  */
  function get_assoc_list($key, $fields) {
    $result = array();
    $records = call_user_func_array(array($this, 'get'), $fields);

    foreach ($records as $record) {
      if (!array_key_exists($record[$key], $result)) $result[$record[$key]] = array();
      $result[$record[$key]][] = $record;
    }

    return $result;
  }

  /**
  * This gets the set union of this query's results with one or more other queries.
  *
  * The columns produced by each query must have the same name and type.
  * @return QueryResult
  */
  function get_union() {
    $others = func_get_args();

    $qs = array('(' . $this->toSQL() . ')');
    foreach ($others as $q) $qs[] = '(' . $q->toSQL() . ')';

    return $this->exec_query(join(' UNION ', $qs));
  }

  private function exec_query($query) {
    global $c, $debuggroups;

    if ( (isset($debuggroups) && isset($debuggroups['querystring'])) || (isset($c) && (isset($c->dbg['querystring']) || isset($c->dbg['ALL']) ))) {
      dbg_error_log('LOG-DBGQ', preg_replace('/\s+/', ' ', $query));
    }

    $start = microtime(true);
    try {
      $result = $this->_db->query($query, PDO::FETCH_ASSOC);

      $errors = $this->_db->errorInfo();
      if (isset($errors[2])) {
        throw new DatabaseError($errors[2]);
      }
    } catch (Exception $e) {
      if (function_exists('dbg_error_log')) {
        dbg_error_log("LOG-QF", $query);
        $errorinfo = $this->_db->errorInfo();
        dbg_error_log("LOG-QF", $e->getMessage());
      }
      throw $e;
    }
    $duration = microtime(true) - $start;
    if ($duration > $this->warn_time) {
      dbg_error_log("LOG-SQ", "Took: %2.06lf seconds for %s.", $duration, $query);
    } elseif ( (isset($debuggroups) && isset($debuggroups['querystring'])) || (isset($c) && (isset($c->dbg['querystring']) || isset($c->dbg['ALL']))) ) {
      dbg_error_log('LOG-DBGQ', "Query took %2.06lf seconds.", $duration, $query);
    }

    if (!$result) {
      $errorinfo = $this->_db->errorInfo();
      if (function_exists('dbg_error_log')) {
        dbg_error_log("LOG-QF", $query);
        dbg_error_log("LOG-QF", $errorinfo[2]);
      }
      throw new QueryError($errorinfo[2]);
    }
    return $result;
  }

  /**
  * This hints that the fields provided as arguments should be included in the results of this query.
  * @return LazyQuery
  */
  function add_fields() {
    $fields = func_get_args();
    $copy = clone $this;

    $copy->_extra_fields = array_merge($copy->_extra_fields, $fields);
    return $copy;
  }

  /**
  * Create a copy of this query referencing a different database.
  *
  * WARNING: switching between different database types may render
  * previous escaping unsafe.
  * This allows queries to be constructed without database access then
  * later bound to a database to be used.
  * @param PDOConnection $database The database to switch to.
  * @return LazyQuery
  */
  function switch_db($database) {
    $other = clone $this;
    $other->_db = $database;
    return $other;
  }

  /**
  * Equivalent to get without any arguments.
  * @return QueryResult
  */
  function getIterator() {
    return $this->get();
  }
}

/**
  QueryResult encapsulates a result and allows the results to be iterated.
*/
class QueryResult implements Iterator {
  private $_result;
  private $_current, $_index = -1;

  function __construct($result) {
    $this->_result = $result;
    $this->next();
  }

  function rewind() {
    # Rewind not possible
  }

  function current() {
    return $this->_current;
  }

  function key() {
    return $this->_index;
  }

  function next() {
    $this->_index += 1;
    $value = $this->_result->fetch();
    if ($value) {
      return $this->_current = new QueryRecord($value);
    } else {
      return $this->_current = $value;
    }
  }

  function valid() {
    return $this->_current !== false;
  }

  function rowCount() {
    return $this->_result->rowCount();
  }

  function length() {
    return $this->_result->rowCount();
  }
}

class QueryRecord implements ArrayAccess, IteratorAggregate {
  private $_record;

  function __construct($record) {
    $this->_record = $record;
  }

  function offsetSet($key,$value) { $this->_record[$key] = $value; }
  function __set($key,$value) { $this->_record[$key] = $value; }

  function offsetGet($key) { return $this->_record[$key]; }
  function __get($key) { return $this->_record[$key]; }

  function offsetUnset($key) {
    if (array_key_exists($key,$this->_record)) {
      unset($this->_record[$key]);
    }
  }
  function __unset($key) {
    if (array_key_exists($key,$this->_record)) {
      unset($this->_record[$key]);
    }
  }

  function offsetExists($key) { return array_key_exists($key,$this->_record); }
  function __isset($key) { return array_key_exists($key,$this->_record); }

  function getIterator() {
    if (is_array($this->_record)) {
      $ao = new ArrayObject($this->_record);
      return $ao->getIterator();
    } else {
      return $this->_record;
    }
  }
}

class QueryError extends Exception {
}
