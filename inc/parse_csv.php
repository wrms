<?php

# A fast but extremely ugly table-driven CSV parser.
# This was generated as Java code using Ragel then translated to PHP.
# This is nearly impossible to understand by eye but is basically
# just a finite state machine encoded in some tables of numbers.
# The original Ragel source is available under scripts/csv.rl

$_csv_actions = array(
      0,    1,    0,    1,    1,    1,    2,    1,    3,    2,    1,    2,
      2,    2,    3,    3,    0,    1,    2,    3,    1,    2,    3,    4,
      0,    1,    2,    3
);


$_csv_key_offsets = array(
      0,    0,    6,   12,   18,   24,   25,   26,   29
);

$_csv_trans_keys = array(
     10,   32,   34,   44,    9,   13,   10,   32,   34,   44,    9,   13,
     10,   32,   34,   44,    9,   13,   10,   32,   34,   44,    9,   13,
     34,   34,   10,   34,   44,   10,   32,   34,   44,    9,   13,    0
);


$_csv_single_lengths = array(
      0,    4,    4,    4,    4,    1,    1,    3,    4
);


$_csv_range_lengths = array(
      0,    1,    1,    1,    1,    0,    0,    0,    1
);

$_csv_index_offsets = array(
      0,    0,    6,   12,   18,   24,   26,   28,   32
);

// $_csv_indicies = array(
//       9,    4,    5,    0,    4,    2,    9,    3,    0,    8,    3,    2,
//       9,    3,    0,    8,    3,    2,    9,    4,    5,    0,    4,    2,
//       7,    6,    7,    6,    9,    6,    8,    0,    8,    8,    5,    0,
//       8,    2,    9,    4,    5,    0,    4,    2,    0
// );

$_csv_trans_targs_wi = array(
      8,    4,    5,    4,    4,    2,    8,    3,    0,    4,    3,    2,
      8,    3,    0,    4,    3,    2,    8,    4,    5,    4,    4,    2,
      7,    6,    7,    6,    8,    6,    4,    0,    8,    4,    5,    4,
      4,    2,    0
);

$_csv_trans_actions_wi = array(
     23,    1,    1,   23,    1,   15,   12,    5,    0,   12,    5,    5,
      7,    0,    0,    7,    0,    0,   19,    0,    0,   19,    0,    9,
      9,    3,    5,    0,    7,    0,    7,    0,   23,    1,    1,   23,
      1,   15,    0
);


$csv_start = 1;
$csv_first_final = 7;
$csv_error = 0;
$csv_en_main = 1;

class ParserError extends Exception {
}

function parse_csv($data) {
  global $_csv_actions, $_csv_key_offsets, $_csv_trans_keys, $_csv_single_lengths, $_csv_range_lengths, $_csv_index_offsets, $_csv_indicies, $_csv_trans_targs_wi, $_csv_trans_actions_wi, $csv_start, $csv_first_final, $csv_error, $csv_en_main;
  $cs = $csv_start;
  
  if ($data[strlen($data)] != "\n") $data .= "\n";
  $p = 0;
  $pe = strlen($data) - 1;
  $lines = array();
  $cline = null;
  $line_number = 0;
  $line_offset = 0;
  $field_start = null;
  $field_end = null;
  
  $_klen = 0;
  $_trans = 0;
  $_acts = 0;
  $_nacts = 0;
  $_keys = 0;
  $_goto_targ = 0;

  while (true) { # goto
    switch ( $_goto_targ ) {
      case 0:
        if ( $p == $pe ) {
          $_goto_targ = 4;
          continue;
        }
        if ( $cs == 0 ) {
          $_goto_targ = 5;
          continue;
        }
      case 1:
        while (true) { # match
          $_keys = $_csv_key_offsets[$cs];
          $_trans = $_csv_index_offsets[$cs];
          $_klen = $_csv_single_lengths[$cs];
          
          $char = ord($data[$p]);
          
          if ( $_klen > 0 ) {
                $_lower = $_keys;
                $_mid = 0;
                $_upper = $_keys + $_klen - 1;
                
                while (true) {
                  if ( $_upper < $_lower )
                    break;

                  $_mid = $_lower + (($_upper - $_lower) >> 1);
                  if ( $char < $_csv_trans_keys[$_mid] )
                          $_upper = $_mid - 1;
                  else if ( $char > $_csv_trans_keys[$_mid] )
                          $_lower = $_mid + 1;
                  else {
                          $_trans += ($_mid - $_keys);
                          break 2; # break match
                  }
                }
                $_keys += $_klen;
                $_trans += $_klen;
          }

          $_klen = $_csv_range_lengths[$cs];
          if ( $_klen > 0 ) {
            $_lower = $_keys;
            $_mid;
            $_upper = $_keys + ($_klen<<1) - 2;
            while (true) {
              if ( $_upper < $_lower )
                break;

              $_mid = $_lower + ((($_upper - $_lower) >> 1) & ~1);
              if ( $char < $_csv_trans_keys[$_mid] )
                $_upper = $_mid - 2;
              else if ( $char > $_csv_trans_keys[$_mid + 1] )
                $_lower = $_mid + 2;
              else {
                $_trans += (($_mid - $_keys) >> 1);
                break 2; # break match
              }
            }
            $_trans += $_klen;
          }
          break;
        }; # end match
  
        #$_trans = $_csv_indicies[$_trans];
        $cs = $_csv_trans_targs_wi[$_trans];
  
        if ( $_csv_trans_actions_wi[$_trans] != 0 ) {
          $_acts = $_csv_trans_actions_wi[$_trans];
          $_nacts = $_csv_actions[$_acts++];
          while ( $_nacts-- > 0 ) {
            switch ( $_csv_actions[$_acts++] )
            {
              case 0:
                # new line
                if ($cline !== null) $lines[] = $cline;
                $cline = array();
                $line_number += 1;
                $line_offset = p;
                break;
              case 1:
                # mark field start
                $field_start = $p;
                break;
              case 2:
                # mark field end (continuing)
                $field_end = $p;
                break;
              case 3:
                # end field
                $field_content = substr($data, $field_start, $field_end - $field_start);
                $field_content = str_replace('""', '"', $field_content);
                array_push($cline, $field_content);
                $field_start = null;
                $field_end = null;
                break;
            }
          }
        }

      case 2:
        if ( $cs == 0 ) {
                $_goto_targ = 5;
                continue 2; # continue goto
        }
        if ( ++$p != $pe ) {
                $_goto_targ = 1;
                continue 2; # continue goto
        }
      case 4:
        if ($field_start !== null) {
          $field_end += 1;
          
          $field_content = substr($data, $field_start, $field_end - $field_start);
          $field_content = str_replace('""', '"', $field_content);
          array_push($cline, $field_content);
        }
        if ($cline !== null) $lines[] = $cline;
        return $lines;
      case 5:
        throw new ParserError('CSV parsing failed! Line: ' . $line_number . ', Column: ' . ($p - $line_offset));
    }
  }
}

?>