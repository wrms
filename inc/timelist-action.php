<?php

  param_to_global('chg_on', '#^[a-z0-9 ,.:/-]+$#i' );
  param_to_global('chg_ok', '#^[a-z0-9]+$#i' );
  param_to_global('chg_amt', '#^[0-9.-]+$#i');
  param_to_global('chg_inv', '#^[a-z0-9 ,.:/-]+$#i');

  if ( $logged_on && isset( $chg_on ) && is_array( $chg_on ) && is_array( $chg_amt ) ) {
    $query = "";
    $total_charges = 0.0;
    $count = 0;
    while( list( $k, $v ) = each ( $chg_on ) ) {
      $charge_ok = ( $chg_ok[$k] == 1 ? "TRUE" : "FALSE" );
      $amount = doubleval( $chg_amt[$k] );
      $invoice = $chg_inv[$k];
      $charge_date = $v;
      $query .= "UPDATE request_timesheet SET";
      if ( $chg_ok[$k] == 1 && $invoice != "" && ($amount <> 0 || eregi( "^w((/o)|(rite))", $invoice)) ) {
        /**
        * To set the charged status we need an invoice number (or writeoff), and we need to have said that it's OK to charge.
        */
        $query .= " work_charged='$charge_date',";
        $query .= " charged_by_id=$session->user_no,";
        $query .= " charged_details='$invoice', ";
        $query .= " charged_amount=$amount, ";
        $total_charges += $amount;
        $count++;
      }
      $query .= " ok_to_charge=$charge_ok ";
      $query .= " WHERE timesheet_id=$k;\n";
    }

    $rid = awm_pgexec( $dbconn, $query );

    $c->messages[] = sprintf("A total of $%.2lf was recorded from %d timesheets.", $total_charges, $count );
  }
