<?php

require_once('class.phpmailer.php');

class WRMSMailer extends PHPMailer {
  function WRMSMailer() {
    global $system_name, $admin_email, $debug_email, $c;
    
    if (isset($c) && $c->mail) {
      // Use WRMS mail configuration
      
      $this->Mailer = (isset($c->mail['method']) ? $c->mail['method'] : "smtp");
      
      if (isset($c->mail['from_address'])) {
        $this->From = $c->mail['from_address'];
      } elseif ($admin_email) {
        $this->From = $admin_email;
      }
      if (isset($c->mail['from_name'])) {
        $this->FromName = $c->mail['from_name'];
      } elseif ($system_name) {
        $this->FromName = $system_name;
      }
      if (isset($c->mail['sender'])) {
        $this->Sender = $c->mail['sender'];
      } elseif ($admin_email) {
        $this->Sender = $admin_email;
      }
      
      if (isset($c->mail['sendmail']) && is_array($c->mail['sendmail'])) {
        $smc = $c->mail['sendmail'];
        if (isset($smc['path'])) $this->Sendmail = $smc['path'];
      }
      
      if (isset($c->mail['smtp']) && is_array($c->mail['smtp'])) {
        $smtpc = $c->mail['smtp'];
        if (isset($smtpc['hostname'])) $this->Hostname = $smtpc['hostname'];
        if (isset($smtpc['host'])) $this->Host = $smtpc['host'];
        if (isset($smtpc['port'])) $this->Port = $smtpc['port'];
        if (isset($smtpc['username'])) $this->Username = $smtpc['username'];
        if (isset($smtpc['username'])) $this->SMTPAuth = true;
        if (isset($smtpc['password'])) $this->Password = $smtpc['password'];
        if (isset($smtpc['timeout'])) $this->Timeout = $smtpc['timeout'];
      }
    } else {
      // Use old method to fill fields
      
      $this->Mailer = "smtp";
      
      if ($admin_email) $this->From = $admin_email;
      if ($admin_email) $this->Sender = $admin_email;
      if ($system_name) $this->FromName = $system_name;
    }
  }
}