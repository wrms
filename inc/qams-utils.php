<?php
/* ******************************************************************** */
/* CATALYST PHP Source Code                                             */
/* -------------------------------------------------------------------- */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with this program; if not, write to:                           */
/*   The Free Software Foundation, Inc., 59 Temple Place, Suite 330,    */
/*   Boston, MA  02111-1307  USA                                        */
/* -------------------------------------------------------------------- */
/*                                                                      */
/* Filename:    qams-utils.php                                          */
/* Author:      Paul Waite                                              */
/* Description: QAMS miscellaneous utility funcs and classes            */
/*                                                                      */
/* ******************************************************************** */
// UTILITY FUNCTIONS

/**
 * Used to escape particular characters (typically the single quote) so
 * that they can form part of the database data, rather than being
 * interpreted as command syntax. Note that this implementation makes
 * sure the string has none of the deprecated backslashes added by the
 * old addslashes() function, before escaping the content properly.
 * @param string $str
 * @return string The same string with appropriate chars escaped.
 */
function escape_string($str="") {
  if (is_string($str)) {
    $str = pg_escape_string(stripslashes($str));
  }
  return $str;
} // escape_string

// .......................................................................
/**
 * Used to unescape any escaped characters. Normally databases require
 * nothing to be done here for single-quote escapes, as they return the
 * data already unescaped.
 * Note: for Postgres we have been using add/stripslashes to escape
 * string data. To remedy a security issue this is no longer used, but
 * we need to apply a stripslashes here to removed slashes from any
 * legacy data out there.
 * @param string $str
 * @return string The same string with escaped chars UN-escaped.
 */
function unescape_string($str="") {
  if (is_string($str)) {
    $str = stripslashes($str);
  }
  return $str;
} // unescape_string
// .......................................................................
/**
 * Returns the 'nice' decoded description for a QAMS approval status code.
 * @param string $code Status code: 'y', 'n', 'p', or 's'
 */
function qa_approval_status($code) {
  switch ($code) {
    case "y": return "Approved"; break;
    case "n": return "Refused"; break;
    case "s": return "Skipped"; break;
    case "p": return "In progress"; break;
     default: return "";
  } // switch
} // qa_approval_status

// .......................................................................
/**
 * Returns the key colour for a QAMS approval status code. Used in
 * highlighting stuff in QAMS.
 * @param string $code Status code: 'y', 'n', 'p', or 's'
 */
function qa_approval_colour($code) {
  switch ($code) {
    case "y": return "green"; break;
    case "n": return "red"; break;
    case "s": return "#E260E0"; break;
    case "p": return "blue"; break;
     default: return "#000000";
  } // switch
} // qa_approval_colour

// .......................................................................
/**
 * Returns the 'nice' decoded description for a QAMS approval
 * status code, and spanned with the appropriate colour.
 * @param string $code Status code: 'y', 'n', 'p', or 's'
 * @param string $label Optional replacement label, displaces status to title
 */
function qa_status_coloured($code, $label="", $suffix="") {
  $status = qa_approval_status($code);

  $title = $status;
  if ($label == "") {
    $label = $status;
  }
  else {
    $title = $status;
  }
  if ($label == "") $label = "--";
  if ($title == "") $title = "No approvals";

  return  "<span "
        . "style=\"color:" . qa_approval_colour($code) . "\" "
        . "title=\"$title $suffix\""
        . ">"
        . $label
        . "</span>";
} // qa_status_coloured

// .......................................................................
/**
* Nice days difference between two timestamps. Returns a nice phrase
* expressing the difference in days.
* @param string $fromts From Unix timestamp
* @param string $tots To Unix timestamp (defaults to 'now')
* @return string Nice sentence describing difference in days
*/
function days_diff_ts($fromts, $tots="now") {
  $s = "";
  if ($tots === "now") {
    $tots = time();
  }
  $diff_secs = abs($fromts - $tots);
  $diff_days = number_format($diff_secs / (3600 * 24), 0);
  if ($diff_days == 0) {
    $s = "less than a day";
  }
  elseif ($diff_days == 1) {
    $s = "1 day";
  }
  else {
    $s = "$diff_days days";
  }
  return $s;
} // days_diff_ts

// .......................................................................
/**
* Add a parameter keyvalue pair to a URL. If it is already there, the
* parameter will just be re-written with the new value.
* @param string $href URL to append the new parm to
* @param string $pname Name of the parameter
* @param string $pvalue Value of the parameter
*/
function href_addparm($href, $pname, $pvalue) {
  if ($href != "") {
    $urlbits = explode("?", $href);
    $baseurl = $urlbits[0];
    $query = (isset($urlbits[1]) ? $urlbits[1] : "");
    $qvars = array();
    parse_str($query, $qvars);
    $qvars[$pname] = $pvalue;
    $q = array();
    foreach ($qvars as $name => $val) {
      $q[] = "$name=$val";
    }
    $query = implode("&", $q);
    $href = $baseurl . (($query != "") ? "?$query" : "");
  }
  return $href;
} // href_addparm

// .......................................................................
/**
* Remove a parameter keyvalue pair from a URL.
* @param string $href URL to remove the parm from
* @param string $pname Name of the parameter
*/
function href_delparm($href, $pname) {
  if ($href != "") {
    $urlbits = explode("?", $href);
    $baseurl = $urlbits[0];
    $query = (isset($urlbits[1]) ? $urlbits[1] : "");
    $qvars = array();
    parse_str($query, $qvars);
    if (isset($qvars[$pname])) {
      unset($qvars[$pname]);
    }
    $q = array();
    foreach ($qvars as $name => $val) {
      $q[] = "$name=$val";
    }
    $query = implode("&", $q);
    $href = $baseurl . (($query != "") ? "?$query" : "");
  }
  return $href;
} // href_delparm

// -----------------------------------------------------------------------
// DATE CONVERSION: USER DISPLAY -> DATABASE
// -----------------------------------------------------------------------
// DATE FORMAT DEFINITIONS
/** Example: 31/12/1999 23:59 */
define("DISPLAY_DATE_FORMAT",      "d/m/Y H:i");

/** Example: 31/12/1999 */
define("DISPLAY_DATE_ONLY",        "d/m/Y");

/** Example: 23:59 */
define("DISPLAY_TIME_ONLY",        "H:i");

/** Example: Mar 3rd 1999 1:30pm */
define("NICE_DATETIME",            "M jS Y g:ia");

/** Example: Mar 3rd 1999 1:30pm */
define("QAMS_DATETIME",            "D j M Y g:ia");

/** Example: Mar 3rd 1999 */
define("NICE_DATE",                "M jS Y");

/** Example: Friday, 20th July 2001 */
define("DAY_AND_DATE",             "l, jS F Y");

/** Example: 1999-07-17 23:59:59 */
define("ISO_DATABASE_FORMAT",      "Y-m-d H:i:s");

/** ISO 8601 Example: YYYYMMDDTHHMMSS-HHMM */
define("ISO_8601",                 "Ymd\THisO");

// -----------------------------------------------------------------------
// DATE CONVERSION FUNCTIONS
// -----------------------------------------------------------------------
/**
* Conversion: descriptive to timestamp.
* Converts a descriptive date/time string into a timestamp.
* @param string $displaydate   Date in any 'descriptive' format
* @return integer Unix timestamp (earliest = 1970-1-1)
*/
function displaydate_to_timestamp($displaydate) {
  $res = 0;
  if ($displaydate) {
    $dmy = get_DMY($displaydate);
    if (($dmy["year"] == 1900 || $dmy["year"] == 1970) && $dmy["month"] == 1 && $dmy["day"] == 1) {
      // Invalid date, so try magic wand..
      $res = strtotime($displaydate);
    }
    else {
      if (checkdate($dmy["month"], $dmy["day"], $dmy["year"])) {
          $hms = get_HMS($displaydate);
          $res = mktime( $hms["hours"], $hms["minutes"], $hms["seconds"],
                        $dmy["month"], $dmy["day"],     $dmy["year"] );
      }
      else {
        // Try this magic routine as last resort..
        $res = strtotime($displaydate);
      }
    }
  }
  return $res;
}

// .......................................................................
/**
* Conversion: timestamp to datetime.
* Returns a string in the correct format for populating
* a database 'datetime' field, as per ISO 8601 spec.
* @param  integer $timestamp   Unix timestamp
* @return string  Datetime string in ISO YYYY-MM-DD HH:MM:SS format
*/
function timestamp_to_datetime($timestamp=false) {
  $res = false;
  if ($timestamp === false) {
    $timestamp = time();
  }
  $res = date(ISO_DATABASE_FORMAT, $timestamp);
  return $res;
}

// .......................................................................
/**
* Conversion: descriptive to datetime.
* Converts a descriptive date/time string into a database
* compatible 'datetime' field as per Postgres ISO spec.
* @param string $displaydate   Date in any 'descriptive' format
* @return string  Datetime string in Postgres YYYY-MM-DD HH:MM:SS format
*/
function displaydate_to_datetime($displaydate) {
  return timestamp_to_datetime( displaydate_to_timestamp($displaydate) );
}

// .......................................................................
/**
* Conversion: descriptive to datetime without the time element.
* Converts a descriptive date/time string into a database
* compatible 'date' field as per Postgres ISO spec.
* @param string $displaydate   Date in any 'descriptive' format
* @return string  Date string in Postgres YYYY-MM-DD format
*/
function displaydate_to_date($displaydate) {
  $dt = displaydate_to_datetime($displaydate);
  $bits = explode(" ", $dt);
  return $bits[0];
}

// .......................................................................
/**
* Conversion: datetime to descriptive
* Returns a timestamp from a database-formatted datetime string.
* We are assuming the datetime is formatted in the ISO format of
* "1999-07-17 23:59:59+12" - POSTGRES_STD_FORMAT (ISO)
* This is set up by the standard application via an SQL query
* with the "SET DATESTYLE ISO" command (see query-defs.php).
* @param  string  $datetime   Database-compatible ISO date string
* @return integer Unix timestamp (earliest = 1970-1-1)
*/
function datetime_to_timestamp($datetime) {
  $res = FALSE;
  if ($datetime) {
    $dt = explode("+", $datetime);
    $dtim = explode(" ", $dt[0]);

    $ymd = explode("-", $dtim[0]);
    if (isset($dtim[1])) $tim = explode(":", $dtim[1]);

    if (isset($ymd[0])) $year = $ymd[0];
    else $year = 1970;
    if (isset($ymd[1])) $month = $ymd[1];
    else $month = 1;
    if (isset($ymd[2])) $day = $ymd[2];
    else $day = 1;

    if (isset($tim[0])) $hrs = $tim[0];
    else $hrs = 0;
    if (isset($tim[1])) $min = $tim[1];
    else $min = 0;
    if (isset($tim[2])) $sec = $tim[2];
    else $sec = 0;

    $res = mktime($hrs, $min, $sec, $month, $day, $year);
  }
  return $res;
}

// .......................................................................
/**
* Conversion: timestamp to descriptive
* Convert a Unix timestamp into a descriptive date/time format
* for the user display, using the given displayformat string.
* @param  string  $displayformat  See defined formats.
* @param  integer $timestamp      Unix timestamp
* @return string  The formatted descriptive datetime string
*/
function timestamp_to_displaydate($displayformat, $timestamp="") {
  if ($timestamp == "") {
    $timestamp = time();
  }
  return date($displayformat, $timestamp);
}

// .......................................................................
/**
* Conversion: datetime to descriptive
* Convert a database-compatible datetime string into a
* descriptive date/time format for the user display,
* using the given displayformat string.
* @param  string  $displayformat  See defined formats.
* @param  string  $datetime       Database datetime string
* @return string  The formatted descriptive datetime string
*/
function datetime_to_displaydate($displayformat, $datetime) {
  if ($datetime == NULL) {
    return "";
  }
  else {
    return timestamp_to_displaydate( $displayformat, datetime_to_timestamp($datetime) );
  }
}

?>