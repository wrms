<?php

function check_etag($unique_key, $resource = false) {
  $etag = md5(strval($unique_key));
  header("ETag: $etag");
  if (array_key_exists('HTTP_IF_NONE_MATCH', $_SERVER) && $etag == $_SERVER['HTTP_IF_NONE_MATCH']) {
    header("HTTP/1.1 304 Not Modified");
    exit;
  } elseif ($resource) {
    write_resource_etag_cache($resource, $etag);
  }
  
  return $etag;
}

function check_mtime() {
  $mtimes = func_get_args();
  $mtime = max($mtimes);
  $mtime_str = gmdate("D, d M Y H:i:s", $mtime) . " GMT";
  header("Last-Modified: $mtime_str");
  
  if (array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER) && $mtime_str <= $_SERVER['HTTP_IF_MODIFIED_SINCE']) {
    header("HTTP/1.1 304 Not Modified");
    exit;
  }
  
  return $mtime;
}

function check_fmtime() {
  $files = func_get_args();
  
  $mtimes = array_map('filemtime', $files);
  
  if (in_array(FALSE, $mtimes, true)) {
    dbg_error_log('LOG-CF', "Cache failure in {$_SERVER['SCRIPT_FILENAME']} - checking modification times.");
  }
  
  return check_mtime(max($mtimes));
}
