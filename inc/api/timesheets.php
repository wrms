<?php

require_once('api/api_util.php');
require_once('WRMSDatabase.php');

$days = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');

function compare_records_by_request_id($a,$b) {
  return $b["request_id"] - $a["request_id"];
}

/**
* Get the user's most recent / popular work request list.
*
* @returns An array of arrays containing "request_id" and "work_description" for the most popular requests.
*/
function recent_activities($max_age = 3, $date = false, $limit = false) {
  global $database, $session;
  
  if (!$date) $date = strftime('%Y-%m-%d');
  
  # Sanitise date
  if (!preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $date)) {
    throw new RequestFormatException("Date must be in YYYY-MM-DD format");
  }
  
  $recent_wrs = $database->get_request_timesheet()
    ->natural_join('request')
    ->filter_eq("work_by_id", intval($session->user_no))
    # Time filters
    ->filter("date_trunc('week', work_on) >= date_trunc('week', date '$date' - interval '$max_age weeks')")
    ->filter("date_trunc('week', work_on) <= date_trunc('week', date '$date')")
    # Index-friendly less selective time filters
    ->filter("work_on > date '$date' - interval '$max_age + 1 weeks'")
    ->filter("work_on < date '$date' + interval '1 week'")
    ->filter("work_duration IS NOT NULL")
    ->group("request.request_id", "request.brief", "work_description")
    ->sort("max(work_on) DESC");
 
  if ($limit) $recent_wrs = $recent_wrs->range(0, $limit - 1);
  
  return $recent_wrs;
}

function recent_requests($max_age = 6, $date = false) {
  $recent = recent_activities($max_age, $date);
  return $recent->group('request.request_id', 'request.brief');
}

function favourite_activities() {
  global $database, $session;
  return $database
    ->get_favourite_timesheet()
    ->natural_join('request')
    ->filter_eq('user_no', $session->user_no)
    ->sort('rank')
    ->get('request.request_id', 'request.brief', 'work_description');
}

/**
* Get the time spent on WR, description pairs during a given week.
*
* @param $week A date within the week to be searched.
*/
function week_times($week) {
  
  if ($week && !preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $week)) {
    throw new RequestFormatException("Date must be in YYYY-MM-DD format");
  }
  
  global $database, $session, $days;
  $times = $database->get("request_timesheet");
  $work = $times
    ->filter_eq("work_by_id", intval($session->user_no))
    ->filter("work_duration IS NOT NULL")
    ->group("request_id", "work_description", "work_on::date", "review_needed")
    ->add_fields("work_on::date AS work_date", "EXTRACT (dow FROM work_on::date) AS day", "sum(work_duration) AS hours", "review_needed AS needs_review");
  
  if ($week) {
    $work = $work
      ->filter("date_trunc('week', timestamp " . $work->quote($week) . ") = date_trunc('week', work_on)")
      ->filter("timestamp " . $work->quote($week) . " + interval '1 week' > work_on")
      ->filter("timestamp " . $work->quote($week) . " - interval '1 week' < work_on");
  } else {
    $work = $work
      ->filter("date_trunc('week', CURRENT_TIMESTAMP) = date_trunc('week', work_on)")
      ->filter("CURRENT_TIMESTAMP + interval '1 week' > work_on")
      ->filter("CURRENT_TIMESTAMP - interval '1 week' < work_on");
  }
  
  if (!defined("NETWORK_API")) return $popular_wrs;
  
  $values = array();
  foreach ($work as $w) {
    list($date, $zeroes) = explode(' ', $w['work_date'], 2);
    
    $values[] = array(
      "request_id" => $w["request_id"],
      "work_description" => $w["work_description"],
      "day" => $days[$w["day"]],
      "date" => $date,
      "hours" => pg_interval_to_hours($w["hours"]),
      "needs_review" => $w["needs_review"]
    );
  }
  
  return $values;
}

/**
* Set the number of hours worked on a WR and description for the current user.
*
* @param $request integer The ID of the request
* @param $desc string The description for this work
* @param $date string The date this work was done on
* @param $hours float The number of hours spent on this activity on the date given.
* @param $check_hours float Check that the existing hours matches this if given, fail otherwise. The default value of -1 means no check is performed.
* @param $rate
* @param $needs_review integer Whether this timesheet needs reviewing
* @returns number of existing hours or "failed" on failure.
*/
function record_timesheet($request, $desc, $date, $hours, $check_hours = -1, $rate = null, $needs_review = 0) {
  global $database, $session;
  if ($request == 0) throw new RequestFormatException("The WR number was invalid");
  if ($hours < 0) throw new RequestFormatException("You can't do negative work!");
  
  if (!preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $date)) {
    throw new RequestFormatException("Date must be in YYYY-MM-DD format");
  }
  
  try {
    $database->beginTransaction();
    
    # Exclusively lock the request and check it it present
    $wr_present = iterator_count($database->get_request()->filter_eq("request_id", $request)->for_update()->get(1));
    if (!$wr_present) {
      throw new RequestFormatException("The WR number was invalid");
    }
    
    # Get already existing records for this user, request, description and date
    $records_present = $database
      ->get("request_timesheet")
      ->filter_eq("request_id", $request)
      ->filter("work_duration IS NOT NULL")
      ->filter_eq("work_description", $desc)
      ->filter_eq("work_by_id", $session->user_no)
      ->filter("work_on::date = date " . $database->quote($date));
    
    # Get number of existing records and total hours
    $res = $records_present->get("COUNT(*) AS existing, SUM(work_duration) AS hours")->current();
    $existing_hours = pg_interval_to_hours($res['hours']);
    $existing_records = $res["existing"];
    
    # Enforce check if it is provided
    if ($check_hours >= 0 && $check_hours != $existing_hours) {
      throw new APIException("check failed. $check_hours != $existing_hours", 200);
    }
    
    if ($rate == null) $rate = default_timesheet_rate(intval($session->user_no), intval($request));
    
    if ($hours == 0.0) {
      # Time has been zeroed - delete existing timesheets
      if ($existing_records > 0) {
        $database->delete_request_timesheet(array(
          "request_id" => intval($request),
          "work_by_id" => intval($session->user_no),
          "work_duration IS NOT NULL",
          "work_description" => $desc,
          "work_on::date" => $date
        ));
      }
    } elseif ($existing_records == 0) {
      # Insert a new record
      $database->add_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_description" => $desc,
        "work_on" => $date,
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours",
        "work_rate" => $rate,
        "review_needed" => !empty($needs_review)
      ));
    } elseif ($existing_records == 1) {
      # Update the existing timesheet with the new value
      $database->update_request_timesheet(array(
        "request_id" => intval($request),
        "work_duration IS NOT NULL",
        "work_description" => $desc,
        "work_on::date" => $date,
        "work_by_id" => intval($session->user_no)
      ), array(
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours",
        "work_rate" => $rate,
        "review_needed" => !empty($needs_review)
      ));
    } else {
      # More than one record is present.
      # Amalgamate them by deleting then inserting.
      
      $database->delete_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_duration IS NOT NULL",
        "work_description" => $desc,
        "work_on::date" => $date
      ));
      
      $database->add_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_description" => $desc,
        "work_on" => $date,
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours",
        "work_rate" => $rate,
        "review_needed" => !empty($needs_review)
      ));
    }
    
    if ($database->commit()) {
      return $existing_hours;
    } else {
      return "failed";
    }
  } catch (Exception $e) {
    $database->attemptRollBack();
    throw $e;
  }
}

/**
* Add some time to a work request.
*
* This is similar to record_time except that it adds to the existing
* time instead of setting it.
*
* @param $request integer The ID of the request
* @param $desc string The description for this work
* @param $date string The date this work was done on
* @param $hours float The number of hours spent on this activity on the date given.
* @returns number of existing hours or "failed" on failure.
*/
function add_time_timesheet($request, $desc, $date, $hours, $rate = null) {
  global $database, $session;
  if ($request == 0) throw new RequestFormatException("The WR number was invalid");
  if ($hours < 0) throw new RequestFormatException("You can't do negative work!");
  
  if (!preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $date)) {
    throw new RequestFormatException("Date must be in YYYY-MM-DD format");
  }
  
  try {
    $database->beginTransaction();
    
    # Exclusively lock the request and check it it present
    $wr_present = iterator_count($database->get_request()->filter_eq("request_id", $request)->for_update());
    if (!$wr_present) {
      throw new RequestFormatException("The WR number was invalid");
    }
    
    # Get already existing records for this user, request, description and date
    $records_present = $database
      ->get("request_timesheet")
      ->filter_eq("request_id", $request)
      ->filter("work_duration IS NOT NULL")
      ->filter_eq("work_description", $desc)
      ->filter_eq("work_by_id", $session->user_no)
      ->filter("work_on::date = date " . $database->quote($date));
    
    # Get number of existing records and total hours
    $res = $records_present->get("COUNT(*) AS existing, SUM(work_duration) AS hours")->current();
    $existing_hours = pg_interval_to_hours($res['hours']);
    $existing_records = $res["existing"];
    
    $hours += $existing_hours;
    if ($rate == null) $rate = default_timesheet_rate(intval($session->user_no), intval($request));
    
    if ($existing_records == 0) {
      # Insert a new record
      $database->add_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_description" => $desc,
        "work_on" => $date,
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours",
        "work_rate" => $rate
      ));
    } elseif ($existing_records == 1) {
      # Update the existing timesheet with the new value
      $database->update_request_timesheet(array(
        "request_id" => intval($request),
        "work_duration IS NOT NULL",
        "work_description" => $desc,
        "work_on::date" => $date,
        "work_by_id" => intval($session->user_no)
      ), array(
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours"
      ));
    } else {
      # More than one record is present.
      # Amalgamate them by deleting then inserting.
      
      $database->delete_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_duration IS NOT NULL",
        "work_description" => $desc,
        "work_on::date" => $date
      ));
      
      $database->add_request_timesheet(array(
        "request_id" => intval($request),
        "work_by_id" => intval($session->user_no),
        "work_description" => $desc,
        "work_on" => $date,
        "work_duration" => "$hours hours",
        "work_quantity" => $hours,
        "work_units" => "hours",
        "work_rate" => $rate
      ));
    }
    
    if ($database->commit()) {
      return $existing_hours;
    } else {
      return "failed";
    }
  } catch (Exception $e) {
    $database->attemptRollBack();
    throw $e;
  }
}

/**
* Change the description of timesheets given a request and the old description.
*
* This updates the description attached to all records having the old one
* for this request. It returns the number of records updated.
*
* @param $request_id integer The unique ID of the request
* @param $old_desc string The original description
* @param $new_desc string The new description for these timesheets
*/
function change_timesheet_description($request_id, $old_desc, $new_desc) {
  global $database, $session;
  $database->beginTransaction();
  
  # Check for existing favourite
  $existing = $database->get_request_timesheet()->filter_eq(array(
    "work_by_id" => $session->user_no,
    "request_id" => $request_id,
    "work_description" => $new_desc
  ))->get("COUNT(*) AS c")->current();
  
  # Add if not already present
  if ($existing['c'] == 0) {
    $database->update_favourite_timesheet(array(
      "user_no" => $session->user_no,
      "request_id" => $request_id,
      "work_description" => $old_desc
    ), array(
      "work_description" => $new_desc
    ));
  } else {
    $database->delete_favourite_timesheet(array(
      "user_no" => $session->user_no,
      "request_id" => $request_id,
      "work_description" => $old_desc
    ));
  }
  
  $count = $database->update_request_timesheet(array(
    "work_by_id" => $session->user_no,
    "request_id" => $request_id,
    "work_description" => $old_desc
  ), array(
    "work_description" => $new_desc
  ));
  
  $database->commit();
  return $count;
}

function set_favourite_timesheets($timesheets) {
  global $database, $session;
  
  $database->beginTransaction();
  
  $reqs = array();
  foreach ($timesheets as $time) {
    if (array_search($time['request_id'], $reqs) == null) $reqs[] = $time['request_id'];
  }
  
  foreach ($reqs as $id) {
    $reqc = $database->get_request()->filter_eq('request_id', $id)->get('COUNT(*) AS c')->current();
    
    if ($reqc['c'] == 0) {
      $database->rollback();
      throw new RequestFormatException('Work request #' . $id . ' does not exist.');
    }
  }
  
  $database->delete_favourite_timesheet(array(
    'user_no' => $session->user_no
  ));
  
  $i = 0;
  foreach ($timesheets as $time) {
    $database->add_favourite_timesheet(array(
      'user_no' => $session->user_no,
      'request_id' => $time['request_id'],
      'work_description' => $time['work_description'],
      'rank' => $i
    ));
    $i += 1;
  }
  
  if ($database->commit()) {
    return $i;
  } else {
    return "failed";
  }
}

function default_timesheet_rate($worker_id, $request_id) {
  global $database;

  $client_rate = $database->get_request()->filter_eq('request_id', $request_id)->join('usr', 'requester_id', 'user_no')->join('organisation', 'org_code')->get('work_rate')->current()->work_rate;
  if ($client_rate) return $client_rate;

  $worker_rate = default_worker_rate($worker_id);
  if ($worker_rate) return $worker_rate;

  $supplier_rate = $database->get_usr()->filter_eq('user_no', $worker_id)->join('organisation', 'org_code')->get('work_rate')->current()->work_rate;
  if ($supplier_rate) return $supplier_rate;

  return 120;
}