<?php

require_once('api/api_util.php');
require_once('WRMSDatabase.php');

function get_users_for_organisation($org_code) {
  global $database;
  return $database->get_usr()->filter_eq('org_code', $org_code)->sort('fullname')->get('user_no', 'enabled', 'username', 'email', 'fullname', 'phone', 'mobile', 'org_code', 'location', 'base_rate', 'active');
}

function default_worker_rate($worker_id) {
  global $database;
  
  return $database->get_usr()->filter_eq('user_no', $worker_id)->get('base_rate')->current()->base_rate;
}