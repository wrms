<?php

require_once('api/api_util.php');

function get_requests_for_organisation($org_code) {
  global $database;
  return $database->get_request()
                  ->join('usr', 'requester_id', 'user_no')
                  ->filter_eq('org_code', $org_code)
                  ->sort('request_id')
                  ->get(
                    'request_id', 'request_on', 'request.active', 'last_status', 'urgency',
                    'importance', 'severity_code', 'request_type', 'requester_id',
                    'eta', 'last_activity', 'request_by', 'brief', 'system_id', 'parent_request'
                  );
}

function search_requests($query, $return_lazyquery = false) {
  global $database, $c, $session;
  $requests = $database->get_request()
                       ->join('work_system', 'system_id')
                       ->join('usr as requester', 'request.requester_id', 'requester.user_no')
                       ->join('usr as creator', 'request.entered_by', 'creator.user_no');
  
  if (!$session->can('access/all_systems') ) {
    $requests = $requests->join('system_usr', 'work_system.system_id', 'system_usr.system_id')->filter_eq('system_usr.user_no', intval($session->user_no));
  }
  
  if (!(isset($query['include_inactive']) && $query['include_inactive'] == 'true')) {
    $requests = $requests->filter('request.active');
  }
  
  if (!$session->can('access/all_organisations')) {
    $requests = $requests->filter_eq('requester.org_code', intval($session->org_code));
  } elseif (isset($query['org_code'])) {
    $requests = $requests->filter_eq('requester.org_code', intval($query['org_code']));
  }
  
  if (isset($query['requester'])) $requests = $requests->filter_eq('requester.user_no', intval($query['requester']));
  if (isset($query['system'])) $requests = $requests->filter_eq('request.system_id', intval($query['system']));
  if (isset($query['type'])) $requests = $requests->filter_eq('request.request_type', intval($query['type']));
  if (isset($query['from'])) $requests = $requests->filter_op('request.last_activity', '>=', $query['from']);
  if (isset($query['to'])) $requests = $requests->filter_op('request.last_activity', '<=', $query['to']);
  
  if (isset($query['watcher'])) {
    $requests = $requests->join('request_interested', 'request_interested.request_id', 'request.request_id')
                         ->filter_eq('request_interested.user_no', intval($query['watcher']));
  }
  
  if (isset($query['allocated'])) {
    $requests = $requests->join('request_allocated', 'request_allocated.request_id', 'request.request_id')
                         ->filter_eq('request_allocated.user_no', intval($query['allocated']));
  } elseif (isset($query['unallocated'])) {
    $requests = $requests->left_join('request_allocated', 'request_allocated.request_id', 'request.request_id')
                         ->filter('request_allocated.user_no IS NULL');
  }
  
  if (isset($query['search'])) {
    $query_safe = $database->quote($query['search']);
    if ($c->use_tsearch2) {
      $matching_notes = $database->get_request_note()->filter("to_tsvector('english', note_detail) @@ to_tsquery($query_safe)");
      $requests = $requests->left_join($matching_notes, 'request.request_id', '.request_id');
      
      $requests = $requests->filter_any(
        "tsvector_concat(to_tsvector('english', brief), to_tsvector('english', detailed)) @@ to_tsquery($query_safe)",
        $matching_notes->query_name . ".request_id IS NOT NULL"
      );
    } else {
      $matching_notes = $database->get_request_note()->filter("note_detail ~* $query_safe");
      $requests = $requests->left_join($matching_notes, 'request.request_id', '.request_id');
      
      $requests = $requests->filter_any(
        'brief ~* ' . $query_safe,
        'detailed ~* ' . $query_safe,
        $matching_notes->query_name . ".request_id IS NOT NULL"
      );
    }
  }
  
  if (isset($query['search_request'])) {
    $query_safe = $database->quote($query['search_request']);
    if ($c->use_tsearch2) {      
      $requests = $requests->filter(
        "tsvector_concat(to_tsvector('english', brief), to_tsvector('english', detailed)) @@ to_tsquery($query_safe)"
      );
    } else {
      $requests = $requests->filter_any(
        'brief ~* ' . $query_safe,
        'detailed ~* ' . $query_safe
      );
    }
  }
  
  if (isset($query['search_notes'])) {
    $query_safe = $database->quote($query['search_notes']);
    if ($c->use_tsearch2) {
      $matching_notes = $database->get_request_note()->filter("to_tsvector('english', note_detail) @@ to_tsquery($query_safe)");
      $requests = $requests->left_join($matching_notes, 'request.request_id', '.request_id');
      
      $requests = $requests->filter(
        $matching_notes->query_name . ".request_id IS NOT NULL"
      );
    } else {
      $matching_notes = $database->get_request_note()->filter("note_detail ~* $query_safe");
      $requests = $requests->left_join($matching_notes, 'request.request_id', '.request_id');
      
      $requests = $requests->filter(
        $matching_notes->query_name . ".request_id IS NOT NULL"
      );
    }
  }
  
  $sort_fields = array(
    'request_id' => 'request.request_id',
    'request_on' => 'request_on',
    'request.active' => 'request.active',
    'last_status' => 'last_status',
    'urgency' => 'urgency',
    'importance' => 'importance',
    'severity_code' => 'severity_code',
    'request_type' => 'request_type',
    'eta' => 'eta',
    'last_activity' => 'last_activity',
    'request_by' => 'request_by',
    'brief' => 'brief',
    'system_id' => 'request.system_id',
    'parent_request' => 'parent_request',
    'requester_id' => 'request.requester_id',
    'requester_name' => 'requester_name'
  );
  
  if (isset($query['sort']) && array_key_exists($query['sort'], $sort_fields)) {
    if (isset($query['order']) && $query['order'] == 'desc') {
      $requests = $requests->sort($sort_fields[$query['sort']] . ' desc');
    } else {
      $requests = $requests->sort($sort_fields[$query['sort']]);
    }
  } else {
    $requests = $requests->sort('request_id desc');
  }
  
  if (isset($query['last'])) {
    if (isset($query['first'])) $first = intval($query['first']); else $first = 0;
    $requests = $requests->range(0, intval($query['last']));
  }
  
  // Query is complete - return data
  
  if ($return_lazyquery) return $requests; // Allow full access to lazy query
  
  return $requests->get_distinct(
    'request.request_id', 'request_on', 'request.active', 'last_status', 'urgency',
    'importance', 'severity_code', 'request_type', 'eta', 'last_activity', 'request_by',
    'brief', 'request.system_id', 'parent_request', 'request.requester_id', 'requester.fullname as requester_name'
  );
}