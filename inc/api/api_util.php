<?php

require_once('api/registry.php');

class APIException extends Exception {
  public $status_code;
  
  function __construct($msg = "API function call failed.", $code = 500) {
    parent::__construct('ERROR - ' . $msg);
    $this->status_code = $code;
  }
}

class RequestFormatException extends APIException {
  function __construct($msg = "API function parameters were invalid") {
    parent::__construct('Bad request format: ' . $msg, 200);
  }
}

function APIErrorHandler($errno, $errstr, $errfile, $errline) {
  static $error_lookup = array(
    E_NOTICE => 'Notice',
    E_WARNING => 'Warning',
    E_ERROR => 'Error',
    E_USER_NOTICE => 'User Notice',
    E_USER_WARNING => 'User Warning',
    E_USER_ERROR => 'User Error',
  );
  $errno = isset($error_lookup[$errno]) ? $error_lookup[$errno] : $errno;

  dbg_error_log('LOG-ERR', "($errfile:$errline) $errno - $errstr");
  if ($errno & E_ERROR) {
    throw new APIException();
  } else {
    return true;
  }
}

/**
* Given an API function this generates a form to submit data to it.
*
* It will try to use first POST then GET as the method.
*/
function api_form($path) {
  global $api_methods;
  $method = 'POST';
  list($fn, $junk) = api_resolve_path($api_methods['POST'], $path);
  if (!$fn) {
    $method = 'GET';
    list($fn, $junk) = api_resolve_path($api_methods['GET'], $path);
    if (!$fn) api_fail("That method does not exist", 404);
  }
  
  $fn_path = join('/', $path);
  $form = "<form action=\"/api.php/$fn_path\" method=\"$method\">";
  
  if (is_array($fn)) {
    array_shift($fn);
    $form .= "<table>";
    foreach ($fn as $p => $def) {
      # Numeric keys mean the field name is in the value
      if (is_numeric($p)) {
        $p = $def;
        $def = null;
      }
      
      # Remove type
      $pparts = explode('::', $p, 2);
      if (count($pparts) > 1) {
        $p = $pparts[0];
      }
      
      $def = strval($def);
      $form .= "<tr><td>$p:</td>";
      $form .= "<td><input type=\"text\" name=\"$p\" value=\"$def\" /></td></tr>";
    }
    $form .= "</table>";
    $form .= "<input type=\"submit\" value=\"Submit\" />";
  } else {
    $form .= "<label for=\"api_form_json\">Parameters</label>";
    $form .= "<textarea id=\"api_form_json\" cols=\"40\" rows=\"10\"></textarea>";
  }
  
  $form .= "</form>";
  return $form;
}

function api_method_list() {
  global $api_methods;
  $posts = list_api_methods_recursive($api_methods['POST'], '', 'post');
  $gets = list_api_methods_recursive($api_methods['GET'], '', 'get');
  return array_merge($posts, $gets);
}

function list_api_methods_recursive($tree, $prefix, $type) {
  if (is_string($tree)) {
    # Default calling method
    return array(array(
      'name' => substr($prefix, 1),
      'method' => $type,
      'required' => array(),
      'optional' => array(),
      'params' => array(),
      'accept_any' => true,
      'accept_path' => false
    ));
  } elseif (array_key_exists(0, $tree)) {
    # Leaf - method
    $method = array(
      'name' => substr($prefix, 1),
      'method' => $type,
      'required' => array(),
      'optional' => array(),
      'params' => array(),
      'accept_any' => false,
      'accept_path' => false
    );
    
    $mname = array_shift($tree);
    
    # Collect arguments
    foreach ($tree as $p => $def) {
      if (is_numeric($p) && $def){
        $p = $def;
        $def = "_required";
      }
      
      if (substr($p,0,1) == '_') {
        if ($p == '_data') {
          $method['accept_any'] = true;
        } elseif ($p == '_path') {
          $method['accept_path'] = true;
        } elseif (substr($p, 0, 6) == "_path_") {
          if ($method['accept_path'] != true) {
            if ($method['accept_path'] == false) $method['accept_path'] = 0;
            $path_c = intval(substr($p, 6)) + 1;
            $method['accept_path'] = max($method['accept_path'], $path_c);
          }
        }
      } elseif ($def !== "_required") {
        $method['params'][] = $p;
        $method['optional'][] = $p;
      } else {
        $method['params'][] = $p;
        $method['required'][] = $p;
      }
    }
    
    return array($method);
  } else {
    $result = array();
    foreach ($tree as $branch => $subtree) {
      $sub_result = list_api_methods_recursive($subtree, $prefix . '/' . $branch, $type);
      foreach ($sub_result as $r) $result[] = $r;
    }
    return $result;
  }
}

/**
 * Convert an object into an associative array
 *
 * This function converts an object into an associative array by iterating
 * over its public properties. Because this function uses the foreach
 * construct, Iterators are respected. It also works on arrays of objects.
 *
 * @author jfdsmit@gmail.com
 * @return array
 */
function object_to_array($var) {
    $result = array();
    $references = array();

    // loop over elements/properties
    foreach ($var as $key => $value) {
        // recursively convert objects
        if (is_object($value) || is_array($value)) {
            // but prevent cycles
            if (!in_array($value, $references)) {
                $result[$key] = object_to_array($value);
                $references[] = $value;
            }
        } else {
            // simple values are untouched
            $result[$key] = $value;
        }
    }
    return $result;
}

/**
 * Convert a value to JSON
 *
 * This function returns a JSON representation of $param. It uses json_encode
 * to accomplish this, but converts objects and arrays containing objects to
 * associative arrays first. This way, objects that do not expose (all) their
 * properties directly but only through an Iterator interface are also encoded
 * correctly.
 *
 * @author jfdsmit@gmail.com
 */
function json_encode_safe($param) {
    if (is_object($param) || is_array($param)) {
        $param = object_to_array($param);
    }
    return json_encode($param);
}

function csv_print_header($data) {
  if (!is_array($data)) $data = iterator_to_array($data);
  echo csv_encode_line(array_keys($data));
}

function csv_encode($data) {
  if (count($data) == 0) return '';
  $s = '';
  $adata = $data[0];
  if (!is_array($adata)) $adata = iterator_to_array($adata);
  $s .= csv_encode_line(array_keys($adata));
  foreach ($data as $value) {
    $s .= "\n";
    $s .= csv_encode_line($value);
  }
  return $s;
}

function csv_encode_line($data) {
  $s = '';
  $first = true;
  foreach ($data as $value) {
    if (!$first) $s .= ',';
    $first = false;
    if (is_string($value)) {
      $s .= '"' . str_replace('"', '""', $value) . '"';
    } else {
      $s .= '"' . str_replace('"', '""', json_encode_safe($value)) . '"';
    }
  }
  return $s;
}
