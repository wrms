<?php

/**
* This is where API functions are registered.
*
* The arrays can be nested to arbitrary depth.
*/
$api_methods = array(
  'GET' => array(
    'requests' => array(
      'recent' => array('recent_requests',
        'max_age::integer' => 6,
        'for_date::string' => strftime('%Y-%m-%d')
      ),
      'search' => array('search_requests', '_data')
    ),
    'activities' => array(
      'recent' => array('recent_activities',
        'max_age::integer' => 3,
        'for_date::string' => strftime('%Y-%m-%d'),
        'limit::integer' => false
      ),
      'favourite' => array('favourite_activities')
    ),
    'times' => array(
      'week' => array('week_times', "_path_0" => false)
    ),
    'form' => array('api_form', '_path'),
    'api_methods' => 'api_method_list'
  ),
  'POST' => array(
    'user' => array (
      'login' => array('api_login', 'username::string', 'password::string'),
      'logout' => array('api_logout')
    ),
    'times' => array(
      'record' => array(
        'record_timesheet',
        'request_id::integer',
        'work_description::string',
        'work_on::string' => strftime('%Y-%m-%d'),
        'hours::float',
        'check_hours::float' => -1,
        'rate::float' => null,
        'needs_review::integer' => 0
      ),
      'add' => array(
        'add_time_timesheet',
        'request_id::integer',
        'work_description::string',
        'work_on::string' => strftime('%Y-%m-%d'),
        'hours::float',
        'rate::float' => null
      )
    ),
    'activities' => array(
      'change_description' => array('change_timesheet_description',
        'request_id::integer',
        'old_description::string',
        'new_description::string'
      ),
      'set_favourites' => array('set_favourite_timesheets', 'favourites::array')
    )
  )
);

?>