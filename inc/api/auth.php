<?php

function api_login($username, $password) {
  global $session;
  $session->Login($username, $password);
  @dbg_error_log( "Login", ":api_login: User %s login status is %d", $username, $session->just_logged_in);
  header("Content-type: text/plain");
  if ($session->just_logged_in) return "success"; else return "failed";
}

function api_logout() {
  dbg_error_log( "Login", ":api_logout: Logging out");
  setcookie( 'sid', '', 0,'/');
  header("Content-type: text/plain");
  return "success";
}