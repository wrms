<?php

/**
* This represents an access control list based on a hierarchy.
*
* Actions are strings separated in to a hierarchy by forward slashes.
* Permissions allow or forbid an action and its descendants.
* The most specific permission always takes priority.
*
* This implementation scales well in terms of number of permissions
* per user but may take a long time to determine that a user cannot
* perform an action if the action's length is too large or if
* wildcards are used to match long actions.
*/
class AccessControl {
  private $_access = array();

  function __construct($perms) {
    ksort($perms);
    $this->_access = $perms;
  }

  /**
  * This generates an access control list from a series of roles.
  *
  * The resulting list allows the user to perform the tasks any
  * of those roles can perform and no more.
  */
  static function from_roles($role_data, $roles) {
    $tree = new AccessTree();

    foreach ($roles as $role) {
      if (array_key_exists($role, $role_data)) {

        # Add this role's permissions to the tree.
        $role_perms = expand_permissions($role_data[$role]);
        ksort($role_perms); # Ensure children are added after their parents.
        foreach ($role_perms as $act => $access) {
          $tree->add_access($role, explode('/', $act), $access);
        }
      }
    }

    $perms = $tree->to_perms();

    return new AccessControl($perms);
  }

  function __sleep() {
    # Only _access should be serialised
    return array('_access');
  }

  function __wakeup() {
    # Regenerate the permissions list for searching.
    $this->_perms = array_keys($this->_access);
  }

  /**
  * This works out whether the owner of these permissions can perform an action.
  *
  * @param string $action The action we want to perform
  */
  function allowed($action, $userdata = null) {
    foreach (AccessControl::generate_wildcards($action) as $entry) {
      $perm_parts = explode('/', $entry);

      for ($i = count($perm_parts); $i > 0; $i--) {
        $perm = join('/', array_slice($perm_parts, 0, $i));
        if (!array_key_exists($perm, $this->_access)) continue;

        if ($perm == $entry or strncmp($perm . '/', $entry, strlen($perm) + 1) == 0) {
          # The permission controls this action
          $res = $this->_access[$perm];
          if (is_array($res)) {
            foreach ($res as $opt) {
              if (call_user_func($opt, $this, explode('/', $action), $userdata)) return true;
            }
            return false;
          } else {
            return $res;
          }
        }
      }
    }
    return false;
  }

  static function generate_wildcards($action) {
    $fields = explode('/', $action);
    $last_field = array_pop($fields);

    $options = false;

    foreach ($fields as $field) {
      if ($options) {
        $new_options = array();
        foreach ($options as $option) {
          $new_options[] = $option . '/' . $field;

          if (AccessControl::count_wildcards($option) < 3) {
            $new_options[] = $option . '/*';
          }
        }
        $options = $new_options;
      } else {
        $options = array($field, '*');
      }
    }

    if ($options) {
      $results = array();
      foreach ($options as $option) {
        $results[] = $option . '/' . $last_field;
      }
      return $results;
    } else {
      return array($action);
    }
  }

  function get_permissions_list() {
    return $this->_access;
  }

  static function count_wildcards($path) {
    $count = -1;
    $i = -1;
    while ($i !== false) {
      $count += 1;
      $i = strpos($path, '*', $i + 1);
    }
    return $count;
  }
}

/**
* This class aids in the generation of access control lists.
*
* It maps out the tree of what roles enable what actions at
* each place in the permissions tree. It is able to combine
* a series of permissions granted to different roles and condense
* them in to the set of all actions that can be performed acting
* in any of these roles.
*/
class AccessTree {
  private $roles = array();
  private $subtrees = array();

  /**
  * Add a permission to this tree.
  *
  * If the access parameter is enabled then this action will be allowed
  * for this and all descendants.
  * If the access parameter is disabled then access granted by the same
  * role will be revoked from this and all descendants.
  * By nesting these arbitrary permissions can be achieved.
  * @param mixed $role A unique identifier for a role
  * @param string $path The action string for this permission (see AccessControl).
  * @param boolean $access Whether access should be allowed for this action.
  */
  function add_access($role, $path, $access) {
    if (count($path) > 0) {
      $branch = array_shift($path);
      if (!array_key_exists($branch, $this->subtrees)) {
        $this->subtrees[$branch] = new AccessTree();
        $ch_enablers = (isset($this->enablers) ? $this->enablers : null);
        $this->subtrees[$branch]->enablers = $ch_enablers;
      }
      $this->subtrees[$branch]->add_access($role, $path, $access);
    } else {
      $this->set_enabler_recursive($role, $access);
    }
  }

  /**
  * Convert this tree in to a permission list as used by AccessControl.
  *
  * @param boolean $parent_val Internal use only.
  * The list returned will be an ordered series of action => allowed
  * pairs.
  */
  function to_perms($parent_val = false) {
    ksort($this->subtrees);

    $results = array();

    if ( isset($this->enablers) && count($this->enablers) > 0) {
      if (in_array(true, $this->enablers, true)) {
        $my_access = true;
      } else {
        $my_access = array_values($this->enablers);
      }
    } else {
      $my_access = false;
    }
    if ($my_access != $parent_val) $results[''] = $my_access;

    foreach ($this->subtrees as $treename => $tree) {
      foreach ($tree->to_perms($my_access) as $perm => $access) {
        if ($perm) {
          $results[$treename . '/' . $perm] = $access;
        } else {
          $results[$treename] = $access;
        }
      }
    }

    return $results;
  }

  private function set_enabler_recursive($role, $on) {
    if ($on == false) {
      unset($this->enablers[$role]);
    } else {
      $this->enablers[$role] = $on;
    }

    foreach ($this->subtrees as $tree) {
      $tree->set_enabler_recursive($role, $on);
    }
  }
}

function expand_permissions($perms) {
  $result = array();
  foreach ($perms as $key => $perm) {
    if (is_array($perm)) {
      foreach (expand_permissions($perm) as $subkey => $subperm) {
        $result[$key . '/' . $subkey] = $subperm;
      }
    } else {
      $result[$key] = $perm;
    }
  }
  return $result;
}

if (!function_exists('get_role_permissions')) {
  function get_role_permissions() {
    global $c, $session, $database;
    if ( !is_object($session) || intval($session->user_no) <= 0) return array();

    require_once('WRMSDatabase.php');

    $result = $database->query('SELECT user_role_permissions(?)', intval($session->user_no))->current();

    $result = json_decode($result[0], true);
    return $result;
  }
}

class PermissionsException extends Exception {

  public $action;

  function __construct($msg, $action) {
    parent::__construct($msg);
    $this->action = $action;
  }
}

function assert_permitted() {
  global $session;
  $actions = func_get_args();
  foreach ($actions as $action) {
    $allow = $session->can($action);
    if (!$allow) {
      // Attempt reload of ACL
      $session->LoadACL();
      if (!$session->can($action)) {
        throw new PermissionsException("You do not have permission to perform this action.", $action);
      }
    }
  }
}

function assert_permitted_userdata($action, $userdata) {
  global $session;
  $allow = $session->can_userdata($action, $userdata);
  if (!$allow) {
    // Attempt reload of ACL
    $session->LoadACL();
    if (!$session->can($action)) {
      throw new PermissionsException("You do not have permission to perform this action.", $action);
    }
  }
}