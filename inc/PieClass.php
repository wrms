<?php

require_once("PgQuery.php");

class PieChart {
  var $qry;
  var $radius;
  var $font_height;
  var $data;
  var $total;

  function PieChart($sql) {
    $this->qry = new PgQuery($sql);
    $this->radius = 150;
    $this->margin = 10;
    $this->font_height = 12;
    $this->total = 0;
    $this->colorSlice = array('#f62','#6f2','#62f','#ef4','#e4f','#4ef','#f26','#2f6','#26f','#fe4','#f4e','#4fe',
                              '#d51','#5d1','#51d','#cd3','#c3d','#3cd','#d15','#1d5','#15d','#dc3','#d3c','#3dc',
                              '#b40','#4b0','#40b','#ab2','#a2b','#2ab','#b04','#0b4','#04b','#ba2','#b2a','#2ba',
                             );
  }

  function _CirclePoint($degrees, $radius) {
    $x = cos(deg2rad($degrees)) * $radius;
    $y = sin(deg2rad($degrees)) * $radius;
    return (array($x, $y));
  }

  function Exec($location="PieChart") {
    $this->data = array();
    $this->url = array();

    // Check for a number of rows first, otherwise Exec the query
    if ( $this->qry->rows > 0 || $this->qry->Exec($location) ) {
      $this->total = 0;
      $this->rownum = -1;
      while( $row = $this->qry->Fetch(true) ) {
        $this->data[$row[0]] = $row[1];
        if ( isset($row[2]) ) $this->url[$row[0]] = $row[2];
        $this->total += $row[1];
      }
    }
    return $this->qry->rows;
  }

  function RenderSlice( $name, $offset, $value, $colour_index, $link_url = null ) {

      $StartDegrees = round(($offset / $this->total) * 360) - 90;
      $EndDegrees = round((($offset + $value)/$this->total)*360) - 90;
      $CurrentColor = $this->colorSlice[($colour_index % count($this->colorSlice))];

      $CenterX = $this->radius + $this->margin;
      $CenterY = $this->radius + $this->margin;

      $h = '';
      if ( isset($link_url) ) {
        $h = sprintf('<a xlink:href="%s" target="_parent">', htmlspecialchars($link_url) );
      }


      $h .= "<path ";
      $h .= sprintf( 'title="%s" ', htmlspecialchars("$value of $this->total requests with status $name") );

      $h .= "d=\"";

      //draw out to circle edge
      list($StartX, $StartY) = $this->_CirclePoint($StartDegrees, $this->radius);
      list($EndX, $EndY) = $this->_CirclePoint($EndDegrees, $this->radius);

      $floodstyle = (( ($value * 2) > $this->total ) ? 1 : 0 );

      $h .= ("M " .  ($CenterX + $StartX) . "," .  ($CenterY + $StartY) ." ");
      $h .= ("A $this->radius,$this->radius 0 $floodstyle 1 " .  ($CenterX + $EndX) . "," .  ($CenterY + $EndY) ." ");

      //finish at center of circle
      $h .= ("L $CenterX,$CenterY ");

      //close polygon
      $h .= ("z\" ");

      $h .= ("style=\"fill:$CurrentColor;\"/>\n");

      if ( isset($link_url) ) $h .= "</a>";

      return $h;
  }

  function SetRadius( $newradius ) {
    $this->radius = $newradius;
  }

  function SetFontHeight( $newheight ) {
    $this->font_height = $newheight;
  }

  function Render() {
    if ( !isset($this->data) ) {
      $this->Exec();
    }

    //determine graphic size
    $Diameter = $this->radius * 2;
    $Width = $Diameter + ($this->margin * 2);
    $Height = $Diameter + ($this->margin * 2) + (($this->font_height + 2) * count($this->data));
    $CenterX = $this->radius + $this->margin;
    $CenterY = $this->radius + $this->margin;

    //allocate colors
    $colorBody = 'white';
    $colorBorder = 'black';
    $colorText = 'black';

    //set the content type
    header("Content-type: image/svg+xml");

    //mark this as XML
    print('<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n");

    //Point to SVG DTD
    print('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20000303 Stylable//EN" ');
                    print('"http://www.w3.org/TR/2000/03/WD-SVG-20000303/DTD/' .  'svg-20000303-stylable.dtd">' . "\n");

    //start SVG document, set size in "user units"
    print("<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" >\n");

    /**
    * make background rectangle & draw the circle on it
    */
    print("<rect x=\"0\" y=\"0\" width=\"$Width\" height=\"$Height\" style=\"fill:$colorBody\" />\n");
    print("<circle cx=\"$CenterX\" cy=\"$CenterY\" r=\"$this->radius\" " .
                  "style=\"fill:none; stroke:$colorBorder; stroke-width:3\" />\n");

    /**
    * Draw each slice, starting the pie at 12 O'clock
    */
    $offset = 0;
    $index = 0;

    foreach($this->data as $Label=>$Value) {
      echo $this->RenderSlice( $Label, $offset, $Value, $index++, $this->url[$Label] );
      $offset += $Value;
    }

    /**
    * draw a legend
    */
    $index=0;
    $gap = round($this->font_height * 0.3, 1);
    $BoxX = $Diameter + 30;
    $BoxWidth = $this->font_height * 0.8;
    $LabelX = $Diameter + 30 + ($BoxWidth * 2);
    foreach($this->data as $Label=>$Value) {
      $CurrentColor = $this->colorSlice[$index%(count($this->colorSlice))];
      $BoxY = 30 + (($index++)*($this->font_height + $gap));
      $LabelY = $BoxY + $this->font_height - ($gap / 2) ;

      //draw color box
      print("<rect x=\"$BoxX\" y=\"$BoxY\" width=\"$BoxWidth\" height=\"$this->font_height\" rx=\"5\" " .
                  "style=\"fill:$CurrentColor; stroke:$colorBorder; stroke-width:1\" />\n");

      //draw label
      print("<text x=\"$LabelX\" y=\"$LabelY\" style=\"fill:$colorText; font-size:$this->font_height"."px\">" .
        "$Label: $Value" .  "</text>\n");
    }

    //end SVG document
    print('</svg>' . "\n");

  }
}

