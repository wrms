<?php
  require_once("organisation-selectors-sql.php");

function get_organisation_list( $current=0, $maxwidth=50 ) {
  global $session;

  // $current could be an array if it was created using a multiple selection list.
  if (is_array($current)) $current = array_filter($current,'intval');
  else $current = intval($current);


  $q = new PgQuery(SqlSelectOrganisations( ($session->AllowedTo("see_other_orgs") ? $current : 0) ));
  $org_code_list = $q->BuildOptionList($current,'get_organisation_list', array( 'maxwidth' => $maxwidth ));

  return $org_code_list;
}
