<?php
  require( "user-list.php" );
  include_once( "WRMSDatabase.php" );

function nice_time( $in_time ) {
  /* does nothing yet... */
  return substr("$in_time", 2);
}
  if ( "$because" != "" )
    echo htmlspecialchars($because);

  if ( !isset($tlsort) ) $tlsort = $settings->get('tlsort');
  if ( !isset($tlseq) ) $tlseq = $settings->get('tlseq');

  if ( "$tlsort" == "" ) $tlsort = "requester_name";
  $tlseq = strtoupper($tlseq);
  if ( "$tlseq" == "" ) {
      $tlseq = "ASC";
  }
  if ( "$tlseq" != "ASC" ) $tlseq = "DESC";
  $tlsort = $database->clean_identifier($tlsort);

  $settings->set('tlsort', $tlsort);
  $settings->set('tlseq', $tlseq);

  param_to_global('user_no', '#\d+#');
  param_to_global('work_for', '#\d+#');
  param_to_global('system_id', '#\d+#');
  param_to_global('org_code', '#\d+#');
  param_to_global('search_for', '#.*#');
  param_to_global('requested_by', '#\d+#');
  param_to_global('uncharged', '#[a-z0-9]#');
  param_to_global('charge', '#[a-z0-9]#');
  param_to_global('from_date', '#[a-z 0-9/:-]+#');
  param_to_global('to_date', '#[a-z 0-9/:-]+#');

  if ( !isset($maxresults) || intval($maxresults) == 0 ) $maxresults = 1000;

  if (  "$style" != "stripped" ) {
    echo "<form method=\"GET\" action=\"$base_url/form.php\" name=\"form\" id=\"form\">\n";
    echo "<input type=hidden value=\"timelist\" name=f>\n";
    echo "<table border=0 cellpadding=0 cellspacing=2 align=center class=row0 style=\"border: 1px dashed #aaaaaa;\"><tr><td><table border=0 cellpadding=0 cellspacing=0 width=100%><tr>\n";
    echo "<td class=smb>Find:</td>\n";
    printf("<td class=sml><input class=sml type=text size=\"10\" name=search_for value=\"%s\"></td>\n", htmlspecialchars($search_for));

    if ( is_member_of('Admin','Support', 'Manage') ) {
      $user_list = "<option value=\"\">--- Any Requester ---</option>" . get_user_list( "", $org_code, "$requested_by" );
      echo "<td class=\"smb\">&nbsp;&nbsp;Request&nbsp;By:</td><td class=\"sml\"><select class=\"sml\" name=\"requested_by\">$user_list</select></td>\n";
    }

    if ( is_member_of('Admin','Support') ) {
      $user_list = "<option value=\"\">--- All Users ---</option>" . get_user_list( "Support,Contractor", "", $user_no );
    }
    echo "<td class=smb align=right>&nbsp;&nbsp;Work&nbsp;By:</td><td class=sml><select class=sml name=user_no>$user_list</select></td>\n";

    printf("<td align=right><input type=checkbox value=1 name=uncharged%s></td><td align=left class=smb><label for=uncharged>Uncharged</label></td>\n", ("$uncharged"<>"" ? " checked" : ""));
    printf("<td align=right><input type=checkbox value=1 name=charge%s></td><td align=left class=smb><label for=charge>Charge</label></td>\n", ("$charge"<>"" ? " checked" : ""));
    echo "</tr></table></td>\n<tr><td><table border=0 cellpadding=0 cellspacing=0 width=100%>\n";
    include("system-list.php");
    if ( is_member_of('Admin','Support') )
      $system_list = get_system_list( "", "$system_id", 35);
    else
      $system_list = get_system_list( "CES", "$system_id", 35);
    echo "<td class=smb>System:</td><td class=sml><font size=1><select class=sml name=system_id><option value=\"\">--- All Systems ---</option>$system_list</select></font></td>\n";

    if ( is_member_of('Admin','Support') ) {
      include( "organisation-list.php" );
      $orglist = "<option value=\"\">--- All Organisations ---</option>\n" . get_organisation_list( "$org_code", 35 );
      echo "<td class=smb>&nbsp; &nbsp;Organisation:</td><td class=sml><select class=sml name=\"org_code\">\n$orglist</select></td>\n";
    }
    echo "<td align=left><input type=submit class=submit alt=go id=go value=\"GO>>\"name=go></td>\n";
    echo "</tr></table></td></tr>
<tr><td><table border=0 cellspacing=0 cellpadding=0 width=100%><tr valign=middle>
<td class=smb align=right>Work&nbsp;From:</td>
<td nowrap class=smb><input type=text size=10 name=from_date class=sml value=\"$from_date\">
<a href=\"javascript:show_calendar('forms.form.from_date');\" onmouseover=\"window.status='Date Picker';return true;\" onmouseout=\"window.status='';return true;\">".$theme->Image("date-picker.gif")."</a>
</td>

<td class=smb align=right>&nbsp;To:</td>
<td nowrap class=smb><input type=text size=10 name=to_date class=sml value=\"$to_date\">
<a href=\"javascript:show_calendar('forms.form.to_date');\" onmouseover=\"window.status='Date Picker';return true;\" onmouseout=\"window.status='';return true;\">".$theme->Image("date-picker.gif")."</a>
</td>
<td class=smb align=right>&nbsp;Request #:</td>
<td nowrap class=smb><input name=\"request_id\" size=\"10\" value=\"" . (array_key_exists('request_id', $_GET) ? htmlspecialchars($_GET['request_id']) : '') . "\" /></td>
</tr></table></td>
</tr>
</table>\n</form>\n";
  }

  $numcols = 7;
  if ( "$search_for$system_id " != "" ) {
    $query = $database->get_request_timesheet()
      ->join('request', 'request_id')
      ->join('usr AS requester', 'requester_id', 'requester.user_no')
      ->join('usr AS worker', 'work_by_id', 'worker.user_no')
      ->join('organisation', 'requester.org_code', 'organisation.org_code')
      ->sort("$tlsort $tlseq")
      ->range(0, $maxresults - 1);

    if ( isset($user_no) && $user_no > 0 ) {
      $query = $query->filter_eq('work_by_id', $user_no);
    }

    if ( "$requested_by" <> "" ) {
      $query = $query->filter_eq('requester_id', $requested_by);
    }
    if ( "$search_for" <> "" ) {
      $query = $query->filter_op('work_description', '~*', $search_for);
    }
    if ( isset($system_id) && $system_id > 0 ) {
      $query = $query->filter_eq('request.system_id', $system_id);
    }
    if ( isset($org_code) && $org_code > 0 ) {
      $numcols--;  // No organisation column
      $query = $query->filter_eq('requester.org_code', $org_code);
    }

    if ( array_key_exists('request_id', $_GET) && $_GET['request_id'] != '') {
      $query = $query->filter_eq('request.request_id', intval($_GET['request_id']));
    }

    if ( $from_date && is_string($from_date) && preg_match('/^\s*(\d{4}-\d{2}-\d{2}|\d{2}\/\d{2}\/\d{2}(\d{2})?)\s*$/', $from_date)) {
      $query = $query->filter(
        "request_timesheet.work_on::date >= " . $database->quote($from_date) . "::date",
        "request_timesheet.work_on >= " . $database->quote($from_date) . "::date - interval '1 day'"
      );
    }
    if ( $to_date && is_string($to_date) && preg_match('/^\s*(\d{4}-\d{2}-\d{2}|\d{2}\/\d{2}\/\d{2}(\d{2})?)\s*$/', $to_date)) {
      $query = $query->filter(
        "request_timesheet.work_on::date <= " . $database->quote($to_date) . "::date",
        "request_timesheet.work_on <= " . $database->quote($to_date) . "::date + interval '1 day'"
      );
    }

    if ( "$uncharged" != "" ) {
      $numcols++;  // No charged on column
      if ( "$charge" != "" )
        $query = $query->filter('request_timesheet.ok_to_charge=TRUE');
      $query = $query->filter('request_timesheet.work_charged IS NULL');
    }

    try {

      $result = $query->get('request.*', 'organisation.*', 'request_timesheet.*', 'worker.fullname AS worker_name', 'requester.fullname AS requester_name');

      // Build up the column header cell, with %s gaps for the sort, sequence and sequence image
      $header_cell = "<th class=\"cols\"><a class=\"cols\" href=\"$_SERVER[PHP_SELF]?f=$form&tlsort=%s&tlseq=%s";
      if ( isset($org_code) && $org_code > 0 ) $header_cell .= "&org_code=" . urlencode($org_code);
      if ( isset($system_id) && $system_id > 0 ) $header_cell .= "&system_id=" . urlencode($system_id);
      if ( isset($search_for) ) $header_cell .= "&search_for=" . urlencode($search_for);
      if ( isset($inactive) ) $header_cell .= "&inactive=" . urlencode($inactive);
      if ( isset($requested_by) ) $header_cell .= "&requested_by=$requested_by";
      if ( isset($user_no) && $user_no > 0 ) $header_cell .= "&user_no=$user_no";
      if ( isset($uncharged) ) $header_cell .= "&uncharged=$uncharged";
      if ( array_key_exists('request_id', $_GET)) $header_cell .= "&request_id=".urlencode($_GET['request_id']);
      if ( isset($from_date) ) $header_cell .= "&from_date=$from_date";
      if ( isset($to_date) ) $header_cell .= "&to_date=$to_date";
      if ( isset($type_code) ) $header_cell .= "&type_code=$type_code";
      if ( isset($incstat) && is_array( $incstat ) ) {
        reset($incstat);
        while( list($k,$v) = each( $incstat ) ) {
          $header_cell .= "&incstat[$k]=$v";
        }
      }
      if ( "$style" != "" ) $header_cell .= "&style=$style";
      if ( "$format" != "" ) $header_cell .= "&format=$format";
      $header_cell .= "\">%s";      // %s for the Cell heading
      $header_cell .= "%s</th>";    // %s For the image

      function column_header( $ftext, $fname ) {
        global $tlsort, $tlseq, $header_cell, $theme;
        $fseq = "";
        $seq_image = "";
        if ( "$tlsort" == "$fname" ) {
          $fseq = ( "$tlseq" == "DESC" ? "ASC" : "DESC");
          $seq_image = "&nbsp;".$theme->Image("sort-$tlseq.png");
        }
        printf( $header_cell, $fname, $fseq, $ftext, $seq_image );
      }
      function header_row() {
        echo "<tr>\n";
        if ( ! $GLOBALS['requested_by'] ) {
          if ( ! $GLOBALS['org_code'] ) {
            column_header("Org.", "abbreviation" );
          }
          column_header("Work For", "requester_name");
        }
        column_header("WR&nbsp;#", "request_timesheet.request_id" );
        column_header("Done On", "work_on" );
        column_header("Duration", "work_quantity" );
        column_header("Rate", "request_timesheet.work_rate");
        column_header("Done By", "worker_name" );
        column_header("Description", "work_description" );
        if ( $GLOBALS['uncharged'] ) {
          column_header("Review?", "review_needed" );
          column_header("OK to charge", "" );
          column_header("Invoice", "charged_details" );
        }
        column_header("Amount", "charged_amount" );
        column_header("Charged On", "work_charged" );
        echo "</tr>";
      }
      if (  "$style" != "stripped" ) {
        echo "<p><small>&nbsp;" . $result->length() . " timesheets found\n";
        if ( $result->length() == $maxresults ) echo " (limit reached)";
        if ( "$uncharged" != "" ) {
          printf( "<form enctype=\"multipart/form-data\" method=post action=\"%s%s\">\n", $_SERVER['REQUEST_URI'], ( ! strpos( $_SERVER['REQUEST_URI'], "uncharged" ) ? "&uncharged=1" : ""));
        }
      }
      echo "<table border=\"0\" cellspacing=1 align=center>\n";
      header_row();

      $grand_total = 0.0;
      $total_hours = 0.0;

      // Build table of organisations found
      $row = 0;
      foreach($result as $timesheet) {

        $this_charge = sprintf( '%0.2lf', doubleval( $timesheet->work_quantity * $timesheet->work_rate ));
        $grand_total += $this_charge;
        switch ( $timesheet->work_units ) {
          case 'hours': $total_hours += doubleval( $timesheet->work_quantity );  break;
          case 'days':  $total_hours += doubleval( $timesheet->work_quantity * 8 );  break;
        }

        printf( '<tr class="row%1d" title="WR#%d: %s">', ($row++ % 2), $timesheet->request_id, htmlspecialchars($timesheet->brief));

        if ( ! $GLOBALS['requested_by'] ) {
          if ( ! $GLOBALS['org_code'] ) {
            echo "<td class=sml nowrap>$timesheet->abbreviation</td>\n";
          }
          echo "<td class=sml nowrap>$timesheet->requester_name</td>\n";
        }
        echo "<td class=sml align=center nowrap><a href=\"$base_url/wr.php?request_id=$timesheet->request_id\">$timesheet->request_id</a></td>\n";
        echo "<td class=sml>" . substr( nice_date($timesheet->work_on), 7) . "</td>\n";
        echo "<td class=sml>$timesheet->work_quantity $timesheet->work_units</td>\n";
        echo "<td class=sml align=right>$timesheet->work_rate&nbsp;</td>\n";
        echo "<td class=sml style=\"white-space:nowrap\">$timesheet->worker_name</td>\n";
        echo "<td class=sml>" . html_format( $timesheet->work_description) . "</td>";

        if ( $uncharged ) {
          printf( "<td class=\"sml\">%s</td>\n", ($timesheet->review_needed == 't' ? '<span style="color:red;">Review</span>' : '&nbsp;') );
          echo "<td class=sml align=right>";
          printf("<input type=\"checkbox\" value=\"1\" id=\"$timesheet->timesheet_id\" name=\"chg_ok[$timesheet->timesheet_id]\"%s>", ( $timesheet->ok_to_charge ? " checked" : ""), $timesheet->ok_to_charge);
          printf("<input type=hidden name=\"chg_worker[$timesheet->timesheet_id]\" value=\"%s\">", htmlspecialchars($timesheet->worker_name));
          printf("<input type=hidden name=\"chg_desc[$timesheet->timesheet_id]\" value=\"%s\">", htmlspecialchars($timesheet->work_description));
          printf("<input type=hidden name=\"chg_request[$timesheet->timesheet_id]\" value=\"%s\">", htmlspecialchars($timesheet->request_id));
          printf("<input type=hidden name=\"chg_requester[$timesheet->timesheet_id]\" value=\"%s\">", htmlspecialchars($timesheet->requester_name));
          echo "</td>\n";
          echo "<td class=\"sml\"><input type=\"text\" size=\"6\" name=\"chg_inv[$timesheet->timesheet_id]\" value=\"\"></td>\n";
          echo "<td class=\"sml\"><input type=\"text\" size=\"8\" style=\"text-align:right\"name=\"chg_amt[$timesheet->timesheet_id]\" value=\"$this_charge\"></td>\n";
          echo "<td class=\"sml\"><input type=\"text\" size=\"10\" name=\"chg_on[$timesheet->timesheet_id]\" value=\"" . date( "d/m/Y" ) . "\"></td>\n";
        }
        else {
          echo "<td class=\"sml\" style=\"text-align:right\">$this_charge</td>\n";
          if ( "$timesheet->work_charged" == "" ) {
            if ( "$uncharged" == "" ) echo "<td class=sml>uncharged</td>";
          }
          else
            echo "<td class=\"sml\">Inv #". $timesheet->charged_details.", ". substr( nice_date($timesheet->work_charged), 7) . "</td>";
        }
        echo "</tr>\n";
      }
      if ( "$uncharged" != "" ) {
        echo "<tr><td colspan=$numcols><input type=submit class=submit alt=\"apply changes\" name=submit value=\"Apply Charges\"></td></tr>\n";
        echo "</form>\n";
      }
      printf( "<tr class=\"row%1d\">\n", ($row % 2));
      printf( "<td align=right colspan=" . (isset($org_code) && $org_code > 0 ? "3" : "4" ) . ">%9.2f hours</td>\n", $total_hours);
      printf( "<th colspan=2 align=right>%9.2f</td>\n", $grand_total);
      echo "<td colspan=2>&nbsp;</td></tr>\n";
      echo "</table>\n";
    } catch (DatabaseError $e) {
      // Do nothing like the old version did
    }
  }
  echo "</table></form>\n";

    if ( is_member_of('Admin','Support') && ( "$style" != "stripped" )) {
      $this_page = "$_SERVER[PHP_SELF]?f=$form&style=%s&format=%s";
      if ( "$search_for" != "" ) $this_page .= "&search_for=$search_for";
      if ( isset($org_code) && $org_code > 0 ) $this_page .= "&org_code=$org_code";
      if ( isset($system_id) && $system_id > 0 ) $this_page .= "&system_id=$system_id";
      if ( isset($requested_by) ) $this_page .= "&requested_by=$requested_by";
      if ( isset($user_no) && $user_no > 0 ) $this_page .= "&user_no=$user_no";
      if ( array_key_exists('request_id', $_GET)) $this_page .= "&request_id=".urlencode($_GET['request_id']);
      if ( isset($from_date) ) $this_page .= "&from_date=$from_date";
      if ( isset($to_date) ) $this_page .= "&to_date=$to_date";
      if ( isset($type_code) ) $this_page .= "&type_code=$type_code";
      if ( isset($uncharged) ) $this_page .= "&uncharged=$uncharged";
      if ( "$style" != "" ) $this_page .= "&style=$style";
      if ( "$format" != "" ) $this_page .= "&format=$format";

      echo "<br clear=all><hr>\n<table cellpadding=5 cellspacing=5 align=right><tr><td>Rerun as report: </td>\n<td>\n";
      printf( "<a href=\"$this_page\" target=_new>Standard</a>\n", "stripped", "brief");
      echo "</td></tr></table>\n";
    }
