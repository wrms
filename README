--- WRMS ---

This is the Work Request Management System created for the internal use of Catalyst IT.

---- Dependencies ----

 - PHP 4 or 5
 - PostgreSQL 8.0 or higher (only client is required if a remote database is used)
 - PHP / Postgres interface
 - Andrew's Web Libraries 0.20 or higher
      (see http://andrew.mcmillan.net.nz/projects/awl)

---- Build Dependencies ----

 - sudo aptitude install php-pear
 - sudo pear install phpdocumentor

---- Installation ----

The best way to install this is by building and installing the Debian package. This can be done easily with the commands:

    dpkg-buildpackage -rfakeroot
    sudo dpkg -i ../wrms_<version>_all.deb

You will then need to create a Postgres user called 'general' for the WRMS and run

    /usr/share/wrms/scripts/create-database.sh

to create the database. It will tell you the password generated for the 'admin' account. The account name for the WRMS to use is hard-coded to 'general' but can be renamed after the database creation script has finished. Your new database will be called 'wrms'.

If the database is on a different server you must set the PGHOST and/or PGPORT environment variables before running the script. The user 'general' can now be renamed to anything you like.

To configure the WRMS you will also need to copy the config/config-example.php file to /etc/wrms/wrms.<host>-conf.php and edit it to add your database and site details.

----- Installation on Apache 2 -----

/usr/share/wrms/html should be served as a normal directory with PHP enabled. This is an example site configuration that can be edited appropriately and placed in /etc/apache2/sites-available:

    <VirtualHost *>
        ServerName wrms.your.site
        DocumentRoot /usr/share/wrms/html
    </VirtualHost>

    <Directory /usr/share/wrms>
        php_value include_path ".:/usr/share/php:/usr/share/wrms/inc:/usr/share/awl/inc"
    </Directory>

The php_value line is required to ensure that the appropriate includes are accessible. To enable the site, create a symlink to this file under /etc/apache2/sites-enabled.

----- Installation on Other Web Servers -----

Your web server must be configured to use PHP. Your configuration for this site must allow the use of PHP and must add the directories /usr/share/wrms/inc and /usr/share/awl/inc to PHP's include path. The directory /usr/share/wrms/html contains the HTML and PHP files to be publicly served.
