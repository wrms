<?php

require_once('test_helper.php');
require_once('lazyquery.php');

global $sample_lq_data;
$sample_lq_data = array(
  array("field_a" => "value_a", "field_b" => "value_b", "field_c" => 42),
  array("field_a" => "value_a2", "field_b" => "value_b2", "field_c" => 21),
  array("field_a" => "value_a3", "field_b" => "value_b3", "field_c" => 84)
);

class FakeDB {
  var $it;
  
  function quote($s) {
    $s = strval($s);
    $s = str_replace("'", "''", $s);
    return "'$s'";
  }
  
  function query($s) {
    global $sample_lq_data;
    $this->it = new ArrayIterator($sample_lq_data);
    return $this;
  }
  
  function fetch() {
    if ($this->it->valid()) {
      $val = $this->it->current();
    } else {
      $val = false;
    }
    $this->it->next();
    return $val;
  }
  
  function errorInfo() {
    return array();
  }
}

class LazyQueryTest extends PHPUnit_Framework_TestCase {
  
  var $ta, $tb;
  
  function setUp() {
    $this->ta = new LazyQuery(new FakeDB(), "table_a");
    $this->tb = new LazyQuery(new FakeDB(), "table_b");
  }
  
  # Query building
  
  function test_simple_select() {
    $expected = "SELECT * FROM table_a";
    $this->assertEquals($expected, $this->ta->toSQL());
  }
  
  function test_select_fields() {
    $expected = "SELECT field_a, field_b FROM table_a";
    $this->assertEquals($expected, $this->ta->toSQL("field_a", "field_b"));
  }
  
  function test_condition_combination() {
    $expected = "SELECT * FROM table_a WHERE (condition_a) AND (NOT condition_b) AND (condition_c)";
    $q = $this->ta->filter("condition_a", "NOT condition_b")->filter("condition_c");
    $this->assertEquals($expected, $q->toSQL());
    
    $expected = "SELECT * FROM table_a HAVING (condition_a) AND (NOT condition_b) AND (condition_c)";
    $q = $this->ta->having("condition_a")->having("NOT condition_b", "condition_c");
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_group_select_all() {
    $expected = "SELECT field_a, field_b, field_c FROM table_a GROUP BY field_a, field_b, field_c";
    $q = $this->ta->group("field_a", "field_b", "field_c");
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_select_range() {
    $expected = "SELECT * FROM table_a OFFSET 100 LIMIT 20";
    $q = $this->ta->range(100,119);
    $this->assertEquals($expected, $q->toSQL());
    
    $expected = "SELECT * FROM table_a OFFSET 110 LIMIT 5";
    $q2 = $q->range(10, 14); # A subrange of the existing range
    $this->assertEquals($expected, $q2->toSQL());
  }
  
  function test_join_table() {
    $expected = "SELECT * FROM table_a INNER JOIN table_b USING (field_k)";
    $q = $this->ta->join("table_b", "field_k");
    $this->assertEquals($expected, $q->toSQL());
    
    $expected = "SELECT * FROM table_a LEFT OUTER JOIN table_b ON field_k = field_z";
    $q = $this->ta->left_join("table_b", "field_k", "field_z");
    $this->assertEquals($expected, $q->toSQL());
    
    $expected = "SELECT * FROM table_a FULL OUTER JOIN table_b WHERE (field_k = field_z)";
    $q = $this->ta->full_outer_join("table_b")->filter("field_k = field_z");
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_join_query() {
    $qbname = $this->tb->query_name;
    $qbsql = $this->tb->toSQL();
    
    $expected = "SELECT * FROM table_a INNER JOIN ($qbsql) AS $qbname ON table_a.field_k = $qbname.field_k";
    $q = $this->ta->join($this->tb, "table_a.field_k", ".field_k");
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_filters_autoquote() {
    # Little Bobby Tables!
    $q = $this->ta->filter_eq("field_a", "Robert'); DROP TABLE Students;--");
    $expected = "SELECT * FROM table_a WHERE (field_a = 'Robert''); DROP TABLE Students;--')";
    $this->assertEquals($expected, $q->toSQL());
    
    # Other operations and numbers as values
    $q = $this->ta->filter_op("field_a", ">", 12);
    $expected = "SELECT * FROM table_a WHERE (field_a > 12)";
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_filter_any() {
    $q = $this->ta->filter_any(
      "foo IS NULL",
      "bar = baz"
    );
    $expected = "SELECT * FROM table_a WHERE ((foo IS NULL) OR (bar = baz))";
    $this->assertEquals($expected, $q->toSQL());
  }
  
  function test_run_query() {
    global $sample_lq_data;
    $result = array_map('iterator_to_array', iterator_to_array($this->ta));
    $expected = $sample_lq_data;
    $this->assertEquals($expected, $result);
  }
}