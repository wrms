<?php
require_once('always.php');
require_once('api/timesheets.php');

require_once("authorisation-page.php");
$session->LoginRequired();
include("tidy.php");

$c->scripts[] = "js/prototype.js";
$c->scripts[] = "js/util.js";
$c->scripts[] = "js/time_entry.js";
$c->scripts[] = "js/api.js.php";
$c->local_styles[] = "css/time_entry.css";

require_once("top-menu-bar.php");
require_once("page-header.php");

try {
  assert_permitted('db/request_timesheet/update/work_by=' . $session->user_no);
  assert_permitted('db/request_timesheet/add/work_by=' . $session->user_no);
  assert_permitted('db/request_timesheet/delete/work_by=' . $session->user_no);
  assert_permitted('db/request_timesheet/get');
  assert_permitted('db/request/get');
  assert_permitted('db/favourite_timesheet');
} catch (PermissionsException $e) {
  echo "<h3>Insufficient Permissions.</h3><p>You do not have sufficient access to use this timesheeting system. If you should be able to, please grouch in the direction of the system administrator.</p> <!-- Action: {$e->action} -->";
  exit(1);
}

?>

<script>
onLoad(function() {

  image_urls = {
    add: '<?php echo $theme->ImageURL('add.png'); ?>',
    down: '<?php echo $theme->ImageURL('down.png'); ?>',
    up: '<?php echo $theme->ImageURL('up.png'); ?>',
    remove: '<?php echo $theme->ImageURL('remove.png'); ?>',
    move: '<?php echo $theme->ImageURL('move.png'); ?>'
  }

  var std_req_rows = [
<?php
  $request_ids = array(10, 11, 12, 13, 14, 15, 16, 17, 18, 19);

  $std_reqs = $database->get_request()->filter_in('request_id', $request_ids)->sort('request_id')->get('request_id', 'brief');

  foreach ($std_reqs as $req) {
    $brief = str_replace("'", "\\'", str_replace('\\', '\\\\', $req['brief']));
    $lines[] = "    create_available_row({$req['request_id']}, '{$brief}')";
  }
  echo join(",\n", (isset($lines)?$lines:array()));
?>
  ];

  fillAvailableColumns($('standard_requests'), std_req_rows);
});
</script>

<div id="timesheet_entry_page">
  <img style="float: left; margin-left: 10em; height: 26px;" src="<?php echo $theme->ImageURL('back.png'); ?>" onclick = "previousWeek()" class="button" alt="Previous">
  <img style="float: right; margin-right: 10em; height: 26px;" src="<?php echo $theme->ImageURL('forward.png'); ?>" onclick="nextWeek();" class="button" alt="Next">

  <h2 id="week_heading"></h2>

  <img id="indicator" src="<?php echo $theme->ImageURL('loading.gif'); ?>" height="30" width="30" alt="loading">

  <div>
    <table cellspacing="2">
      <thead id="top_totals_grid">
        <tr class="totals_row" class="grid_row">
          <th colspan="2" style="text-align: left;">Total Hours</th>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <th class="week_total"> </th>
        </tr>
        <tr style="height: 1em; vertical-align: bottom;">
          <td colspan="10">&nbsp;</td>
        </tr>
      </tfoot>
      <tbody id="favourites_grid">
        <tr class="header_row grid_row">
          <th>Favourites</th>
          <th class="desc_field">Activity Description</th>
          <th class="time_field dow_header">Mon</th>
          <th class="time_field dow_header">Tue</th>
          <th class="time_field dow_header">Wed</th>
          <th class="time_field dow_header">Thu</th>
          <th class="time_field dow_header">Fri</th>
          <th class="time_field dow_header">Sat</th>
          <th class="time_field dow_header">Sun</th>
          <td><div style="width: 5em;">&nbsp;</div></td>
        </tr>
        <tr id="new_favourite_row">
            <th><input type="text" name="new_id_field" id="new_id_field" size="4" onkeydown="if (event.keyCode == 13) this.blur();" onblur="newRequestEntered();" title="Enter a work request number to add a row."></th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
        </tr>
      </tbody>
      <tbody id="recent_grid">
        <tr style="height: 1em; vertical-align: bottom;">
          <th colspan="10">&nbsp;</th>
        </tr>
        <tr class="header_row grid_row">
          <th>Recent</th>
          <th class="desc_field">Activity Description</th>
          <th class="time_field dow_header">Mon</th>
          <th class="time_field dow_header">Tue</th>
          <th class="time_field dow_header">Wed</th>
          <th class="time_field dow_header">Thu</th>
          <th class="time_field dow_header">Fri</th>
          <th class="time_field dow_header">Sat</th>
          <th class="time_field dow_header">Sun</th>
          <td><div style="width: 5em;">&nbsp;</div></td>
        </tr>
      </tbody>
      <tfoot id="bottom_totals_grid">
        <tr style="height: 1em; vertical-align: bottom;">
          <td colspan="10">&nbsp;</td>
        </tr>
        <tr class="totals_row" class="grid_row">
          <th colspan="2" style="text-align: left;">Total Hours</th>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <td class="total_field"> </td>
          <th class="week_total"> </th>
        </tr>
      </tfoot>
    </table>
  </div>

  <div id="requests_available" class="wr_list_box">
    <h4>Recently Used Requests</h4>
  </div>

  <div id="standard_requests" class="wr_list_box">
    <h4>Standard Requests</h4>
  </div>

<!--  <div style="float: right; clear: both; padding: 10px;">
    <form action="#" onsubmit="return addRequestSubmit();">
      <p>
        Request ID: <input type="text" id="new_id_field">
        <input type="submit" value="Add" onclick="">
      </p>
    </form>
  </div>-->
</div>

<?php

include("page-footer.php");

?>