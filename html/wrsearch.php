<?php
  include("always.php");
  require_once("authorisation-page.php");
  $session->LoginRequired();

  param_to_global('saved_query');
  param_to_global('format', '#[a-z0-9]+#');
  param_to_global('style', '#[a-z0-9]+#');
  param_to_global('action', '#[a-z0-9]+#');
  param_to_global('submit', '#[a-z0-9]+#i');

  if ( isset($saved_query) && "$saved_query" != "" && "$action" == "delete" ) {
    $q = new PgQuery( "DELETE FROM saved_queries WHERE user_no = '$session->user_no' AND lower(query_name) = lower(?);", $saved_query);
    $q->Exec("wrsearch");
    unset($saved_query);
  }

  require_once("maintenance-page.php");

  // Force some variables to have values.
  param_to_global('qs');
  param_to_global('org_code', 'int');
  param_to_global('system_id', 'int');
  param_to_global('user_no', 'int');
  param_to_global('requested_by', 'int');
  param_to_global('interested_in', 'int');
  param_to_global('allocated_to', 'int');
  param_to_global('search_for');
  param_to_global('type_code');
  param_to_global('inactive');
  param_to_global('from_date');
  param_to_global('to_date');
  param_to_global('where_clause');
  param_to_global('columns');
  param_to_global('incstat');
  param_to_global('taglist_count', 'int');
  param_to_global('tag_list');
  param_to_global('tag_and');
  param_to_global('rlsort');
  param_to_global('rlseq');
  param_to_global('savelist');
  param_to_global('saved_query_order');
  param_to_global('maxresults');


  // Can't just let anyone type in a where clause on the command line!
  if ( ! is_member_of('Admin' ) ) {
    $where_clause = "";
  }

  if ( !isset($default_search_statuses) ) $default_search_statuses = '@NRILKTQADSPZU';

  // If they didn't provide a $columns, we use a default.
  if ( !isset($columns) || $columns == "" || $columns == array() ) {
    if ( $format == "edit" )
      $columns = array("request_id","lfull","request_on","lbrief","status_desc","active","request_type_desc","request.last_activity");
    else
      $columns = array("request_id","lfull","request_on","lbrief","status_desc","request_type_desc","request.last_activity");
  }
  elseif ( ! is_array($columns) )
    $columns = explode( ',', $columns );

  // Internal column names (some have 'nice' alternatives defined in header_row() )
  // The order of these defines the ordering when columns are chosen
  $available_columns = array(
          "request_id" => "WR&nbsp;#",
          "lby_fullname" => "Created By",
          "lfull" => "Request For",
          "request_on" => "Request On",
          "lbrief" => "Description",
          "request_type_desc" => "Type",
          "request_tags" => "Tags",
          "status_desc" => "Status",
          "system_code" => "System Code",
          "system_desc" => "System Name",
          "request.last_activity" => "Last Chng",
          "urgency" => "Urgency",
          "importance" => "Importance",
          "ranking" => "Ranking",
          "request_allocated_to" => "Allocated To",
          "active" => "Active",
   );

  /**
  * The hours column is not visible to clients.
  */
  if ( $session->AllowedTo("Support") || $session->AllowedTo("Admin") ) {
    $available_columns["request_hours"] = "Hours";
  }

  include_once("search_listing_functions.php");
  include_once("search_build_query.php");

  $c->scripts[] = "/js/wrsearch.js";

  include("page-header.php");

  include_once("search_form.php");
  include_once("search_list_results.php");

  include("page-footer.php");

