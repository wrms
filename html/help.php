<?php
  include("always.php");
  require_once("authorisation-page.php");
  $session->LoginRequired();

  $form = "help";
  param_to_global('topic', '#[^\\/\']+#', 'h' );
  param_to_global('submit', '#[a-z0-9]+#i');
  param_to_global('action', '#[a-z0-9_-]+#');

  if ( "$submit" <> "") {
    include("$form-valid.php");
    if ( "$because" == "" ) include("$form-action.php");
  }

  $title = "$system_name - " . ucfirst($form);
  $right_panel = true;
  include("page-header.php");

  include("$form-form.php");

  include("page-footer.php");

