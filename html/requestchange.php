<?php
require("always.php");
require("authorisation-page.php");
require_once("organisation-selectors-sql.php");

  $session->LoginRequired();


param_to_global('org_code','int');
if ( ! is_member_of('Admin','Support') ) {
  $org_code = $session->org_code;
}

$systems = array();
$systems_sql = "SELECT * FROM work_system WHERE active ";
$org_where = '';
$org_url = '';
if ( $org_code > 0 ) {
  $systems_sql .= "AND EXISTS(SELECT 1 FROM org_system WHERE org_code=$org_code AND org_system.system_id = work_system.system_id)";
  $org_where = "AND EXISTS(SELECT 1 FROM usr WHERE org_code=$org_code AND usr.user_no = request.requester_id)";
  $org_url = "&org_code=$org_code";
}
$qry = new PgQuery( $systems_sql . " ORDER BY system_desc ASC" );

if ( !$qry->Exec("requestchange") || $qry->rows == 0 ) {
  $c->messages[] = "Can't find any active systems";
}

// Fetch the systems into an array
while( $row = $qry->Fetch() ) {
  $systems[$row->system_id] = $row ;
}

function add_system_data( $sql, $column ) {
global $systems, $org_where;

  $qry = new PgQuery( $sql . $org_where. " GROUP BY system_id" );
  if ( !$qry->Exec("rqchange") || $qry->rows == 0 ) return;

  while( $row = $qry->Fetch() ) {
    $systems[$row->key]->{$column} = $row->data ;
  }
}

$sql = "SELECT system_id AS key, count(*) AS data FROM request ";
$sql .= "WHERE request_on > 'today'::timestamp - '1 week'::interval";
add_system_data( $sql, 'new_in_week' );

$sql = "SELECT system_id AS key, count(*) AS data FROM request_status INNER JOIN request USING ( request_id ) ";
$sql .= "WHERE status_on > 'today'::timestamp - '1 week'::interval AND status_code = 'F'";
add_system_data( $sql, 'done_in_week' );

$sql = "SELECT work_system.system_id AS key, count(*) AS data FROM work_system ";
$sql .= "JOIN request USING (system_id) ";
$sql .= "WHERE request.active AND last_status != 'F'";
add_system_data( $sql, 'still_active' );

$c->page_title = $c->system_name . " - Request Activity Report";

require("top-menu-bar.php");
require("page-header.php");

if ( is_member_of('Admin','Support') ) {
  $orgsquery = new PgQuery(SqlSelectOrganisations($org_code));
  $orglist = $orgsquery->BuildOptionList($org_code,'usrsearch', array( 'maxwidth' => 50 ));
  $orglist = "<option value=\"\">--- All Organisations ---</option>\n$orglist";
  echo <<<EOFORM
<form method="POST" action="$_SERVER[SCRIPT_NAME]">
<table align="center"><tr valign="middle">
<td class="smb">Organisation:</td><td><select class=sml" name="org_code">$orglist</select></td>
<td><input TYPE="submit" alt="go" class=submit value="GO>>" name="submit"></td>
</tr></table>
</form>
EOFORM;
}


// Now output the collected report.
echo "<table width=\"100%\">\n";
echo "<tr>\n";
echo "<th class=\"cols\" align=\"left\">Code</td>\n";
echo "<th class=\"cols\" align=\"left\">Description</td>\n";
echo "<th class=\"cols\">New</td>\n";
echo "<th class=\"cols\">Done</td>\n";
echo "<th class=\"cols\">Active</td>\n";
echo "</tr>\n";

reset($systems);
$i = 0;
foreach( $systems AS $scode => $sys ) {
  $url1 = "/requestrank.php?system_id=".urlencode($scode)."$org_url&inactive=0&status[N]=1&status[R]=1&status[H]=1&status[C]=1&status[I]=1&status[L]=1&status[T]=1&status[Q]=1&status[A]=1&status[D]=1&status[S]=1&status[P]=1&status[Z]=1";
  $url2 = "/requestlist.php?style=stripped&format=brief&system_id=".urlencode($scode)."$org_url&inactive=0&incstat[N]=1&incstat[R]=1&incstat[H]=1&incstat[C]=1&incstat[I]=1&incstat[L]=1&incstat[T]=1&incstat[Q]=1&incstat[A]=1&incstat[D]=1&incstat[S]=1&incstat[P]=1&incstat[Z]=1";
  printf( "<tr class=\"row\">\n", $i++ % 2);
  echo "<td>$scode</td>\n";
  echo "<td><a href=\"$url1\" target=_new>$sys->system_desc</a></td>\n";
  printf( "<td align=\"center\">%s</td>\n", (isset($sys->new_in_week)?$sys->new_in_week : '-') );
  printf( "<td align=\"center\">%s</td>\n", (isset($sys->done_in_week)?$sys->done_in_week : '-') );
  printf( "<td align=\"center\"><a href=\"$url2\" target=_new>%s</a></td>\n", (isset($sys->still_active)?$sys->still_active : '-') );
  echo "</tr>\n";
}
echo "</table>\n";

include("page-footer.php");
