<?php
  require_once("always.php");
  require_once("authorisation-page.php");
  $session->LoginRequired();
  require_once("maintenance-page.php");

  require_once("OrganisationPlus.php");

  param_to_global('edit', 'int');
  param_to_global('id', 'int');
  $org = new OrganisationPlus($id);


  // form submitted
  if ( isset($_POST['submit']) ) {
    $session->Dbg( "OrgPlus", "Record %s write type is %s.", $org->Table, "insert" );
    $org->PostToValues();
    if ( $org->Validate() ) {
      if ( $org->Write() ) {
        // Reread the record, if it worked
        $org = new OrganisationPlus($id);
      }
    }
  }

  if ( $org->Get('org_code') ) $org_code = $org->Get('org_code');
  require_once("top-menu-bar.php");
  include("page-header.php");

  echo $org->Render();

  include("page-footer.php");
