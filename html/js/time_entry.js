currentDate = new Date();
week_changes = 0;

onLoad(function() {
  // Set up globals
  wrs = [];
  
  $('indicator').count = 0;
  $('indicator').hide();
  
  grid_elements = [$('recent_grid'), $('favourites_grid'), $('requests_available'), $('top_totals_grid'), $('bottom_totals_grid'), $('standard_requests')];
  
  loadFavourites();
  loadRecent();
  loadAvailable();
  updateDayHeadings();
  
  document.observe('click', function(event) {
    var elem = event.element();
    if (!elem) return;
    var path = elem.ancestors();
    if (path.length == 0) return;
    path.unshift(elem);
    
    if (activeRow && !path.member(activeRow)) {
      rowDeactivated(activeRow);
      activeRow = false;
    }
  });
});

/*
* Grid update tracking.
*
* This application involves a number of asynchronous callbacks that modify the
* rows in the grid. To prevent modification of contents, ugly incremental updates
* and the triggering of more events the grid is hidden while these operations
* execute. beginGridUpdate() and endGridUpdate() accomplish this by keeping a
* counter of the number of grid operations underway.
*/
grid_updates_pending = 0;

// Called before all updates to the rows available in the grid.
function beginGridUpdate() {
  grid_updates_pending += 1;
  grid_elements.invoke('hide');
}

// Called after any updates to the rows in the grid are complete.
function endGridUpdate() {
  grid_updates_pending -= 1;
  if (grid_updates_pending == 0) {
    loadTime(function() {
      grid_elements.invoke('show');
    });
  }
}

digitsPattern = /^\d+$/;
hoursMinutesTimeFormat = /^(\d+)*:(\d+)$/;
decimalTimeFormat = /^(\d*)(\d|(\.(\d+)))$/;

/*
* Converts a time string in either HH:MM or decimal form into a floating-point number.
*/
function timeStringToDecimal(str) {
  var match;
  if (decimalTimeFormat.test(str)) {
    return parseFloat(str);
  } else if (match = hoursMinutesTimeFormat.exec(str)) {
    return (parseFloat(match[1]) + (parseFloat(match[2]) / 60.0));
  } else if (str == '') {
    return 0;
  } else {
    return NaN;
  }
}

/*
* This renders a floating-point number in HH:MM form, rounded to the nearest minute.
*/
function decimalToTimeString(time) {
  var hours = String(Math.floor(time));
  var minutes = String(Math.round((time - hours) * 60));
  
  minutes = '00' + minutes;
  minutes = minutes.substring(minutes.length - 2);
  
  return hours + ':' + minutes;
}

// Event callback for when a time field is changed.
// Calls back to the server to store the new time.
function recordTime(event) {
  var elem = event.element();
  
  var decimal_val = timeStringToDecimal(elem.value);
  if (isNaN(decimal_val)) {
    elem.style.backgroundColor = '#fdd';
    return;
  } else {
    elem.style.backgroundColor = '#fff';
  }
  
  elem.server_value = decimal_val;
  elem.datum = elem.value;
  
  var row = elem.up('.entry_row');
  var desc = row.down('.desc_field');
  
  var date = dateForDay(elem.day);
  date = dateToServerString(date);
  
  api_times_record(row.request_id, desc.server_value, decimal_val, {work_on: date});
  updateTotals();
}

// Calls back to the server to change the descriptions of timesheets.
function changeDescription(elem) {
  var desc = elem.value;
  var old_desc = elem.server_value;
  var row = elem.up('.entry_row');
  
  var existing = activityRow(row.request_id, desc);
  if (existing) {
    if (existing.descendantOf($('favourites_grid')) && !row.descendantOf($('favourites_grid'))) {
      var tmp = row;
      row = existing;
      existing = tmp;
      rowActivated.defer(row);
    }
    new_times = row.getElementsBySelector('.time_field');
    old_times = existing.getElementsBySelector('.time_field');
    
    new_times.zip(old_times, function(p) {
      p[0].server_value += p[1].server_value;
      p[0].datum = p[0].server_value == 0 ? '' : decimalToTimeString(p[0].server_value);
      p[0].value = p[0].datum;
      p[0].innerHTML = p[0].datum;
    });
    
    existing.remove();
    colourRows($('favourites_grid'));
    colourRows($('recent_grid'));
  }
  
  api_activities_change_description(row.request_id, old_desc, desc);
  
  row.work_description = desc;
  elem.server_value = desc; // More likely to succeed if these happen in parallel
  elem.datum = desc;
}

// Load the list of recent timesheets.
// They will be inserted as table rows in the table 'recent_grid'
function loadRecent() {
  var changes_on_start = week_changes;
  beginGridUpdate();
  api_activities_recent(
    {
      max_age: 2,
      for_date: dateToServerString(currentDate)
    },
    function(activities) {
      // Protect against async callbacks after view change
      if (week_changes == changes_on_start) {
        sortRecentByRequest(activities).each(function(activity) {
          if (!activityRow(activity['request_id'], activity['work_description'])) {
            
            var row = create_row(activity.request_id, activity.work_description, activity.brief);
            $('recent_grid').insert(row);
            row.getElementsBySelector('.recent_only').invoke('show');
          }
        });
        colourRows($('recent_grid'));
      }
      endGridUpdate();
    }
  );
}

// Load the user's selected timesheets.
// They will be inserted as table rows in the table 'favourites_grid'
function loadFavourites() {
  beginGridUpdate();
  api_activities_favourite(function(activities) {
    activities.each(function(activity) {
      var row = activityRow(activity['request_id'], activity['work_description']);
      if (row) {
        row.remove();
        row.getElementsBySelector('.recent_only').invoke('hide');
        row.getElementsBySelector('.favourite_only').invoke('show');
        insertFavourite(row, false, true);
      } else {
        var row = create_row(activity.request_id, activity.work_description, activity.brief);
        insertFavourite(row, false, true);
        row.getElementsBySelector('.favourite_only').invoke('show');
      }
    });
    colourRows($('recent_grid'));
    colourRows($('favourites_grid'));
    endGridUpdate();
  });
}

function create_available_row(request_id, desc) {
  var entry = new Element('tr', {class: 'wr_list_entry'});
  
  entry.request_id = request_id;
  entry.description = desc;
  
  var img = new Element('img', {src: image_urls.add, style: 'height: 1em;', class: 'button', title: 'Add to favourites'});
  entry.insert(img.wrap(new Element('td')));
  img.observe('click', function(event) {
    var row = create_row(request_id, '', desc);
    row.getElementsBySelector('.favourite_only').invoke('show');
    insertFavourite(row, true);
    var selectRow = function() {
      row.clickObserver();
      row.down('.desc_field').focus();
    };
    
    selectRow.defer();
  });
  
  var cell = new Element('td');
  cell.innerHTML = ' WR#' + request_id;
  entry.insert(cell);
  
  cell = new Element('td');
  cell.innerHTML = desc;
  entry.insert(cell);
  
  return entry;
}

// Load a longer list of available timesheets.
// These will be listed at the bottom for easy addition.
function loadAvailable() {
  var changes_on_start = week_changes;
  beginGridUpdate();
  api_requests_recent(
    {
      max_age: 6,
      for_date: dateToServerString(currentDate)
    },
    function(avail) {
      if (week_changes == changes_on_start) {
        var available_requests = avail.map(function(req) {
          // Add an available request
          var id = req['request_id'];
          var desc = req['brief'];
          
          return create_available_row(id, desc);
        });
        fillAvailableColumns($('requests_available'), available_requests);
      }
      endGridUpdate();
    }
  );
}

// Update the row containing the totals for each day.
function updateTotals() {
  var counts = $$('.entry_row').map(function(row) {
    return row.getElementsBySelector('.time_field').pluck('datum').map(timeStringToDecimal);
  });
  var row1 = counts.shift();
  var totals;
  if (row1) {
    var values = row1.zip.apply(row1, counts);
    totals = values.map(enumSum);
  } else {
    totals = [0,0,0,0,0,0,0];
  }
  
  $$('.totals_row').each(function(trow) {
    trow.getElementsBySelector('.total_field').zip(totals, function(tuple) {
      tuple[0].innerHTML = '<b>' +  decimalToTimeString(tuple[1]) + '</b>';
    });
    trow.down('.week_total').innerHTML = decimalToTimeString(enumSum(totals));
  });
}

// Rearrange the columns in the available requests list to flow between them.
function fillAvailableColumns(container, rows, column_size, max_cols) {
  if (!column_size) column_size = 5;
  if (!max_cols) max_cols = 2;
  
  container.getElementsBySelector('.available_requests_column').invoke('remove');
  
  rows.eachSlice(column_size, function(slice) {
    if (max_cols == 0) return;
    max_cols -= 1;
    var col = new Element('div', {class: 'available_requests_column'});
    var table = new Element('table');
    container.insert(table.wrap(col));
    
    slice.each(function(entry) {
      table.insert(entry);
    });
  });
}

// Load the current week's time and update the table.
function loadTime(on_complete) {
  var changes_on_start = week_changes;
  api_times_week(dateToServerString(currentDate),function(data) {
    // Protect against async callbacks after view change
    if (week_changes != changes_on_start) return;
    
    data.each(function(entry) {
      var row = activityRow(entry['request_id'], entry['work_description']);
      
      if (row) {
        var day_short = entry['day'].slice(0, 3);
        var time_entry = row.getElementsBySelector(".time_field").find(function(field) {
          return field.day == day_short;
        });
        
        if ((! time_entry.server_value) || (time_entry.server_value != entry['hours'])) {
          time_entry.datum = decimalToTimeString(entry['hours']);
          time_entry.value = time_entry.datum;
          time_entry.innerHTML = time_entry.datum;
          time_entry.server_value = entry['hours'];
        }
      }
    });
    updateTotals();
    
    if (on_complete) on_complete();
  });
}

// Find the document row for a request id and description.
function activityRow(id, desc, row_selector) {
  if (!row_selector) row_selector = '.entry_row';
  return $$(row_selector).find(function(row) {
    return row.request_id == id && row.down('.desc_field').server_value == desc;
  });
}

// Create a new activity row.
function create_row(wr, desc, request_brief) {
  var row = new Element("tr");
  row.addClassName("entry_row");
  row.request_id = wr;
  row.work_description = desc;
  row.brief = request_brief;
  
  row.clickObserver = rowActivated.curry(row);
  row.observe('click', row.clickObserver);
  
  var cell;
  var input_name;
  
  var link = new Element("a", {href: "/wr.php?request_id=" + wr, title: request_brief});
  link.innerHTML = wr;
  row.insert(link.wrap(new Element('th')));
  
  cell = new Element("td");
  row.insert(cell);
  var desc_span = createDataSpan({class: "desc_field", datum: desc, server_value: desc});
  cell.insert(desc_span);
  
  // days in util.js
  days.each(function(day) {
    cell = new Element("td");
    row.insert(cell);
    
    var time_field = createDataSpan({class: "time_field", datum: '', server_value: 0});
    cell.insert(time_field);
    
    time_field.day = day;
    time_field.observe('change', recordTime);
  });
  
  cell = new Element("td");
  row.insert(cell);
  
  var add_button = new Element('img', {class: 'button recent_only', src: image_urls.add, style: 'width: 1em; height: 1em;', title: 'Add to favourites'});
  add_button.hide();
  cell.insert(add_button);
  
  add_button.observe('click', addButtonClick.curry(row));
  
  var remove_button = new Element('img', {class: 'button favourite_only', src: image_urls.remove, style: 'width: 1em; height: 1em;', title: 'Remove favourite'});
  remove_button.hide();
  cell.insert(remove_button);
  
  remove_button.observe('click', removeButtonClick.curry(row));
  
  var drag_button = new Element('img', {class: 'button favourite_only', src: image_urls.move, style: 'width: 1em; height: 1em;', title: 'Reorder favourites'});
  drag_button.hide();
  cell.insert(drag_button);
  drag_button.observe('mousedown', function(event) {
    event.preventDefault();
    row.addClassName('dragging');

    var lastDetails = {y: 0};
    var drag = function(ev) {
      var elem = ev.element();
      if (!elem.match('.entry_row')) elem = elem.up('.entry_row');
      if (elem && elem.descendantOf($('favourites_grid'))) {
        if (elem != row) {
          row.remove();
          if (lastDetails.y > ev.clientY) {
            // moving up
            elem.insert({before: row});
          } else {
            // moving down
            elem.insert({after: row});
          }
        }
      }
      lastDetails.y = ev.clientY;
    };

    var stopDrag;
    stopDrag = function(ev) {
      document.stopObserving('mousemove', drag);
      document.stopObserving('mouseup', stopDrag);
      row.removeClassName('dragging');
      recordFavourites();
      colourRows($('favourites_grid'));
    };

    document.observe('mousemove', drag);
    document.observe('mouseup', stopDrag);
  });
  
  return row;
}

function addButtonClick(row, event) {
  row.remove();
  insertFavourite(row, true);
  row.getElementsBySelector('.recent_only').invoke('hide');
  row.getElementsBySelector('.favourite_only').invoke('show');
  
  recordFavourites();
  colourRows($('recent_grid'));
}

function removeButtonClick(row, event) {
  row.remove();
  
  var prev_row = $('recent_grid').getElementsBySelector('.entry_row').reverse().find(function(r) {
    return r.request_id == row.request_id
  });
  if (prev_row) {
    prev_row.insert({after: row});
  } else {
    $('recent_grid').insert(row);
  }
  
  row.getElementsBySelector('.favourite_only').invoke('hide');
  row.getElementsBySelector('.recent_only').invoke('show');
  
  recordFavourites();
  colourRows($('favourites_grid'));
  colourRows($('recent_grid'));
}

/* Document-scope record of a single (or no) active row */
activeRow = false;

/*
* This is triggered when a row is clicked.
* It deselects the previous active row and makes this one active.
* All the data fields are made editable.
*/
function rowActivated(row, event) {
  row.stopObserving('click', row.clickObserver);
  if (activeRow && activeRow.descendantOf(document)) {
    rowDeactivated(activeRow);
  }
  activeRow = row;
  
  var desc_span = row.down('.desc_field');
  var time_spans = row.getElementsBySelector('.time_field');
  
  var desc_field = createDataField(desc_span);
  desc_field.observe('blur', function(event) {
    if (desc_field.value.blank()) {
      row.remove();
    } else if (desc_field.server_value.blank()) {
      changeDescription(desc_field);
      if (desc_field.descendantOf($('favourites_grid'))) recordFavourites.defer();
    } else if (desc_field.value != desc_field.server_value) {
      changeDescription(desc_field);
    }
  });
  
  var time_fields = time_spans.map(createDataField)
  time_fields.invoke('observe', 'change', recordTime);
  
  if (event) {
    var c = event.findElement('td');
  }
  
  desc_span.replace(desc_field);
  time_spans.zip(time_fields, function(t) {t[0].replace(t[1])});
  
  if (c) {
    var field = c.down('input');
    if (field) {
      field.focus();
      field.select();
    }
  }
}

/*
* This is triggered when the active row is deactivated.
* It converts all the text inputs to data spans until next time the row is activated.
*/
function rowDeactivated(row) {
  var desc_field = row.down('.desc_field');
  if (!desc_field) return;
  var desc_span = createDataSpan(desc_field);
  desc_field.up().insert(desc_span);
  desc_field.remove();
  
  var time_fields = row.getElementsBySelector('.time_field');
  var time_spans = time_fields.map(createDataSpan)
  time_fields.zip(time_spans, function(t) {t[0].up().insert(t[1]); t[0].remove()});
  
  row.observe('click', row.clickObserver);
}

/*
* This creates a span out of a text input, preserving all classes and contents.
*/
function createDataSpan(source) {
  if (!source) return;
  var span = new Element('div');
  if (source.class) span.addClassName(source.class);
  if (source.classNames) source.classNames().each(span.addClassName.bind(span));
  span.datum = source.datum;
  span.innerHTML = source.datum;
  span.server_value = source.server_value;
  if (source.day) span.day = source.day;
  return span;
}

/*
* This creates a text field out of a data span, preserving all classes and data.
* The span must have a 'value' attribute containing the data to be used.
*/
function createDataField(source) {
  if (!source) return;
  var field = new Element('input', {type: 'text'});
  if (source.class) field.addClassName(source.class);
  if (source.classNames) source.classNames().each(field.addClassName.bind(field));
  field.value = source.datum;
  field.datum = source.datum;
  field.server_value = source.server_value;
  if (source.day) field.day = source.day;
  return field;
}

// Callback for the add request form.
function newRequestEntered() {
  field = $('new_id_field');
  if (field.value.length > 0) {
    if (digitsPattern.test(field.value)) {
      field.style.backgroundColor = '#fff';
    
      var row = create_row(parseInt(field.value), '', '');
      insertFavourite(row, true);
      
      row.clickObserver();
      row.down('.desc_field').focus();
  
      field.value = '';
      
      row.getElementsBySelector('.favourite_only').invoke('show');
      row.down('.desc_field').focus();
      loadTime();
    } else {
      field.style.backgroundColor = '#faa';
    }
  }
  return false;
}

/*
* This inserts a favourite row in the favourites grid.
* If the second parameter is true is will try to insert below rows
* of the same request.
*/
function insertFavourite(row, below_same, no_colour) {
  var prev_row;
  prev_row = activityRow(row.request_id, row.work_description, '#favourites_grid .entry_row')
  if (prev_row) {
    prev_row.replace(row);
    return;
  }
  
  if (below_same) {
    prev_row = $('favourites_grid').getElementsBySelector('.entry_row').reverse().find(function(r) {
      return r.request_id == row.request_id
    });
    if (prev_row) {
      prev_row.insert({after: row});
    } else {
      $('new_favourite_row').insert({before: row});
    }
  } else {
    $('new_favourite_row').insert({before: row});
  }
  if (!no_colour) colourRows($('favourites_grid'));
}

// Send the current list of favourites back to the server to be stored.
function recordFavourites() {
  var favourites = [];
  $('favourites_grid').getElementsBySelector('.entry_row').each(function(row) {
    favourites.push({
      request_id: row.request_id,
      work_description: row.down('.desc_field').server_value
    });
  });
  
  api_activities_set_favourites(favourites);
}

/*
* This changes the currently viewed week.
* It hides the grid until data has been loaded.
* The parameter specifies a day the week of which will be loaded.
*/
function changeWeek(day) {
  week_changes += 1;
  currentDate = day;
  beginGridUpdate();
  $('recent_grid').getElementsBySelector('.entry_row').invoke('remove');
  $('favourites_grid').getElementsBySelector('.entry_row').each(function(row) {
    row.getElementsBySelector('.time_field').each(function(field) {
      field.datum = '';
      field.value = '';
      field.innerHTML = '';
      field.server_value = 0;
    });
  });
  updateDayHeadings();
  loadAvailable();
  loadRecent();
  endGridUpdate();
}

/*
* Updates the dates in the column headings and top header to the current week.
*/
function updateDayHeadings() {
  var start = dateForDay('mon');
  var end = dateForDay('sun');
  var week_start = months[start.getMonth()] + ' ' + start.getDate();
  if (start.getFullYear() != end.getFullYear()) week_start += ' ' + start.getFullYear();
  var week_end = months[end.getMonth()] + ' ' + end.getDate() + ' ' + end.getFullYear();
  $('week_heading').innerHTML = week_start + ' - ' + week_end;
  $$('.dow_header').each(function(header) {
    var hday = header.innerHTML.strip().slice(0, 3).toLowerCase();
    var date = dateForDay(hday);
    header.innerHTML = hday.capitalize() + "<br/>" + date.getDate();
  });
}

/*
* This groups together activities of the same request number.
* The requests are ordered by most recently worked on first.
*/
function sortRecentByRequest(requests) {
  var indices = new Hash();
  var reqs_2d = [];
  requests.each(function(req) {
    if (!indices.keys().include(req.request_id)) {
      indices.set(req.request_id, reqs_2d.length);
      reqs_2d.push([]);
    }
    reqs_2d[indices.get(req.request_id)].push(req);
  });
  return reqs_2d.flatten();
}

function previousWeek() {
  var day = new Date(currentDate);
  day.setTime(day.getTime() - 1000 * 60 * 60 * 24 * 7);
  changeWeek(day);
}

function nextWeek() {
  var day = new Date(currentDate);
  day.setTime(day.getTime() + 1000 * 60 * 60 * 24 * 7);
  changeWeek(day);
}

/*
* Get the date on the current week for a given day (i.e. monday, tuesday, etc)
* Only the first three letters count.
*/
function dateForDay(day) {
  var date = new Date(currentDate);
  var current_day = date.getDay();
  if (current_day == 0) current_day = 7; // Shift Sunday to last
  current_day -= 1;
  
  // days defined in util.js
  var diff = days.indexOf(day.slice(0, 3).toLowerCase()) - current_day;
  date.setTime(date.getTime() + 1000 * 60 * 60 * 24 * diff);
  return date;
}

row_colours = ["#eee", "#ccc"];

/*
* This colours a series of activity rows in alternating colours.
* It only changes colours when the request number changes.
*/
function colourRows(grid) {
  var lid = -1;
  var cindex = 0;
  grid.getElementsBySelector('.entry_row').each(function(row) {
    if (lid != row.request_id) {
      cindex = (cindex + 1) % row_colours.length;
      lid = row.request_id;
      row.down('th').down('a').show();
    } else {
      row.down('th').down('a').hide();
    }
    row.style.backgroundColor = row_colours[cindex];
  });
}
