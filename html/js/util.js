startCalls = [];
days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

// Register a function to be run when the page is fully loaded.
function onLoad(callback) {
  startCalls.push(callback);
}

// Called on start. Call all registered callbacks.
function setup() {
  startCalls.each(function(callback) {callback();});
}
window.onload = setup;

function dateToServerString(date) {
  return String(date.getFullYear()) + '-' + String(date.getMonth() + 1) + '-' + String(date.getDate());
}

// Send an XMLHTTPRequest back with some default options.
function runRequest(url, opts) {
  var indicator = $('indicator');
  
  opts = new Hash(opts);
  
  var origOnFailure = opts.get('onFailure');
  var onError = opts.get('onError');
  var onFailure = function(transport) {
    alert(transport.responseText);
    if (onError) {
      onError(transport.responseText);
    } else if (origOnFailure) {
      origOnFailure(transport);
    }
  };
  
  opts.set('onFailure', onFailure);
  opts.set(405, onFailure);
  
  var origOnLoading = opts.get('onLoading');
  opts.set('onLoading', function(transport) {
    indicator.count += 1;
    indicator.show();
    if (origOnLoading) origOnLoading(transport);
  });
  
  var origOnComplete = opts.get('onComplete');
  opts.set('onComplete', function(transport) {
    indicator.count -= 1;
    if (indicator.count == 0) indicator.hide();
    if (origOnComplete) origOnComplete(transport);
  });
  
  opts.set('onSuccess', handleResponse.curry(opts.get('onSuccess'), onError));
  
  return new Ajax.Request(url, opts.toObject());
}

function handleResponse(callback, failCallback, transport) {
  
  if (transport.responseJSON) {
    if (callback) callback(transport.responseJSON);
  } else {
    var text = transport.responseText;
    if (text.startsWith("ERROR ")) {
      if (failCallback) {
        failCallback(text);
      } else {
        alert(text);
      }
      return;
    }
    if (callback) callback(text);
  }
}

function pointWithin(offset, width, height, x, y) {
  return x >= offset.left && x < offset.left + width && y >= offset.top && y < offset.top + height;
}

function enumSum(array) {
  return array.inject(0, function(a,b) {return a+b;});
}