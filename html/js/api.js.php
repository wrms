<?php
  require_once('api/api_util.php');
  require_once('caching.php');
  
  header('Content-type: application/javascript');
  
  # Calculate last modified time
  $registry = realpath(dirname(__FILE__)) . '/../../inc/api/registry.php';
  $util = realpath(dirname(__FILE__)) . '/../../inc/api/api_util.php';
  $self = $_SERVER['SCRIPT_FILENAME'];
  $mtime = check_fmtime($registry, $util, $self);
  check_etag($_SERVER['SCRIPT_FILENAME'] . strval($mtime));

  foreach (api_method_list() as $method) {
    $arguments = array();
    $setup_lines = array();
    $method_name = str_replace('/', '_', $method['name']);
    $param_init = '';
    
    $call_url = '\'api.php/' . $method['name'] . '\'';
    
    if ($method['accept_path'] === true) {
      $arguments[] = "_path";
      $call_url .= ' + \'/\' + _path.join(\'/\')';
    } elseif ($method['accept_path'] === false) {
      # Do nothing
    } else {
      # Add a series of arguments
      for ($i = 0; $i < $method['accept_path']; $i++) {
        $arguments[] = "_path_$i";
        $call_url .= " + '/' + _path_$i";
      }
    }
    
    foreach($method['required'] as $param) {
      $param = explode('::', $param, 2);
      $param = $param[0];
      $arguments[] = $param;
      $setup_lines[] = "  params.set('$param', $param);";
    }
    
    if (count($method['optional']) > 0 || $method['accept_any']) {
      $arguments[] = '_extras';
      $param_init = '_extras';
    }
    
    $arguments[] = '_callback';
    
    echo "function api_" . $method_name . '(' . join(', ', $arguments) . ") {\n";
    echo "  var params = new Hash(" . $param_init . ");\n";
    echo join("\n", $setup_lines);
    echo "\n\n  runRequest(" . $call_url . ", {\n";
    if ($method['method'] == 'get') {
      echo "    method: 'get',\n    parameters: params.toObject()";
    } else {
      echo "    postBody: params.toJSON()";
    }
    echo ",\n    onSuccess: _callback\n  });\n}\n\n";
  }
?>