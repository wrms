<?php
/* ******************************************************************** */
/* CATALYST PHP Source Code                                             */
/* -------------------------------------------------------------------- */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with this program; if not, write to:                           */
/*   The Free Software Foundation, Inc., 59 Temple Place, Suite 330,    */
/*   Boston, MA  02111-1307  USA                                        */
/* -------------------------------------------------------------------- */
/*                                                                      */
/* Filename:    qams-step-approve.php                                   */
/* Author:      Paul Waite                                              */
/* Description: QAMS step approve page                                  */
/*                                                                      */
/* ******************************************************************** */
require_once("always.php");
require_once("authorisation-page.php");

$session->LoginRequired();

require_once("maintenance-page.php");

$title = "QAMS Step Approval";

// -----------------------------------------------------------------------------------------------
include_once("qams-project-defs.php");

// -----------------------------------------------------------------------
// FUNCTIONS
function ContentForm($ef, &$project, &$qastep, $ap_type_id) {
  global $session;

  // Allow approval editing for assigned user - ie. the user being
  // asked to approve the step, and administrators of course.
  $have_admin = $project->qa_process->have_admin;
  $qastep->get_approvals();
  if ($have_admin) {
    $user_editmode = true;
  }
  else {
    if (isset($qastep->approvals[$ap_type_id])) {
      $approval = $qastep->approvals[$ap_type_id];
      $user_editmode = ($approval->assigned_to_usr == $session->user_no
                     && $approval->approval_status != "y"
                     );
    }
  }


  $s = "";
  $s .= "<table cellspacing=\"2\" cellpadding=\"2\" width=\"100%\">\n";

  $s .= "<tr class=\"row0\">";
  $s .= "<td colspan=\"2\" style=\"padding-left:20px;padding-right:50px\">"
     . "<p>This screen allows you to post an approval decision for the given step of the project.</p>"
     . "<p>If there are any documents for you to read or other files to consider, then these will be "
     . "found attached at the bottom of this page. <b><u>You should read these thoroughly</u></b> before "
     . "posting your decision.</p>"
     . "<p>When ready, select your chosen approval response and add any extra covering notes "
     . "as required. When you have finished click on the 'Post Decision' button to register it.</p>"
     . "<p>Thanks for participating in our QA program.</p>"
     . "</td>";
  $s .= "</tr>\n";

  // Vertical spacer
  $s .= "<tr class=\"row0\">";
  $s .= "<td colspan=\"2\" height=\"15\">&nbsp;</td>";
  $s .= "</tr>\n";

  // Project brief
  $s .= "<tr class=\"row1\">";
  $s .= "<th width=\"40%\" class=\"prompt\"><b>Project:</b> </th>";
  $s .= "<td width=\"60%\">$project->brief</td>";
  $s .= "</tr>\n";

  // Step description
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Step to approve:</b> </th>";
  $s .= "<td>" . $qastep->qa_step_desc . "</td>";
  $s .= "</tr>\n";

  // Step notes
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Notes:</b> </th>";
  $s .= "<td>" . $qastep->qa_step_notes . "</td>";
  $s .= "</tr>\n";

  // Special notes
  if ( isset($qastep->special_notes) ) {
    $s .= "<tr class=\"row1\">";
    $s .= "<th class=\"prompt\"><b>Special notes:</b> </th>";
    $s .= "<td>" . $qastep->special_notes . "</td>";
    $s .= "</tr>\n";
  }

  // Required approvals list
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Current status:</b> </th>";
  $s .= "<td>" . $qastep->render_approval_types(false, true, $ap_type_id) . "</td>";
  $s .= "</tr>\n";

  $pmmailto = "<a href=\"mailto:$project->project_manager_email\">"
            . "E-Mail the Project Manager</a>";

  // Warning when changing approval status of already-approved step
  if ($user_editmode && isset($qastep->approvals[$ap_type_id]) && $qastep->approvals[$ap_type_id]->approval_status == "y") {
    $s .= "<tr class=\"row1\">";
    $s .= "<th>&nbsp;</th>";
    $s .= "<td style=\"padding-right:30px\">"
            . "<p class=\"error\" style=\"margin-bottom:0px\">"
            . "WARNING: THIS HAS ALREADY BEEN APPROVED</p>"
            . "<p class=\"helpnote\" style=\"margin-top:0px\">"
            . "However, you can still change the approval status here, if that is what "
            . "you think is required. If you are not sure, or think this is an error then "
            . "please " . $pmmailto . ", and let him know about the problem.</p>"
            . "</td>";
    $s .= "</tr>\n";
  }

  // Approval decision. We only allow the assigned user to see the
  // form elements for doing this, plus QA admin folks
  if ($user_editmode) {

    $F  = "<select size=\"1\" name=\"new_approval_status\">";
    $F .= "<option value=\"\">-- select an approval decision --</option>\n";
    $F .= "<option value=\"y\">Approve this step</option>\n";
    $F .= "<option value=\"n\">Refuse approval for this step</option>\n";
    if ($have_admin) {
      $F .= "<option value=\"s\">Skip approval for this step</option>\n";
    }

    $s .= "<tr class=\"row1\">";
    $s .= "<th class=\"prompt\"><b>Post an approval:</b> </th>";
    $s .= "<td>" . $F . "</td>";
    $s .= "</tr>\n";

    $F  = "<textarea name=\"approval_covernotes\" style=\"width:400px;height:150px\">";
    $F .= "</textarea>";
    $s .= "<tr class=\"row1\">";
    $s .= "<th class=\"prompt\"><b>Covering notes:</b> </th>";
    $s .= "<td>" . $F . "</td>";
    $s .= "</tr>\n";
  }

  // Vertical spacer
  $s .= "<tr class=\"row0\">";
  $s .= "<th>&nbsp;</th>";
  $s .= "<td style=\"padding-right:30px\">"
      . "<p class=\"helpnote\" style=\"margin-top:0px\">"
      . "If you are not sure what to do with this approval request, or have any other "
      . "questions about it please " . $pmmailto . ", to clarify things.</p>"
      . "</td>";
  $s .= "</tr>\n";

  // Vertical spacer
  $s .= "<tr class=\"row0\">";
  $s .= "<td colspan=\"2\" height=\"15\">&nbsp;</td>";
  $s .= "</tr>\n";

  // Attachments
  $s .= $qastep->RenderAttachments($ef);

  $s .= "</table>\n";
  return $s;

} // ContentForm

// -----------------------------------------------------------------------------------------------
// MAIN CONTENT

// Must haves..
if (!isset($step_id) || !isset($ap_type_id)) {
  exit;
}

// Get the step
$qastep = new qa_project_step();
$qastep->get($step_id);

if ($qastep->valid) {
  // Project object to work with..
  $project = new qa_project($qastep->project_id);
  $have_admin = $project->qa_process->have_admin;
}
else {
  unset($qastep);
}

// ---------------------------------------------------------------------
$s = "";
if (isset($qastep)) {

  // PROCESS POSTED UPDATES..
  if (isset($submit) && $submit == "Post Decision") {
    if ($project->request_id > 0 && $ap_type_id != "" && $new_approval_status != "") {
      // Where are we currently..
      $orig_overall_status = $qastep->overall_approval_status();

      // Write the approval history record..
      $qastep->approve($ap_type_id, $new_approval_status, $approval_covernotes);

      $s = "";
      $display_status = strtoupper(qa_approval_status($new_approval_status));
      $subject = "QAMS Approval: $qastep->qa_step_desc [$display_status/$project->system_id/$session->username]";

      // Assemble body for approver..
      $s .= "<p>The Quality Assurance Step '$qastep->qa_step_desc' has had an Approval ";
      $s .= "posted to it by $session->fullname.</p>";

      $s .= "<p>The decision was: <b><u>$display_status</u></b>.</p>";

      // Statement of resulting overall status..
      $new_overall_status = $qastep->overall_approval_status();
      $s .= "<p>The overall approval status of the Step ";
      if ($new_overall_status == $orig_overall_status) {
        $s .= "remains as";
      }
      else {
        $s .= "has now changed to";
      }
      $s .= " <b>" . qa_approval_status($new_overall_status) . "</b>.";
      $s .= "</p>";

      // Make special note of overriden approval - this is usually because the
      // person originally requested for it can't do it for some lame reason
      // so the project manager ends up doing it manually
      if ($qastep->approval_overridden($ap_type_id)) {
        $s .= "<p>NB: the approval was manually posted (overridden). ";
        if ( $qastep->approvals[$ap_type_id]->assigned_fullname != "") {
          $s .= $qastep->approvals[$ap_type_id]->assigned_fullname . " was originally requested to approve this.</p>";
        }
      }

      // Step details link..
      $s .= "<p>The details for this QA step are available here:<br>";
      $href = $URL_PREFIX . "/qams-step-detail.php?step_id=$qastep->qa_project_step_id";
      $desc = "Quality Assurance Step Details for: $qastep->qa_step_desc";
      $stlink = "<a href=\"$href\">$desc</a>";
      $s .= "&nbsp;&nbsp;" . $stlink . "</p>";

      // Covering notes..
      if ($approval_covernotes != "") {
        $s .= "<p><b>The approver also noted:</b><br>";
        $s .= $approval_covernotes . "</p>";
      }
      $project->QAMSNotifyEmail("Approval Post Advice", $s, $subject);

      // Now re-direct them where they can see the project summary..
      header("Location: /qams-project.php?request_id=$project->request_id");
      exit;
    }
  }

  require_once("top-menu-bar.php");
  require_once("page-header.php");

  // Main content..
  $s = "";
  $ef = new EntryForm($_SERVER['REQUEST_URI'], $project, ($have_admin ? 1 : 0));
  $ef->NoHelp();

  $s .= $ef->StartForm();
  $s .= $ef->HiddenField( "qa_action", "$qa_action" );
  if ( $project->request_id > 0 ) {
    $s .= $ef->HiddenField( "project_id", $project->request_id );
    $s .= $ef->HiddenField( "step_id", "$qastep->qa_project_step_id" );
    $s .= $ef->HiddenField( "ap_type_id", "$ap_type_id" );
  }
  // Start main table..
  $s .= "<table width=\"100%\" class=\"data\" cellspacing=\"0\" cellpadding=\"0\">\n";
  $s .= $ef->BreakLine("Quality Assurance Approval");
  $s .= "<tr><td height=\"15\" colspan=\"2\">&nbsp;</td></tr>";
  $s .= "<tr><td colspan=\"2\">" . ContentForm($ef, $project, $qastep, $ap_type_id) . "</td></tr>";
  $s .= "<tr><td height=\"15\" colspan=\"2\">&nbsp;</td></tr>";
  $s .= "</table>\n";

  $s .= $ef->SubmitButton( "submit", "Post Decision" );
  $s .= $ef->EndForm();

} // isset qastep

// -----------------------------------------------------------------------------
// ASSEMBLE CONTENT
if ($s == "") {
  $content = "<p>Nothing known about that QA step.</p>";
}
else {
  $content = $s;
}

// -----------------------------------------------------------------------------
// DELIVER..
echo $content;

include("page-footer.php");
