<?php
/* ******************************************************************** */
/* CATALYST PHP Source Code                                             */
/* -------------------------------------------------------------------- */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with this program; if not, write to:                           */
/*   The Free Software Foundation, Inc., 59 Temple Place, Suite 330,    */
/*   Boston, MA  02111-1307  USA                                        */
/* -------------------------------------------------------------------- */
/*                                                                      */
/* Filename:    qams-project.php                                        */
/* Author:      Paul Waite                                              */
/* Description: QAMS project maintain/view                              */
/*                                                                      */
/* ******************************************************************** */
require_once("always.php");
require_once("authorisation-page.php");

$session->LoginRequired();

require_once("maintenance-page.php");

$title = "QAMS Project";

// -----------------------------------------------------------------------------------------------
// MAIN CONTENT
include_once("qams-project-defs.php");

// New project object to work with..
$project = new qa_project($request_id);

require_once("top-menu-bar.php");
require_once("page-header.php");

switch ($qa_action) {
  case "reset":
  case "reapprove":
    if (isset($step_id)) {
      if (isset($project->qa_process->qa_steps[$step_id])) {
        $qastep = $project->qa_process->qa_steps[$step_id];
        $qastep->reapprove();

        // Only tell everyone what's going on if we are re-approving an approved
        // QA Step. For 'resets' of in-progress steps, we don't bother.
        if ($qa_action == "reapprove" && $qastep->overall_approval_status() == "") {
          $href = $URL_PREFIX . "/qams-step-detail.php";
          $href = href_addparm($href, "step_id", $qastep->qa_project_step_id);
          $link = "<a href=\"$href\">$href</a>";

          $subject = "QAMS Re-Approval: $qastep->qa_step_desc [$project->system_id/$session->username]";
          $s  = "<p>The QA Step '$qastep->qa_step_desc' has now been 'Unapproved' so that it can ";
          $s .= "go through the approval process once again.</p>";
          // Step details link..
          $s .= "<p>The details for this QA step are available here:<br>";
          $href = $URL_PREFIX . "/qams-step-detail.php?step_id=$qastep->qa_project_step_id";
          $desc = "Quality Assurance Step Details for: $qastep->qa_step_desc";
          $stlink = "<a href=\"$href\">$desc</a>";
          $s .= "&nbsp;&nbsp;" . $stlink . "</p>";
          $project->QAMSNotifyEmail("QAMS Activity Notice", $s, $subject);
        }
      }
    }
    $content = $project->RenderQAPlan();
    break;

  // Re-ordering of QA Plan steps within Phase
  case "move_up":
  case "move_down":
    if (isset($step_id)) {
      $project->qa_process->reorder_step($step_id, $qa_action);
    }
    $content = $project->RenderQAPlan();
    break;

  // View the QA Plan
  case "qaplan":
    $content = $project->RenderQAPlan();
    break;

  default:
    $content = $project->project_details($edit);
} // switch

// -----------------------------------------------------------------------------------------------
// DELIVER..

echo $content;

include("page-footer.php");
?>