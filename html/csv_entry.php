<?php

require_once('always.php');
require_once('api/all.php');
require_once('parse_csv.php');
require_once('api/timesheets.php');

require_once("authorisation-page.php");
$session->LoginRequired();
include("tidy.php");

require_once("top-menu-bar.php");
require_once("page-header.php");

# Parse as either TSV (simple separators) or CSV (including quoting, etc)
function parse_ctsv($data) {
  # Find a tab outside quotes on first line (number of quotes to the left is even)
  $tsv_pattern = '/\A([^"\n]*"[^"\n]*")*[^"\n]*\t/';
  if (preg_match($tsv_pattern, $data)) {
    # Data is TSV - split lines and fields in a simple fashion
    $tlines = explode("\n", $data);
    $lines = array();
    foreach ($tlines as $tline) {
      if (strlen($tline) > 0) $lines[] = explode("\t", $tline);
    }
    return $lines;
  } else {
    # Data is CSV - start full parser
    return parse_csv($data);
  }
}

function parse_hhmm($str) {
  if (strpos(':', $str) == false) {
    list($hours, $minutes) = array_map('intval', explode(':', $str));
    return $hours + ($minutes / 60.0);
  } else {
    $duration = floatval($str);
    return $duration;
  }
}

function decimal_to_hhmm($decimal) {
  return sprintf('%d:%02d', floor($decimal), ($decimal - floor($decimal)) * 60);
}


echo "<h2>CSV Time Entry</h2>";

if (!array_key_exists('action', $_POST)) {
echo <<<END
<h3>Example</h3>
<pre>
request_id,  work_description,                           date,       hours
10101,       "Telling clients to ""stop complaining!""", 2007-01-01, 3:00
10101,       "Bugs, testing and integration",            2007-01-02, 2:45
10102,       Annoying stuff,                             2007-01-03, 4.5
</pre>
<p>Dates must be entered in YYYY-MM-DD form. Times can be entered in HH:MM format or as a decimal number of hours.</p>
<p>The form also accepts tab-separated values and will use this format if unquoted tabs are found on the first line. In TSV form no quoting or advanced features are supported.</p>
END;
}

# Default header
$fields = array('request_id', 'work_description', 'date', 'hours');

# Extract records containing fields from 2d array of CSV data
if (array_key_exists('times', $_POST) && strlen($_POST['times']) > 0) {
  $entered = array();
  
  $data = parse_ctsv($_POST['times']);
  
  if (count($data) > 0) {
    $line = $data[0];
    if (count($line > 2) && !preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $line[2])) {
      $fields = array_shift($data);;
    }
  }
  
  $errors = array();
  $index = 0;
  
  foreach($data as $line) {
    $index ++;
    
    $in = null;
    $record = array();
    foreach ($fields as $field) {
      switch($field) {
        case "in":
          $in = parse_hhmm(array_shift($line));
          break;
        case "out":
          if ($in !== null) {
            $out = parse_hhmm(array_shift($line));
            if (!array_key_exists('hours', $record)) $record['hours'] = 0;
            while ($out < $in) $out += 12;
            $record['hours'] = $record['hours'] + $out - $in;
            $in = null;
          }
          break;
        default:
          $datum = array_shift($line);
          if ($datum !== null) $record[$field] = $datum;
          break;
      }
    }
    
    $required_keys = array("request_id", "work_description", "date", "hours");
    foreach ($required_keys as $rkey) {
      if (!array_key_exists($rkey, $record)) {
        $errors[] = "Required field $rkey missing on line $index.";
      }
    }
    
    if (!preg_match('/^\d\d\d\d-\d?\d-\d?\d$/', $record['date'])) {
      throw new Exception("Dates must be in YYYY-MM-DD format");
    }
    
    # Normalise date
    $parts = explode('-', $record['date']);
    if (strlen($parts[1]) < 2) $parts[1] = '0' . $parts[1];
    if (strlen($parts[2]) < 2) $parts[2] = '0' . $parts[2];
    $record['date'] = implode('-', $parts);
    
    $entered[] = $record;
  }
  
  if (count($errors) > 0) {
    foreach ($errors as $error) {
      echo "<p class=\"error\">$error</p>";
    }
  } elseif ($_POST['action'] == "Submit") {
    foreach($entered as $entry) {
    # Record timesheets
    if (strpos(':', $entry['hours']) != false) {
      list($hours, $minutes) = explode(':', $entry["hours"]);
      $duration = 60.0 * intval($hours) + intval($minutes);
    } else {
      $duration = floatval($entry['hours']);
    }
    add_time_timesheet(intval($entry['request_id']), $entry['work_description'], $entry['date'], floatval($entry['hours']));
    }
  } else {
    
    $dates = array();
    $activity_strings = array();
    $activities = array();
    $requests = array();
    $new = array();
    
    foreach ($entered as $entry) {
      $dates[] = $entry['date'];
      $request_id = intval($entry['request_id']);
      $work_description = $entry['work_description'];
      
      $requests[] = $request_id;
      $activity = array('request_id' => $request_id, 'work_description' => $work_description);
      $act_str = strval(request_id) . ':' . $work_description;
      if (array_search($act_str, $activity_strings) === false) {
        $activity_strings[] = $act_str;
        $activities[] = $activity;
      }
    }
    
    $activity_strings = null;
    
    $min_date = min($dates);
    $max_date = max($dates);
    
    $requests = array_unique($requests);
    
    $relevant_timesheets = $database->get_request_timesheet()
        ->filter_eq("work_by_id", intval($session->user_no))
        ->filter_in('request_id', $requests)
        ->filter("work_on::date >= date '$min_date'")
        ->filter("work_on::date <= date '$max_date'")
        ->filter("work_on > date '$min_date' - interval '1 day'")
        ->filter("work_on < date '$max_date' + interval '1 day'")
        ->sort('work_date', 'request_id', 'work_description');
    
    $existing = $relevant_timesheets->get_assoc_list('work_date', array('request_id', 'work_description', 'work_duration', 'work_on::date AS work_date'));
    
    echo '<table cellpadding="5" border="1">';
    echo '<tr><th>Date</th>';
    foreach ($activities as $activity) {
      echo '<th>';
      echo $activity['request_id'] . '<br />' . htmlspecialchars($activity['work_description']);
      echo '</th>';
    }
    echo '</tr>';
    
    $current_time = strtotime($min_date) - 2 * 86400;
    $end_time = strtotime($max_date) + 2 * 86400;
    while ($current_time <= $end_time) {
      $date = strftime("%Y-%m-%d", $current_time);
      $current_time += 86400;
      
      echo '<tr>';
      echo '<th>' . $date . '</th>';
      $new_today = array();
      foreach ($entered as $i => $entry) {
        if ($entry['date'] == $date) {
          $new_today[] = $entry;
          unset($entered[$i]);
        }
      }
      
      foreach ($activities as $activity) {
        echo '<td>';
        $new_exists = false;
        foreach ($new_today as $entry) {
          if (intval($entry['request_id']) == $activity['request_id'] && $entry['work_description'] == $activity['work_description']) {
            echo decimal_to_hhmm($entry['hours']);
            $new_exists = true;
            break;
          }
        }
        
        $existing_exists = false;
        if (array_key_exists($date, $existing)) {
          foreach ($existing[$date] as $entry) {
            if ($entry['request_id'] == $activity['request_id'] && $entry['work_description'] == $activity['work_description']) {
              echo ($new_exists ? ' +' : ' (');
              $duration = pg_interval_to_hours($entry['work_duration']);
              echo decimal_to_hhmm($duration);
              if (!$new_exists) echo ')';
              $existing_exists = true;
              break;
            }
          }
        }
        
        if (!$new_exists && !$existing_exists) echo '&nbsp;';
        echo '</td>';
      }
      echo '</tr>';
    }
    
    echo '</table>';
  }
}

$times_out = htmlspecialchars($_POST['times']);
echo <<<END
<form action="csv_entry.php" method="post">
  <textarea name="times" rows="15" cols="80">{$times_out}</textarea><br /><br />
  <input type="submit" name="action" value="Preview" />
END;
if (!$invalid && $_POST['action'] == 'Preview') echo '<input type="submit" name="action" value="Submit" />';
echo '</form>';


?>