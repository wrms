<?php

require_once('always.php');
require_once('WRMSDatabase.php');
require_once('caching.php');

require_once('api/registry.php');
require_once('api/api_util.php');
require_once('api/all.php');

function api_fail($msg, $status = 500) {
  header("Content-type: text/plain", true, $status);
  echo $msg;
  exit;
}

/**
* Given an API function tree and a path, resolve the function referenced.
*
* This returns the first leaf node on the path it comes to. Leaf nodes are
* any non-arrays or arrays containing an entry for key 0. To get there it
* uses each path component as the key to the current tree and expects to
* get back a new tree.
*/
function api_resolve_path($tree, $path) {
  while (count($path) > 0) {
    $sb = array_shift($path);
    
    # Check the path exists
    if (!array_key_exists($sb, $tree)) {
      return array(null, $path);
    }
    
    $tree = $tree[$sb];
    
    if (!is_array($tree) or array_key_exists(0, $tree)) {
      return array($tree, $path);
    }
  }
  return array(null, $path);
}

define("NETWORK_API", true);
set_error_handler('APIErrorHandler');
ini_set('error_prepend_string', 'ERROR - ');
ini_set('error_append_string', '');

$path = explode('/', $_SERVER['PATH_INFO']);
array_shift($path);

$branch = $api_methods[$_SERVER['REQUEST_METHOD']];
# Descend through registry until we find a match or fail.
list($fn, $rem_path) = api_resolve_path($branch, $path);
if ($fn !== null) {
  switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
      $data = $_GET;
      break;
    case 'POST':
      $body = @file_get_contents('php://input');
      $data = $_POST;
      if (strlen($body) > 0) {
        $json_data = json_decode($body, true);
        if ($json_data and is_array($json_data)) {
          $data = array_merge($data, $json_data);
        }
      }
      break;
  }
  
  # Decide how to handle the function call
  if (is_array($fn)) {
    # Calling conventions have been specified
    $method = $fn[0];
    unset($fn[0]);
    
    # Collect arguments
    $params = array();
    foreach ($fn as $p => $def) {
      # If no default was provided, the value becomes the parameter name
      if (is_numeric($p) && $def){
        $p = $def;
        $def = "_required";
      }
      
      $pparts = explode('::', $p, 2);
      if (count($pparts) > 1) {
        $p = $pparts[0];
      }
      
      $coerce = true;
      if (substr($p,0,1) == '_') {
        if ($p == '_data') {
          # All data as a parameter
          array_push($params, $data);
        } elseif ($p == '_path') {
          # Remaining path as a parameter
          $datum = $rem_path;
        } elseif (substr($p, 0, 6) == "_path_" and array_key_exists(intval(substr($p, 6)), $path)) {
          # Path component
          $datum = $rem_path[intval(substr($p, 6))];
        }
      } elseif (array_key_exists($p, $data)) {
        # In GET/POST data
        $datum =  $data[$p];
      } elseif ($def !== "_required") {
        # Fall back to default value
        $datum =  $def;
        $coerce = false;
      } else {
        api_fail("ERROR - Parameter $p is required but was not provided.", 200);
      }
      
      if ($coerce and count($pparts) > 1) {
        # A type has been provided - enforce it
        switch ($pparts[1]) {
          case "string":
            $datum = strval($datum);
            break;
          case "integer":
            $datum = intval($datum);
            break;
          case "float":
            $datum = floatval($datum);
            break;
          case "array":
            if (!is_array($datum)) {
              $datum = json_decode($datum, true);
              if (!is_array($datum)) api_fail("ERROR - Bad request format: Parameter $p must be an array.", 200);
            }
            break;
          default:
            api_fail("Method $p requires an invalid type.");
        }
      }
      
      $params[] = $datum;
    }
  } else {
    # Use default calling conventions
    $method = $fn;
    $params = array($data, $path);
  }
  
  try {
    # Evaluate function
    $val = call_user_func_array($method, $params);
  } catch (PermissionsException $e) {
    api_fail("Permission denied", 403);
  } catch (APIException $e) {
    api_fail($e->getMessage(), $e->status_code);
  } catch (Exception $e) {
    dbg_error_log("LOG-NAF", $e->getMessage());
    api_fail("failed");
  }
  
  # Process the returned value
  $requested_format = $data['format'];
  switch ($requested_format) {
    case 'csv':
      $output_start_callback = 'csv_print_header';
      $record_separator = "\n";
      $record_serialiser = 'csv_encode_line';
      $data_serialiser = 'csv_encode';
      $content_type = 'text/csv';
      break;
    default:
      $requested_format = 'json';
      $output_prefix = '[';
      $output_suffix = ']';
      $record_separator = ",";
      $record_serialiser = 'json_encode_safe';
      $data_serialiser = 'json_encode_safe';
      $content_type = 'application/json';
  }
  
  
  if (is_numeric($val) or is_string($val)) {
    if (is_numeric($val)) header("Content-type: text/plain");
    $etag = check_etag($val);
    print $val;
    $log_val = $val;
  } elseif (is_resource($val) and (get_resource_type($val) == 'stream' or get_resource_type($val) == 'file')) {
    $iter = 0;
    ob_start(); # Buffer output
    
    while (!feof($val)) {
      echo(fread($val, 8192));
      $iter += 1;
      if ($iter == 32) {
        ob_end_flush();
      }
    }
    
    fclose($val);
    
    if ($iter < 32) {
      // not streamed - check etag
      $buffer = ob_get_clean();
      $etag = check_etag($buffer);
      print $buffer;
    }
  } elseif ((!is_array($val)) and ($val instanceof Iterator or $val instanceof IteratorAggregate)) {
    header("Content-type: $content_type");
    $first = true;
    ob_start(); # Buffer output
    if ($output_prefix) print $output_prefix;
    $tag_content = true;
    
    foreach ($val as $item) {
      if ($first and $output_start_callback) call_user_func($output_start_callback, $item);
      if (((!$first) or $output_start_callback) and $record_separator) print $record_separator;
      $first = false;
      print call_user_func($record_serialiser, $item);
      
      if ($tag_content && ob_get_length() > 262144) {
        # Buffer exceeds 256k - stop buffering
        $log_val = ob_get();
        ob_end_flush();
        $tag_content = false;
      }
    }
    
    if ($output_suffix) print $output_suffix;
    
    $buffer = ob_get_clean();
    if ($tag_content) {
      # Output was short so we didn't stream it.
      $etag = check_etag($buffer);
      print $buffer;
      $log_val = $buffer;
    }
  } elseif ($val !== null) {
    header("Content-type: $content_type");
    $val = call_user_func($data_serialiser, $val);
    $etag = check_etag($val);
    print $val;
    $log_val = $val;
  } else {
    api_fail("The API function did not return a result.");
  }
  
  if ( isset($debuggroups['api_calls']) || isset($c->dbg['api_calls']) || isset($c->dbg['ALL']) ) {
      dbg_error_log('LOG-NAC', 'API call to function %s (parameters: %s) (%s) by user %d: result was %s',
        $_SERVER['PATH_INFO'],
        preg_replace('/^\[|\]$/', '', preg_replace('/\s+/', ' ', json_encode($params))),
        $_SERVER['REQUEST_METHOD'],
        $session->user_no,
        (strlen($log_val) > 50) ? (substr($log_val,0,47) . '...') : $log_val
      );
  }
} else {
  # Function does not exist for this request method.
  # Check whether it exists for _any_ request method.
  $api_all = call_user_func_array('array_merge_recursive', array_values($api_methods));
  list($ofn, $orp) = api_resolve_path($api_all, $path);
  if ($ofn !== null) {
    # This function exists for some other method
    api_fail("You cannot make {$_SERVER['REQUEST_METHOD']} requests to this function.", 405);
  } else {
    # This function just doesn't exist
    api_fail("That method does not exist", 404);
  }
}