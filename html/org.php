<?php
require_once("always.php");
require_once("authorisation-page.php");

  $session->LoginRequired();

require_once("maintenance-page.php");
require_once("Organisation.class");

param_to_global('org_code', 'int');
param_to_global('edit', 'int');
param_to_global('id', 'int');

$org = new Organisation($org_code);
if ( $org->org_code == 0 ) {
  unset( $org_code );
  $edit = 1;
  $title = "New Organisation";
}
else {
  $title = "$org->org_code - $org->org_name";
}
$show = 0;
$new = isset($edit) && intval($edit) && !isset($id);

if ( $org->AllowedTo("update") && isset($_POST['submit']) ) {
  if ( $org->Validate() ) {
    $org->Write();
    $org = new Organisation($org_code);
  }
}

  require_once("top-menu-bar.php");
  require_once("page-header.php");
  echo '<script language="JavaScript" src="/js/organisation.js"></script>' . "\n";
  echo $org->Render();
  include("page-footer.php");
