<?php
  require_once("always.php");
  require_once("authorisation-page.php");
  $session->LoginRequired("Admin");

param_to_global('action', '#[a-z]+#' );
param_to_global('table', '#[a-z0-9_]+#i' );
param_to_global('field', '#[a-z0-9_]+#i' );
param_to_global('stext');
param_to_global('maxdesc', 'int');

param_to_global('lookup_code');
param_to_global('lookup_seq');
param_to_global('lookup_desc');
param_to_global('lookup_misc');
param_to_global('old_lookup_code');

  include("lookwrite.php");

  $title = "$system_name - " . ucfirst("$table") . ", " . ucfirst("$field");
  include("page-header.php");
  include("lookhead.php");

  if ( $logged_on && "$error_loc$error_msg" == "" ) {
    $look_href = "$_SERVER[SCRIPT_NAME]?table=$table&field=$field&stext=".rawurlencode($stext);
    include("looksearch.php");
    include("looklist.php");
  }
?>
<h4>Hints</h4>
<p class=helptext>Each table which is presented as a drop-down list for the user should
have one code which has the lowest sequence, a blank &quot;code&quot; value and a description
resembling &quot;--- not selected ---&quot; to allow the user not entering a value.</p>
<p class=helptext>If possible keep the &quot;code&quot; value as short as possible and avoid
spaces. It will usually be interpreted by computers rather than people, but it will be sent
back and forth several times for each form submitted.</p>
<?php
  include("page-footer.php");
?>
