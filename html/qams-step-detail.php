<?php
/* ******************************************************************** */
/* CATALYST PHP Source Code                                             */
/* -------------------------------------------------------------------- */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with this program; if not, write to:                           */
/*   The Free Software Foundation, Inc., 59 Temple Place, Suite 330,    */
/*   Boston, MA  02111-1307  USA                                        */
/* -------------------------------------------------------------------- */
/*                                                                      */
/* Filename:    qams-step-detail.php                                    */
/* Author:      Paul Waite                                              */
/* Description: QAMS step editing and viewing page                      */
/*                                                                      */
/* ******************************************************************** */
require_once("always.php");
require_once("authorisation-page.php");

$session->LoginRequired();

require_once("maintenance-page.php");

$title = "QAMS Step Detail";

// -----------------------------------------------------------------------
include_once("qams-utils.php");
include_once("qams-project-defs.php");

// -----------------------------------------------------------------------
// FUNCTIONS
function ContentForm($ef, &$project, &$qastep, $view_history="no", $view_tsheets="no") {
  global $session;

  // Who can edit this - admin users plus assigned user
  $have_admin = $project->qa_process->have_admin;

  // Allow editing for:
  // admins        - always
  // assigned user - while step is fully unapproved
  $user_editmode = ( ($have_admin || $session->user_no == $qastep->responsible_usr)
                   && $qastep->overall_approval_status() == "" );

  $s = "";
  $s .= "<table cellspacing=\"2\" cellpadding=\"2\" width=\"100%\">\n";
  $s .= "<tr class=\"row0\">";

  // Project brief..
  $s .= "<tr class=\"row1\">";
  $s .= "<th width=\"30%\" class=\"prompt\"><b>Project:</b> </th>";
  $s .= "<td width=\"70%\">$project->brief</td>";
  $s .= "</tr>\n";

  // Step description..
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Step:</b> </th>";
  $s .= "<td>" . $qastep->qa_step_desc . "</td>";
  $s .= "</tr>\n";

  if ($qastep->qa_document_title != "") {
    $s .= "<tr class=\"row1\">";
    $s .= "<th class=\"prompt\"><b>Associated document:</b> </th>";
    $s .= "<td>" . $qastep->qa_document_title . "</td>";
    $s .= "</tr>\n";
    if ($qastep->qa_document_desc != "") {
      $s .= "<tr class=\"row1\">";
      $s .= "<th class=\"prompt\">&nbsp;</b> </th>";
      $s .= "<td>" . $qastep->qa_document_desc . "</td>";
      $s .= "</tr>\n";
    }
  }

  // Step notes..
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Notes for reviewers:</b> </th>";
  $s .= "<td>" . $qastep->qa_step_notes . "</td>";
  $s .= "</tr>\n";

  // Special notes..
  if ( isset($qastep->special_notes) ) {
    $s .= "<tr class=\"row1\">";
    $s .= "<th class=\"prompt\"><b>Special notes:</b> </th>";
    $s .= "<td>" . $qastep->special_notes . "</td>";
    $s .= "</tr>\n";
  }

  // Assignment..
  $s .= $ef->BreakLine("Assignment");
  if ($have_admin && $qastep->overall_approval_status() != "y") {
    // Person selector for assignment..
    $F  = "<select size=\"1\" name=\"new_assignment\">";
    $F .= "<option value=\"\">-- select a person --</option>\n";
    $extras = array(
        $project->project_manager => $project->project_manager_fullname,
        $project->qa_mentor => $project->qa_mentor_fullname
        );
    $allocatables = $project->allocated + $extras;
    foreach ($allocatables as $user_no => $fullname) {
      $F .= "<option value=\"$user_no\"";
      if ($user_no == $qastep->responsible_usr) {
        $F .= " selected";
      }
      $F .= ">$fullname</option>\n";
    }
    $F .= "</select>\n";

    $s .= "<tr class=\"row0\">";
    $s .= "<th class=\"prompt\"><b>Assigned to:</b> </th>";
    $s .= "<td>" . $F . "</td>";
    $s .= "</tr>\n";

    $assignment = $qastep->assigned();
    if ($assignment !== false) {
      $s .= "<tr class=\"row0\">";
      $s .= "<th>&nbsp;</th>";
      $s .= "<td>"
          . "<b>Assigned " . datetime_to_displaydate(QAMS_DATETIME, $assignment["datetime"]) . "</b>"
          . "</td>";
      $s .= "</tr>\n";
    }

    $s .= "<tr class=\"row0\">";
    $s .= "<th>&nbsp;</th>";
    $s .= "<td style=\"padding-right:30px\">"
            . "<p><i>Assigning a person to "
            . "a QA Step makes them responsible for delivering it. When you click UPDATE below "
            . "QAMS will send an e-mail to them containing all of the relevant details they need.</i></p>"
            . "</td>";
    $s .= "</tr>\n";

    // Empty text area for assignment notes..
    $F  = "<textarea name=\"assignment_covernotes\" style=\"width:400px;height:120px\">";
    $F .= "</textarea>";

    $s .= "<tr class=\"row0\">";
    $s .= "<th class=\"prompt\"><b>Assignment notes:</b> </th>";
    $s .= "<td>" . $F . "</td>";
    $s .= "</tr>\n";
    $s .= "<tr class=\"row0\">";
    $s .= "<th>&nbsp;</th>";
    $s .= "<td style=\"padding-right:30px\">"
            . "<p><i>If you changed the above "
            . "assignee, any notes entered here will be e-mailed to them along with the "
            . "usual details about the QA step.</i></p>"
            . "</td>";
    $s .= "</tr>\n";
  }
  else {
    $assigned = $qastep->responsible_fullname;
    $s .= "<tr class=\"row0\">";
    $s .= "<th class=\"prompt\"><b>Assigned to:</b> </th>";
    $s .= "<td>" . ($assigned != "" ? $assigned : "(nobody)") . "</td>";
    $s .= "</tr>\n";
  }

  // Overall status..
  $s .= $ef->BreakLine("Status");
  $status = qa_status_coloured($qastep->overall_approval_status());
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Current overall status:</b> </th>";
  $s .= "<td>" . ($status == "--" ? "Unapproved" : $status) . "</td>";
  $s .= "</tr>\n";

  // Required approvals list..
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Current approval statuses:</b> </th>";
  $s .= "<td>" . $qastep->render_approval_types($have_admin && $qastep->overall_approval_status() != "y") . "</td>";
  $s .= "</tr>\n";

  // Approvals being sought..
  $cnt = 0;
  $approvals = $qastep->approvals();
  $sought = array();
  foreach ($approvals as $ap_type_id => $approval) {
    if (($approval->approval_status == "p" || $approval->approval_status == "")
      && $approval->assigned_datetime != ""
    ) {

      $days  = "<span style=\"color:orange\" title=\"Days since approval was requested\">";
      $days .= "(" . $approval->since_assignment_days() . " days)";
      $days .= "</span>";

      $seek = $approval->qa_approval_type_desc . "&nbsp;sought";
      if ($approval->assigned_fullname != "") {
        $seek .= "&nbsp;from&nbsp;" . $approval->assigned_fullname;
      }
      $seek .= "&nbsp;" . datetime_to_displaydate(QAMS_DATETIME, $approval->assigned_datetime);
      $seek .= "&nbsp;$days";

      $sought[] = $seek;
    }
  }
  if (count($sought) == 0) {
    $appseek_status = "(none)";
  }
  else {
    $appseek_status = implode("<br>", $sought) . "<br>";
  }
  if ($have_admin && $qastep->overall_approval_status() != "y") {
    $href  = "/qams-request-approval.php";
    $href .= "?step_id=$qastep->qa_project_step_id";
    $label = "[Seek]";
    $title = "Seek an approval from someone for this QA step";
    $link  = "<a href=\"$href\" title=\"$title\">$label</a>";
  }
  else {
    $link = "";
  }
  $s .= "<tr class=\"row1\">";
  $s .= "<th class=\"prompt\"><b>Approvals being sought:</b> </th>";
  $s .= "<td>" . "$appseek_status $link" . "</td>";
  $s .= "</tr>\n";

  // Attachments
  $ef->EditMode = $user_editmode;
  $s .= $qastep->RenderAttachments($ef);
  $ef->EditMode = $have_admin;
  if ($qastep->overall_approval_status() == "y" && $qastep->attachment_count > 0) {
    $s .= "<tr class=\"row0\">";
    $s .= "<td colspan=\"2\" align=\"center\"><span style=\"color:green\">All attached files above have been approved as part of this QA Step.<br>No further changes are permitted.</span></td>";
    $s .= "</tr>\n";
  }

  // Timesheets
  if ($view_tsheets == "yes") {
    $ef->EditMode = true;
    $s .= $qastep->RenderTimesheets($ef);
    $ef->EditMode = $have_admin;
  }
  else {
    $s .= $ef->BreakLine("Work Done");
  }
  $s .= showhidelink("view_tsheets", "Timesheets", "the timesheeting for this QA step");


  // Approvals history..
  $s .= $ef->BreakLine("Approvals History");
  if ($view_history == "yes") {
    $s .= "<tr class=\"row0\">";
    $s .= "<td colspan=\"2\" align=\"center\">" . $qastep->render_approvals_history() . "</td>";
    $s .= "</tr>\n";
  }
  $s .= showhidelink("view_history", "Approvals History", "the approvals history for this QA step");

  $s .= "</table>\n";
  return $s;

} // ContentForm

/**
 * Show or hide a clickable link which will either show or hide a section of the
 * page for the user. Preserves existing portions of the query string on the URL.
 */
function showhidelink($flag_name, $label, $title) {
  global $REQUEST_URI, $$flag_name, $qastep;
  if (isset($$flag_name) && $$flag_name == "yes") {
    $href  = href_delparm($REQUEST_URI, $flag_name);
    $link = "<a href=\"$href\" title=\"Hide $title\">Hide $label</a>";
  }
  else {
    $href  = href_addparm($REQUEST_URI, $flag_name, "yes");
    $link = "<a href=\"$href\" title=\"Show $title\">Show $label</a>";
  }
  return "<tr>"
        . "<td colspan=\"2\" align=\"center\" style=\"text-align:center;vertical-align:bottom;height:20px\">"
        . $link
        . "</td>"
        . "</tr>\n"
        ;
}

// -----------------------------------------------------------------------------------------------
// MAIN CONTENT

// Must haves..
if (!isset($step_id)) {
  exit;
}

// Get the step
$qastep = new qa_project_step();
$qastep->get($step_id);

if ($qastep->valid) {
  // Project object to work with..
  $project = new qa_project($qastep->project_id);
  $have_admin = $project->qa_process->have_admin;
}
else {
  unset($qastep);
}

$s = "";
if (isset($qastep)) {

  // Viewing approvals history?
  if (!isset($view_history)) {
    $view_history = "no";
  }
  // Viewing time sheets?
  if (!isset($view_tsheets)) {
    $view_tsheets = "no";
  }

  // PROCESS UPDATE BUTTON SUBMIT
  if (isset($submit) && $submit == "Update") {
    if ($project->request_id > 0) {

      // APPROVALS
      if ($project->POSTprocess_approval_updates($qastep->qa_project_step_id)) {
        $project->get_project();
      }

      // ATTACHMENT UPLOAD
      if (isset($att_filename) && $att_filename != "") {
          $request_id = $qastep->request_id;
          $qastep->save_request();
      }

      // ASSIGNMENT
      if (isset($new_assignment) && $new_assignment != $qastep->responsible_usr) {

        // First, save the assignment
        $qastep->responsible_usr = $new_assignment;
        if ($new_assignment != "") {
          $qastep->responsible_datetime = date("Y-m-d H:i:s");
        }
        else {
          $qastep->responsible_datetime = "";
        }
        $qastep->save();

        // Save new phase to project record
        $q  = "UPDATE request_project SET";
        $q .= " qa_phase='$qastep->qa_phase'";
        $q .= " WHERE request_id=$qastep->project_id";
        $qry = new PgQuery($q);
        $ok = $qry->Exec("qams-step-detail.php::assignment");

        // Re-read to get new user name and email
        $qastep->get($qastep->qa_project_step_id);

        // If we are assigning someone, then let everyone know. Otherwise a null
        // assignment is de-assigning somebody, which we keep quiet about
        if ($new_assignment != "") {
          $qry = new PgQuery("SELECT email, fullname FROM usr WHERE user_no=$new_assignment");
          if ($qry->Exec("qams-step-detail.php::new_assignment") && $qry->rows > 0) {
            $row = $qry->Fetch();
            // Assignee email..
            $assignee_email = $row->email;
            $assignee_fullname = $row->fullname;
            $subject = "QAMS Assignment: $qastep->qa_step_desc [$project->system_id/$project->username]";
            $recipients = array($assignee_email => $assignee_fullname);

            // Assemble body for assignee..
            $s .= "<p>Congratulations! You have been chosen from thousands of eager applicants ";
            $s .= "to take ownership of this quality assurance step, and deliver it through ";
            $s .= "the approval process.</p>";

            $s .= "<p>The step you are charged with getting through approval is known as '" . $qastep->qa_step_desc . "'</p>";
            if ($qastep->qa_step_notes != "") {
              $s .= "<p>Some notes on what reviewers will be looking for when approving this step: ";
              $s .= $qastep->qa_step_notes . "</p>";
            }

            if ($qastep->qa_document_title != "") {
              $s .= "<p>For this step you have to produce a document, the '" . $qastep->qa_document_title . "'. ";
              if ($qastep->qa_document_desc != "") {
                $s .= $qastep->qa_document_desc;
              }
              $s .= "</p>";

              // Look up templates and examples..
              $qastep->get_documents();
              $docurls = array();
              $template = false;
              $example = false;
              if ($qastep->path_to_template != "") {
                $docurls[$qastep->path_to_template] = "Template for $qastep->qa_document_title";
                $template = true;
              }
              if ($qastep->path_to_example != "") {
                $docurls[$qastep->path_to_example] = "Example for $qastep->qa_document_title";
                $example = true;
              }
              if ($template || $example) {
                $s .= "<p>To help you in this task, QAMS has ";
                if ($template && $example) {
                  $s .= "some links to a template and an example document. The example is a filled-out ";
                  $s .= "and finished item just to look through for ideas on what to put in. ";
                  $s .= "The template is an empty document for you to use as a possible starting ";
                  $s .= "point.";
                }
                elseif ($template) {
                  $s .= "a link to a template document for you to use as a possible starting point.";
                }
                else {
                  $s .= "a link to an example document for you to use as a possible starting point. ";
                  $s .= "You will have to strip out content which is not applicable.";
                }
                $s .= "</p>";

                // Insert link(s)..
                foreach ($docurls as $href => $label) {
                  // Make sure it's a good clickable link..
                  if (!strstr("http", $href)) {
                    if (substr($href, 0, 1) != "/") {
                      $href = "/$href";
                    }
                    $href = $URL_PREFIX . $href;
                  }
                  $link  = "<a href=\"$href\" title=\"Click to download $label\">$href</a>";
                  $s .= "<p>Click the below link to download $label:<br>";
                  $s .= "&nbsp;&nbsp;$link";
                  $s .= "</p>";
                }
              }
            }

            // Step details link
            $s .= "<p>&nbsp; &nbsp;</p>";
            $s .= "<p><b><u>You must attach any documents or files that you produce, to the ";
            $s .= "QA Step Details Page here:</u></b><br>";
            $href = $URL_PREFIX . "/qams-step-detail.php?step_id=$qastep->qa_project_step_id";
            $desc = "Quality Assurance Step Details for: $qastep->qa_step_desc";
            $detailslink = "<a href=\"$href\">$desc</a>";
            $s .= "&nbsp;&nbsp;" . $detailslink . "</p>";
            $s .= "<p>&nbsp; &nbsp;</p>";

            // Covering notes..
            if ($assignment_covernotes != "") {
              $s .= "<p><b>Specific Notes:</b><br>";
              $s .= $assignment_covernotes . "</p>";
            }
            $project->QAMSNotifyEmail("QAMS Assignment", $s, $subject, $recipients);

            // Other emails to let everyone know what's going on..
            $recipients = $project->GetRecipients();
            if (isset($recipients[$assignee_email])) {
              unset($recipients[$assignee_email]);
            }
            $s  = "<p>$assignee_fullname has been assigned to the Quality Assurance Step ";
            $s .= "'$qastep->qa_step_desc' on this project.</p>";
            // Step details link..
            $s .= "<p>The details for this QA step are available here:<br>";
            $s .= "&nbsp;&nbsp;" . $detailslink . "</p>";
            $project->QAMSNotifyEmail("QAMS Activity Notice", $s, $subject, $recipients);
          }
        }
      } // assignment

    } // got request id
  } // process update button submit

  // PROCESS DE-ATTACHMENTS
  elseif (isset($action) && $action == "removeattachment") {
    if (isset($attachment_id)) {
      if ($have_admin || $session->user_no == $qastep->responsible_usr) {
        $qastep->RemoveAttachment($attachment_id);
      }
    }
  } // process de-attachments

  // Main content..
  require_once("top-menu-bar.php");
  require_once("page-header.php");

  $s = "";
  $user_editmode = ($have_admin || $session->user_no == $qastep->responsible_usr);
  $ef = new EntryForm('', $project, ($user_editmode ? 1 : 0));
  $ef->NoHelp();

  if ($user_editmode) {
    $s .= $ef->StartForm();
    $s .= $ef->HiddenField( "qa_action", "$qa_action" );
    if ( $project->request_id > 0 ) {
      $s .= $ef->HiddenField( "project_id", $project->request_id );
      $s .= $ef->HiddenField( "step_id", "$qastep->qa_project_step_id" );
    }
  }
  // Start main table..
  $s .= "<table width=\"100%\" class=\"data\" cellspacing=\"0\" cellpadding=\"0\">\n";

  $s .= $ef->BreakLine("Quality Assurance Step");
  $s .= "<tr><td height=\"15\" colspan=\"2\">&nbsp;</td></tr>";

  if ($project->project_manager_fullname != "") {
    $href = "/user.php?user_no=$project->project_manager";
    $link = "<a href=\"$href\">" . $project->project_manager_fullname . "</a>";
    $s .= "<tr><td colspan=\"2\"><b>Project Manager:</b> " . $link . "</td></tr>";
  }
  if ($project->qa_mentor_fullname != "") {
    $href = "/user.php?user_no=$project->qa_mentor";
    $link = "<a href=\"$href\">" . $project->qa_mentor_fullname . "</a>";
    $s .= "<tr><td colspan=\"2\"><b>QA Mentor:</b> " . $link . "</td></tr>";
  }

  $s .= "<tr><td height=\"15\" colspan=\"2\">&nbsp;</td></tr>";
  $s .= "<tr><td colspan=\"2\">" . ContentForm($ef, $project, $qastep, $view_history, $view_tsheets) . "</td></tr>";
  $s .= "</table>\n";

  if ($user_editmode) {
    $s .= $ef->SubmitButton( "submit", "Update" );
    $s .= $ef->EndForm();
  }
} // isset qastep

// -----------------------------------------------------------------------------
// ASSEMBLE CONTENT
if ($s == "") {
  $content = "<p>Nothing known about that QA step.</p>";
}
else {
  $content = $s;
}

// -----------------------------------------------------------------------------
// DELIVER..
echo $content;

include("page-footer.php");
?>
