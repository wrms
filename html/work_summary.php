<?php
include("always.php");
require_once("authorisation-page.php");

// require_once 'debuglib.php'; # include debuglib at the first line in your script.

$session->LoginRequired();
include("code-list.php");
include("user-list.php");
include("page-header.php");
include("system-list.php");
include("organisation-list.php");

$c->title = "$c->system_name Work Summary";

param_to_global('from_date', '#[0-9a-z:/ -]+#i');
param_to_global('to_date', '#[0-9a-z:/ -]+#i');
param_to_global('organisation_code', 'int');
param_to_global('system_id', 'int');
param_to_global('users', 'int');
param_to_global('detail_period', '#^(day|week|month|quarter|year)$#i');
param_to_global('subtotal_period', '#^(day|week|month|quarter|year)$#i');
param_to_global('break_columns', 'int' );
param_to_global('columns', '/[a-z0-9.# _]+/i' );
param_to_global('invoice_status', '#^((un)?invoiced|all)$#i' );


// Initialise variables.
if ( !isset($from_date) ) $from_date = date('01/m/y', mktime(0, 0, 0, date("m"), 0,  date("Y")));
if ( !isset($to_date) )   $to_date   = date('d/m/y',mktime(0, 0, 0, date("m"), 0,  date("Y")));
if ( !isset($break_columns) ) $break_columns = 1;

if ( is_member_of('Admin', 'Accounts' ) ) {
  $system_list = get_system_list( "", $system_id);
  $user_list   = get_user_list( "Support", "", $users );
}
else {
  $system_list = get_system_list( "CES", $system_id);
  $users=array($session->user_no  );
  $user_list="<option value=\"" . $session->user_no . "\">" . substr($session->fullname,0, 25) . " (" . $session->abbreviation . ")" . "</option>";
}

// Get full list of allowable organisations if none specified. F&*%ed if I know how you should really do this.
if (! isset($organisation_code)  || $organisation_code[0] == "" )  {
	$organisation_list = get_organisation_list();
  if ($organisation_code[0] == "") $organisation_list = "<option value=\"\" selected=\"selected\">(All)</option>" . $organisation_list;

	unset($organisation_code); // Clean it out

	$q = new PgQuery(SqlSelectOrganisations());
	$results = $q->Exec();

	while ($result = pg_fetch_array($results)) $organisation_code[] = $result["org_code"] ;
}
else {
	$organisation_code=array_filter($organisation_code,'intval');
	// $q = new PgQuery(SqlSelectOrganisations( $organisation_code ));
	// while ($result = pg_fetch_array($results)) $organisation_code[] = $result["org_code"] ;

	$organisation_list = get_organisation_list($organisation_code);

	$organisation_list = "<option value=\"\">(All)</option>" . $organisation_list;
}

/* later??
$request_types     = get_code_list( "request", "request_type", "$request_type" );
$quote_types       = get_code_list( "request_quote", "quote_type", "$quote_type" );
*/


$select_columns=array(""=>"","Organisation"=>"o.org_name","System"=>"ws.system_desc","WR#"=>"r.request_id","Work By"=>"rtu.fullname","Request Brief"=>"r.brief","Request Status"=>"lcs.lookup_desc","Request Type"=>"lct.lookup_desc");
// $select_columns=array(""=>"","Organisation"=>"o.org_name","System"=>"r.system_id","WR#"=>"r.request_id","Work By"=>"rtu.fullname","Request Brief"=>"r.brief","Request Status"=>"r.last_status");

$detail_periods=array("day"=>"Day","week"=>"Week","month"=>"Month","quarter"=>"Quarter","year"=>"Year");
if ( !array_key_exists($detail_period, $detail_periods) ) $detail_period = 'week';

$subtotal_periods=array("week"=>"Week","month"=>"Month","quarter"=>"Quarter","year"=>"Year");
if ( !array_key_exists($subtotal_period, $subtotal_periods) ) $subtotal_period = 'month';

$invoice_statuses=array("invoiced"=>"Invoiced","uninvoiced"=>"Uninvoiced","all"=>"All");
if ( !array_key_exists($invoice_status, $invoice_statuses) ) $invoice_status = 'all';

function buildSelect($name, $key_values, $current ) {
  $select="<select name=\"" .$name . "\" class=\"entry\" id=\"" . $name . "\">\n";
  foreach ($key_values as $key=>$value) {
    $select .=  "<option";
    if ($key==$current) $select .= " selected=\"selected\"";
    $select .= ">$key</option>\n";
  }
  $select.="</select>\n";
  return $select;
}

function buildRadio($name, $key_values, $current ) {
  $radio = '';
  foreach ($key_values as $key=>$value) {
    $radio.="<input type=\"radio\" name=\"" .$name . "\" " . " value=\"$key\"" . " class=\"entry\"";

    if ($key==$current) $radio .= " checked=\"true\"";
    $radio .= "><span class=\"entry\">$value</span>";
  }
  return $radio;
}

function insert_subtotal_rows( &$subtotal, $break_columns, $break_level, $prev_row ) {
	global $doc, $clone_tr, $xtbody;  // $first_quantity_column, $xtbody ;

	for ($i=$break_columns-1; $i>=$break_level; $i--) {
    if ( ! isset($subtotal[$i]) ) {
      $subtotal[$i] = array();
      continue;
    }

    $xtr  = $clone_tr->cloneNode(true);  // Copy this empty row so we don't have to reset all the styles etc.
		$xtr->setAttribute("class", "subtotal");
    $xtr  = $xtbody->appendChild($xtr);
		$xtds = $xtr->getElementsByTagname("td");

		// Insert total labels for the row.
    for ($k=0;$k<$i;$k++) $xtds->item($k)->appendChild($doc->createTextNode(htmlspecialchars($prev_row[$k])));
		if ($i>=0) $xtds->item($i)->appendChild($doc->createTextNode(htmlspecialchars($prev_row[$i]. " Total")));
		else $xtds->item(0)->appendChild($doc->createTextNode(htmlspecialchars("Total")));

    // Insert totals
    foreach ($subtotal[$i] as $key=>$value) {
      $xtds->item($key)->appendChild($doc->createTextNode(number_format($value,2)));
      unset ($subtotal[$i][$key]);
    }
	}
}

?>

<div class="row1" style="padding: 3px;">
<form  method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <h4>Select the entities you want to report on:</h4>
  <table>
    <tr>
      <td><label for="organisation_code" class="entry">Organisation</label></td>
      <td>
        <select class=entry name="organisation_code[]" id="organisation_code" size="6" multiple="true">
        	<?php echo $organisation_list; ?>
        </select>
      </td>
      <td class=entry><label for="system_id">System</label></td>
      <td class=entry>
        <select class=entry name="system_id[]" id="system_id" size="6" multiple="true">
          <option value=""<?php if (isset($system_id) && array_search("",$system_id) !== false) echo " selected=\"selected\""?>>(All)</option>
          <?php echo $system_list; ?>
        </select>
      </td>

      <td class=entry><label for="users">Work By</label></td>
      <td class=entry>
        <select class=entry name="users[]" id="users" size="6" multiple="true">
          <option value=""<?php if (isset($users) && array_search("",$users) !== false) echo " selected=\"selected\""?>>(All)</option>
          <?php echo $user_list; ?>
        </select>
      </td>
    </tr>
  </table>
  <p>
  	<label class="entry" for="break_columns">Subtotal Levels</label>
	<?php
            // Number of break columns to do subtotals by.-!>
            echo buildSelect("break_columns", array(0=>0,1=>1,2=>2,3=>3),$break_columns) ;
        ?>
	<label for="from_date">From Date</label>
    <input type=text name="from_date" id="from_date" class="entry" size=10<?php if ("$from_date" != "") echo " value=$from_date";?>>
    <label for="to_date">To Date</label>
    <input type=text name=to_date id="to_date" class="entry" size=10<?php if ("$to_date" != "") echo " value=$to_date";?>>
	<label for="invoiced">Invoice Status: </label>
    <?php  echo buildRadio("invoice_status",$invoice_statuses,$invoice_status);  ?>

   </p>
	<p>
    <label for="detail_period">Detail Period: </label>
    <?php  echo buildRadio("detail_period",$detail_periods,$detail_period);  ?>
    </p>
    <p>
    <label for="subtotal_period">Subtotal Period: </label>
    <?php  echo buildRadio("subtotal_period",$subtotal_periods,$subtotal_period);  ?>
    </p>


  <h4>Select the columns you want on the report:</h4>
  <p><?php
            echo buildSelect("columns[]", $select_columns, isset($columns[0]) ? $columns[0] : "Organisation") ;
            echo buildSelect("columns[]", $select_columns, isset($columns[1]) ? $columns[1] : "System") ;
            echo buildSelect("columns[]", $select_columns, $columns[2]) ;
            echo buildSelect("columns[]", $select_columns, $columns[3]) ;
            echo buildSelect("columns[]", $select_columns, $columns[4]) ;
            echo buildSelect("columns[]", $select_columns, $columns[5]) ;
        ?>
        <input type=submit value="RUN QUERY" alt=go name=submit class="submit">
   </p>
</form>

</div>

<p>
<?php

// if  ( !($session->AllowedTo("Admin") || $session->AllowedTo("Support"))) $users[]=$session->user_no;
// if ( isset($organisation_code) ) $organisation_code = intval($organisation_code);

if (isset($organisation_code)) $organisation_code=array_filter($organisation_code);
if (isset($system_id)) $system_id=array_filter($system_id);
if (isset($users)) $users=array_filter($users);

// If these variables are not set, don't display any results.
// && ( isset($organisation_code) || isset($system_id) || isset($users)) ) {

if (($_SERVER['REQUEST_METHOD'] == "POST") && is_array($organisation_code) ) {

  // Select all data

  $columns=array_filter($columns) ;

  if (! array_search("WR#",$columns)) $columns[]="WR#" ;  // Always needs to be selected so subselects will work

//******* Build query ************
// DISTINCT?? arrgh!  must fix!!!
  $query  = "SELECT DISTINCT";
  foreach ($columns as $column) $query .= "  $select_columns[$column]  AS \"$column\" ,\n";

	$query .= " ARRAY(SELECT substring(date_trunc('" . $subtotal_period . "',rt2.work_on)::text from 1 for 10) || '|subtotal' FROM request_timesheet rt2 WHERE rt2.request_id = r.request_id AND rt2.work_units = 'hours'";
    if (array_search("Work By",$columns)!==false) $query .= " AND rt2.work_by_id = rt.work_by_id" ;
    $query .= " AND date(rt2.work_on) >= '$from_date' AND date(rt2.work_on) <= '$to_date' ";
    if ( count($users) > 0 ) $query .= " AND rt2.work_by_id IN (" . implode(",",$users) . ") ";
    // Include invoiced or uninvoiced work
    if ( "$invoice_status" == "invoiced" ) $query .= " AND rt2.charged_details is not null ";
    elseif ( "$invoice_status" == "uninvoiced" ) $query .= " AND rt2.charged_details is null ";
    $query .= " GROUP BY date_trunc('" .$subtotal_period . "',rt2.work_on) \n";
    $query .= " ORDER BY date_trunc('" .$subtotal_period . "',rt2.work_on))";
    $query .= " AS subtotal_period_start,\n";

	//*** Get work for work request summarised by subtotal period as an array ***
    $query .= " ARRAY(SELECT SUM(rt2.work_quantity) FROM request_timesheet rt2 WHERE rt2.request_id = r.request_id AND rt2.work_units = 'hours'" ;
    if (array_search("Work By",$columns)!==false) $query .= " AND rt2.work_by_id = rt.work_by_id" ;
    $query .= " AND date(rt2.work_on) >= '$from_date' AND date(rt2.work_on) <= '$to_date' ";
    if ( count($users) > 0 ) $query .= " AND rt2.work_by_id IN (" . implode(",",$users) . ") ";
    // Include invoiced or uninvoiced work
    if ( "$invoice_status" == "invoiced" ) $query .= " AND rt2.charged_details is not null ";
    elseif ( "$invoice_status" == "uninvoiced" ) $query .= " AND rt2.charged_details is null ";

    $query .= " GROUP BY date_trunc('" . $subtotal_period . "',rt2.work_on) \n";
    $query .= " ORDER BY date_trunc('" . $subtotal_period . "',rt2.work_on))";
    $query .= " AS subtotal_period_quantity,\n";

	$query .= " ARRAY(SELECT substring(date_trunc('" . $subtotal_period . "',rt2.work_on)::text from 1 for 10) || '|' || substring(date_trunc('" . $detail_period . "',rt2.work_on)::text from 1 for 10) FROM request_timesheet rt2 WHERE rt2.request_id = r.request_id AND rt2.work_units = 'hours'";
    if (array_search("Work By",$columns)!==false) $query .= " AND rt2.work_by_id = rt.work_by_id" ;
    $query .= " AND date(rt2.work_on) >= '$from_date' AND date(rt2.work_on) <= '$to_date' ";
    if ( count($users) > 0 ) $query .= " AND rt2.work_by_id IN (" . implode(",",$users) . ") ";
    // Include invoiced or uninvoiced work
    if ( "$invoice_status" == "invoiced" ) $query .= " AND rt2.charged_details is not null ";
    elseif ( "$invoice_status" == "uninvoiced" ) $query .= " AND rt2.charged_details is null ";

     $query .= " GROUP BY date_trunc('" .$subtotal_period . "',rt2.work_on), date_trunc('" .$detail_period . "',rt2.work_on) \n";
     $query .= " ORDER BY date_trunc('" .$subtotal_period . "',rt2.work_on), date_trunc('" . $detail_period . "',rt2.work_on)) ";
    $query .= " AS period_start,\n";

	//*** Get sum of work for work request over complete time period ***
    $query .= " ARRAY(SELECT SUM(rt2.work_quantity) FROM request_timesheet rt2 WHERE rt2.request_id = r.request_id AND rt2.work_units = 'hours'" ;
    if (array_search("Work By",$columns)!==false) $query .= " AND rt2.work_by_id = rt.work_by_id" ;
    $query .= " AND date(rt2.work_on) >= '$from_date' AND date(rt2.work_on) <= '$to_date' ";
    if ( count($users) > 0 ) $query .= " AND rt2.work_by_id IN (" . implode(",",$users) . ") ";
    // Include invoiced or uninvoiced work
    if ( "$invoice_status" == "invoiced" ) $query .= " AND rt2.charged_details is not null ";
    elseif ( "$invoice_status" == "uninvoiced" ) $query .= " AND rt2.charged_details is null ";
	$query .= " GROUP BY date_trunc('" . $subtotal_period . "',rt2.work_on), date_trunc('" . $detail_period . "',rt2.work_on) \n";
    $query .= " ORDER BY date_trunc('" . $subtotal_period . "',rt2.work_on), date_trunc('" . $detail_period . "',rt2.work_on)) AS period_quantity\n";

    $query .= " FROM request r\n";
    $query .= " LEFT JOIN usr rqu ON rqu.user_no = r.requester_id\n";
    $query .= " LEFT JOIN work_system ws USING (system_id)\n";
    $query .= " LEFT JOIN organisation o USING (org_code)\n";
    $query .= " LEFT JOIN lookup_code lcs ON lcs.source_table = 'request' AND lcs.source_field = 'status_code' AND lcs.lookup_code = r.last_status\n";
 	$query .= " LEFT JOIN lookup_code lct ON lct.source_table = 'request' AND lct.source_field = 'request_type' AND lct.lookup_code = r.request_type::text\n";

    $query .= " LEFT OUTER JOIN request_timesheet rt  USING (request_id)\n";

    $query .= " LEFT OUTER JOIN usr rtu ON rtu.user_no = rt.work_by_id\n";

  // Build WHERE clause
  $where = '';
  if ( count($organisation_code) > 0 ) $where .= " AND o.org_code IN (" . implode(",",$organisation_code) . ") ";
  if ( count($system_id) > 0 ) $where .= " AND r.system_id IN ('" . implode("','",$system_id) . "') ";
  if ( count($users) > 0 ) $where .= " AND rt.work_by_id IN (" . implode(",",$users) . ") ";

  if ( "$from_date" != "" ) $where .= " AND rt.work_on >= '$from_date' ";
  if ( "$to_date"   != "" ) $where .= " AND rt.work_on <= '$to_date' ";

    // Include invoiced or uninvoiced work
    if ( "$invoice_status" == "invoiced" ) $where .= " AND rt.charged_details is not null ";
    elseif ( "$invoice_status" == "uninvoiced" ) $where .= " AND rt.charged_details is null ";


    if (isset($where)) $query .= " WHERE " . substr($where,4);

  // Build ORDER BY clause
  foreach($columns as $column) $by[]=$select_columns[$column];
  $query .= " ORDER BY " . implode(",",$by) . "\n";

    // Execute query
    $result = awm_pgexec( $dbconn, $query, "plan", false, 2 );

  // ****** Create xml doc to put query data into. ******
  $doc = new DOMDocument();
  $xtable = $doc->createElement("table");
  $xtable = $doc->appendChild($xtable);
  $xtable->setAttribute("style", "empty-cells: show; border-collapse: collapse; border: 1px solid $theme->colors[row1] ;");
  $xtable->setAttribute("border", "1");

  // Create column headers for selected fields.
  $xthead = $doc->createElement("thead");
  $xthead = $xtable->appendChild($xthead);

  $xthr = $doc->createElement("tr");
  $xthr = $xthead->appendChild($xthr);

  // Set up an empty row for cloning later on.
  $clone_tr = $doc->createElement("tr");

  // Put selected column headings into table header row.
  foreach ($columns as $column) {
    $xth = $doc->createElement("th");
    $xth->setAttribute("class", "cols");
    $xth->appendChild($doc->createTextNode(htmlspecialchars($column)));
    $xth = $xthr->appendChild($xth);

		// add cells to clone row at the same time.
    $clone_td = $doc->createElement("td");
    $clone_td = $clone_tr->appendChild($clone_td);
    $clone_td->setAttribute("class", "entry");
  }

	// Put periods into table header row.
  list($day, $month, $year) = explode('/', nice_date($from_date));
  $from_timestamp = mktime(0, 0, 0, $month, $day, $year);
  list($day, $month, $year) = explode('/', nice_date($to_date));
  $to_timestamp = mktime(0, 0, 0, $month, $day, $year);
  $j_from_date  = GregorianToJD( date("n",$from_timestamp),date("j",$from_timestamp),date("Y",$from_timestamp));
  $j_to_date    = GregorianToJD( date("n",$to_timestamp),date("j",$to_timestamp),date("Y",$to_timestamp));

	// Use SQL to calculate dates for the date range, detail period and subtotal period. date_trunc and UNION do it all nicely.
	$date_query = "SELECT substring(date_trunc('$subtotal_period',date '" . nice_date($from_date) . "')::text from 1 for 10 )";
	$date_query .= " || '|' || substring(date_trunc('$detail_period',date '" . nice_date($from_date) . "')::text from 1 for 10 )  ";
	for ($day = 1; $day <= $j_to_date - $j_from_date; $day++ ) {
		$date_query .= " UNION SELECT substring(date_trunc('$subtotal_period',date '" . nice_date($from_date) . "' + $day)::text from 1 for 10)";
	$date_query .= " || '|' || substring(date_trunc('$detail_period',date '" . nice_date($from_date) . "' + $day)::text from 1 for 10)";
	}
	$date_results = awm_pgexec( $dbconn, $date_query, "date", false, 2 );
	while ($date_result = pg_fetch_row($date_results)) $header_dates[] = $date_result[0];

	$date_query = "SELECT substring(date_trunc('$subtotal_period',date '" . nice_date($from_date) . "')::text from 1 for 10 ) ";
	for ($day = 1; $day <= $j_to_date - $j_from_date; $day++ ) {
		$date_query .= " UNION SELECT substring(date_trunc('$subtotal_period',date '" . nice_date($from_date) . "' + $day)::text from 1 for 10 )";
	}
	$date_results = awm_pgexec( $dbconn, $date_query, "date", false, 2 );
	while ($date_result = pg_fetch_row($date_results)) $header_dates[] = $date_result[0] . "|subtotal" ;

	$header_dates[] = "|total";

	sort($header_dates);

	// We now have the array of dates with sorted unique keys.

	// Create all the Table Header elements to put into the table header row.
	// At the same time create an empty results row for 'cloning' later on.
	foreach ($header_dates as $header_date ) {
		$clone_td = $doc->createElement("td");
		$clone_td->setAttribute("align","right");

		$header_key_array = explode("|",$header_date);
//		print_a($header_key_array[1]);

		if ($header_key_array[1] == "subtotal") {
			$xth = $doc->createElement("th", "Subtotal for $subtotal_period");
			$clone_td->setAttribute("class","entry period");
		}
		elseif ($header_key_array[1] == "total") {
			$xth = $doc->createElement("th", "Total");
			$clone_td->setAttribute("class","entry period");
		}
		else {
			// Format Table Headings according to detail period chosen

      // Apply different style for weekend columns.
      if ($detail_period == "day") {
        $xth = $doc->createElement("th", date("D d m y", strtotime($header_key_array[1])));

        if (date("D", strtotime($header_key_array[1])) == "Sat" || date("D", strtotime($header_key_array[1])) == "Sun")   $clone_td->setAttribute("class","entry row1");
        else $clone_td->setAttribute("class","entry");
      }
      else {
        $clone_td->setAttribute("class","entry");

				if ($detail_period == "year") $xth = $doc->createElement("th", date("Y", strtotime($header_key_array[1])));
				elseif ($detail_period == "quarter") $xth = $doc->createElement("th", date("M", strtotime($header_key_array[1])) . "-" . date("M Y",strtotime($header_key_array[1] . " + 2 month") ));
				elseif ($detail_period == "month") $xth = $doc->createElement("th", date("M Y", strtotime($header_key_array[1])));
				elseif ($detail_period == "week") $xth = $doc->createElement("th", date("D d m y", strtotime($header_key_array[1])));
			}
		}

		// $xth = $doc->createElement("th", $header_key_array[1] . date("Y",strtotime($header_key_array[1])) );
		$xth->setAttribute("class", "cols");
		$xth = $xthr->appendChild($xth);

    $clone_td = $clone_tr->appendChild($clone_td);
	}


  // Create table footer with empty row
  $xtfoot = $doc->createElement("tfoot");
  $xtfoot = $xtable->appendChild($xtfoot);

  $xtfr = $doc->createElement("tr");
  $xtfr = $xtfoot->appendChild($xtfr);

  // How many columns to skip over when starting any subtotal calculations.
  $first_quantity_column = count($columns);

  $xtbody = $doc->createElement("tbody");
  $xtable->appendChild($xtbody);

  if ( $result ) $result_array=pg_fetch_all($result);

  $subtotal = array();

  // Put result rows into xml doc.
  for ( $row_no=0; $result && $row_no < pg_NumRows($result); $row_no++ ) {
    $row = pg_fetch_array( $result, $row_no );

    if ($row_no == 0) $prev_row=$row;

    // Check for break. Insert subtotal rows if needed.
    for ($break_level = 0; $break_level<$break_columns; $break_level++){
      // Is this column different from same column in previous record?
      if ($row[$columns[$break_level]] != $prev_row[$break_level]){
        // Break found, insert subtotal rows, starting at innermost break.
        insert_subtotal_rows($subtotal, $break_columns, $break_level, $prev_row);
        break;
      }
    }

    // Insert result row into XML
    $xtr = $clone_tr->cloneNode(true);  // Copy this empty row so we don't have to reset all the styles etc.
    $xtr = $xtbody->appendChild($xtr);
    $xtds=$xtr->getElementsByTagname("td");

    // Set labels for requested columns
    for ($j=0;$j<count($columns);$j++) $xtds->item($j)->appendChild($doc->createTextNode(htmlspecialchars($row[$columns[$j]])));

    // Set quantities for date range.
    $period_start      = array_filter(explode(",", ereg_replace("[{\"-\"}]", "", $row["period_start"])));
    $period_quantity   = explode(",", ereg_replace("[{\"-\"}]", "", $row["period_quantity"]));

    $subtotal_period_start      = array_filter(explode(",", ereg_replace("[{\"-\"}]", "", $row["subtotal_period_start"])));
    $subtotal_period_quantity   = explode(",", ereg_replace("[{\"-\"}]", "", $row["subtotal_period_quantity"]));

    $period_start    = array_merge($period_start, $subtotal_period_start);
    $period_quantity = array_merge($period_quantity,$subtotal_period_quantity);

    $period_start[] = "|total";
    $period_quantity[] = array_sum($subtotal_period_quantity);

    for ($j=0;$j<count($period_start);$j++) {
      // Set quantity for date.
      $xtds->item(array_search($period_start[$j],$header_dates)+$first_quantity_column)->appendChild($doc->createTextNode(number_format($period_quantity[$j],2)));

      // Accumulate subtotals
      for ($break_level = -1; $break_level<$break_columns; $break_level++){  // -1 is for grand total.
        if ( !isset($subtotal[$break_level]) ) $subtotal[$break_level] = array();
        $column_no = array_search($period_start[$j],$header_dates)+$first_quantity_column;
        if ( !isset($subtotal[$break_level][$column_no]) ) $subtotal[$break_level][$column_no] = 0;
        $subtotal[$break_level][$column_no] += $period_quantity[$j];
      }
    }
    $prev_row=$row;
  }

    // Grand total (array row -1)
	if (pg_NumRows($result) > 0) insert_subtotal_rows($subtotal, $break_columns, -1, $prev_row);

  $xtd = $doc->createElement("td","Rows selected: " . pg_NumRows($result));
  $xtd->setAttribute("colspan", $xthr->getElementsByTagname("td")->length);
  $xtfr->appendChild($xtd);

  // Output the html table
  echo $doc->saveHTML();
}

include("page-footer.php");
