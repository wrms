<?php
  require("always.php");
  require_once("authorisation-page.php");
  $session->LoginRequired();
  require_once("maintenance-page.php");

  param_to_global('format', '#[a-z0-9]+#');
  param_to_global('style', '#[a-z0-9]+#');
  param_to_global('submit', '#[a-z0-9]+#i');
  param_to_global('form', '#[a-z0-9_]+#i', 'f' );

  $because = "";
  if ( isset($submit) && "$submit" <> "") {
    @include( "$form-valid.php" );
    if ( "$because" == "" ) @include("$form-action.php");
  }

  $title = "$system_name - " . ucfirst($form);
  $right_panel = false;
  require("top-menu-bar.php");
  require("page-header.php");

  if ( ! @include_once( "$form-form.php" ) ) {
    $c->messages[] = "Form '$form' not found";
  }

  include("page-footer.php");

