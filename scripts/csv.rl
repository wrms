# This is a CSV parser to be compiled using Ragel.
# It is intended to produce a PHP parser but since Ragel does
# not support PHP uses a translated version of the Java output.

# The driving loop has been translated to PHP and can be found in parse_csv.php.
# Changes not affecting the actions can be made by copying the Java array contents
# in to the equivalent PHP arrays.

# To compile the parser tables:      ragel -J -o csv.java csv.rl
# To visualise the state machine: ragel -Vp csv.rl | dot -Tps -o csv.ps

%%{
  
  machine csv;
  
  action start_line {
    print "\n";
  }
  
  action mark_field {
    $field_start = $p;
  }
  
  action mark_field_end {
    $field_end = $p;
  }
  
  action end_field {
    $cline[] = substr($data, $field_start, $field_end - $field_start);
  }
  
  separator = [,];
  str_char = ([^"] | '""');
  ns_space = space - [\n];
  quoted_string = '"' %mark_field (str_char**) %mark_field_end '"';
  field = (
      quoted_string
    | (
        (
          ns_space
          | (^('"' | separator | ns_space)) %mark_field_end
        )* >mark_field >mark_field_end
      )
    ) %end_field;
  line = (ns_space* <: (field (separator ns_space* <: field)*)? :> '\n') >start_line;
  
  main := line+;
}%%

%%{ write data; }%%

function parse_csv($s) {
  %%{ write init; }%%
  
  $p = 0;
  $pe = strlen($s) - 1;
  
  %%{ write exec; }%%
}
