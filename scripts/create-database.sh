#!/bin/sh
#
# Build the WRMS database
#

DBNAME="${1:-wrms}"
ADMINPW="${2}"

DBADIR="`dirname \"$0\"`/dba"

INSTALL_NOTE_FN="`mktemp -t tmp.XXXXXXXXXX`"

testawldir() {
  [ -f "${1}/dba/awl-tables.sql" ]
}

#
# Attempt to locate the AWL directory
AWLDIR="${DBADIR}/../../awl"
if ! testawldir "${AWLDIR}"; then
  AWLDIR="/usr/share/awl"
  if ! testawldir "${AWLDIR}"; then
    AWLDIR="/usr/local/share/awl"
    if ! testawldir "${AWLDIR}"; then
      echo "Unable to find AWL libraries"
      exit 1
    fi
  fi
fi

export AWL_DBAUSER=${DBNAME}_dba
export AWL_APPUSER=${DBNAME}_app

# Get the major version for PostgreSQL
export DBVERSION="`psql -qAt -c "SELECT version();" template1 | cut -f2 -d' ' | cut -f1-2 -d'.'`"

install_note() {
  cat >>"${INSTALL_NOTE_FN}"
}

db_users() {
  psql -qAt -c "SELECT usename FROM pg_user;" template1
}

create_db_user() {
  if ! db_users | grep "^${1}$" >/dev/null ; then
    psql -qAt -c "CREATE USER ${1} NOCREATEDB NOCREATEROLE;" template1
    cat <<EONOTE | install_note
*  You will need to edit the PostgreSQL pg_hba.conf to allow the
   ${2} to access the '${DBNAME}' database as the
   '${1}' database user.

EONOTE
  fi
}


create_plpgsql_language() {
  if ! psql ${DBA} -qAt -c "SELECT lanname FROM pg_language;" template1 | grep "^plpgsql$" >/dev/null; then
    createlang plpgsql "${DBNAME}"
  fi
}


try_db_user() {
  [ "XtestX`psql -U "${1}" -qAt -c "SELECT usename FROM pg_user;" template1 2>/dev/null`" != "XtestX" ]
}


create_db_user "${AWL_APPUSER}" "web application user"
create_db_user "${AWL_DBAUSER}" "systems administrator"


# FIXME: Need to check that the database was actually created.
if ! createdb --encoding UTF8 --template template0 --owner "${AWL_DBAUSER}" "${DBNAME}" ; then
  echo "Unable to create database"
  exit 1
fi

#
# Try a few alternatives for a database user or give up...
if try_db_user "${AWL_DBAUSER}" ; then
  export DBA="-U ${AWL_DBAUSER}"
else
  if try_db_user "postgres" ; then
    export DBA="-U postgres"
  else
    if try_db_user "${USER}" ; then
      export DBA=""
    else
      if try_db_user "${PGUSER}" ; then
        export DBA=""
      else
        cat <<EOFAILURE
* * * * ERROR * * * *
I cannot find a usable database user to construct the DAViCal database with, but
may have successfully created the ${DBNAME}_app and ${DBNAME}_dba users (I tried :-).

You should edit your pg_hba.conf file to give permissions to the ${DBNAME}_app and
${DBNAME}_dba users to access the database and run this script again.  If you still
continue to see this message then you will need to make sure you run the script
as a user with full permissions to access the local PostgreSQL database.

If your PostgreSQL database is non-standard then you will need to set the PGHOST,
PGPORT and/or PGCLUSTER environment variables before running this script again.

See:  http://wiki.davical.org/w/Install_Errors/No_Database_Rights

EOFAILURE
        exit 1
      fi
    fi
  fi
fi

create_plpgsql_language


# create the general user
createuser general -S -D -R

# Things matching this are not errors
FINE="(^CREATE |^GRANT|^BEGIN|^COMMIT|NOTICE: |WARNING:  sequence .* only supports )"

# Load the AWL DBA stuff first...
AWLDBA="/usr/share/awl/dba"
psql -q -f "${AWLDBA}/awl-tables.sql" "${DBNAME}" 2>&1 | egrep -v "${FINE}"
psql -q -f "${AWLDBA}/schema-management.sql" "${DBNAME}" 2>&1 | egrep -v "${FINE}"

# Now load the WRMS bits on top of that
sed -e "s/TO general/TO ${DBNAME}_app/g" "${DBADIR}/wrms.sql" | psql -q "${DBNAME}" 2>&1 | egrep -v "${FINE}"

psql -q -f "${DBADIR}/procedures.sql" "${DBNAME}"
psql -q -f "${DBADIR}/views.sql" "${DBNAME}"

sed -e "s/general/${DBNAME}_app/g" "${DBADIR}/grants.sql" | psql -q "${DBNAME}" 2>&1 | egrep -v "${FINE}"

psql -q -f "${DBADIR}/base-data.sql" "${DBNAME}" | egrep -v '(setval|-------|1 row|^ *[0-9]* *$)'

#
# We can override the admin password generation for regression testing predictability
if [ "${ADMINPW}" = "" ] ; then
  #
  # Generate a random administrative password.  If pwgen is available we'll use that,
  # otherwise try and hack something up using a few standard utilities
  ADMINPW="`pwgen -Bcny 2>/dev/null | tr \"\\\\\'\" '^='`"
fi

if [ "$ADMINPW" = "" ] ; then
  # OK.  They didn't supply one, and pwgen didn't work, so we hack something
  # together from /dev/random ...
  ADMINPW="`dd if=/dev/urandom bs=512 count=1 2>/dev/null | tr -c -d "a-km-zA-HJ-NP-Y0-9" | cut -c2-9`"
fi

if [ "$ADMINPW" = "" ] ; then
  # Right.  We're getting desperate now.  We'll have to use a default password
  # and hope that they change it to something more sensible.
  ADMINPW="please change this password"
fi

psql -q -c "UPDATE usr SET password = '**${ADMINPW}' WHERE user_no = 1;" "${DBNAME}"

#
# The supported locales are in a separate file to make them easier to upgrade
psql -q -f "${DBADIR}/supported_locales.sql" "${DBNAME}"


echo "NOTE"
echo "===="
cat "${INSTALL_NOTE_FN}"
rm "${INSTALL_NOTE_FN}"

cat <<FRIENDLY
*  The password for the 'admin' user has been set to '${ADMINPW}'"

Thanks for trying WRMS!  Check in /usr/share/doc/wrms/examples/ for
some configuration examples.  For help, visit #wrms on irc.oftc.net.

FRIENDLY

