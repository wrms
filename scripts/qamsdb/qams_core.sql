/*==============================================================*/
/* Database name:  qams_core                                    */
/* DBMS name:      PostgreSQL 7                                 */
/* Created on:     4/01/2008 11:19:33 a.m.                      */
/*==============================================================*/


CREATE SEQUENCE seq_qa_approval_id
increment 1
minvalue 1
maxvalue 2147483647
start 1
cache 1;

CREATE SEQUENCE seq_qa_approval_type_id
increment 1
minvalue 1
maxvalue 2147483647
start 1
cache 1;

CREATE SEQUENCE seq_qa_document_id
increment 1
minvalue 1
maxvalue 2147483647
start 1
cache 1;

CREATE SEQUENCE seq_qa_model_id
increment 1
minvalue 1
maxvalue 2147483647
start 1
cache 1;

CREATE SEQUENCE seq_qa_step_id
increment 1
minvalue 1
maxvalue 2147483647
start 1
cache 1;

/*==============================================================*/
/* Table: qa_approval                                           */
/*==============================================================*/
CREATE TABLE qa_approval (
qa_step_id           INT4                 not null,
qa_approval_type_id  INT4                 not null,
qa_approval_order    INT4                 not null DEFAULT 0,
CONSTRAINT PK_QA_APPROVAL PRIMARY KEY (qa_step_id, qa_approval_type_id)
);

COMMENT ON TABLE qa_approval is
'Contains the required Quality Assurance Approvals for given QA Step. A QA Approval is associated with a given QA Step. The contents of this table define which approvals records have to be created for QA Steps, when you create the QA instance records for a project.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_approval TO general;

/*==============================================================*/
/* Table: qa_approval_type                                      */
/*==============================================================*/
CREATE TABLE qa_approval_type (
qa_approval_type_id  INT4                 not null,
qa_approval_type_desc TEXT                 not null,
CONSTRAINT PK_QA_APPROVAL_TYPE PRIMARY KEY (qa_approval_type_id)
);

COMMENT ON TABLE qa_approval_type is
'Contains Quality Assurance Approval Types. A QA Approval Type represents a particular kind of approval required for a QA Step. Examples would be ''Internal Approval'', ''Peer Review'', ''Maintainer Approval'', or ''Client Approval''.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_approval_type TO general;

/*==============================================================*/
/* Table: qa_document                                           */
/*==============================================================*/
CREATE TABLE qa_document (
qa_document_id       INT4                 not null,
qa_document_title    TEXT                 not null,
qa_document_desc     TEXT                 null,
CONSTRAINT PK_QA_DOCUMENT PRIMARY KEY (qa_document_id)
);

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_document TO general;

/*==============================================================*/
/* Table: qa_model                                              */
/*==============================================================*/
CREATE TABLE qa_model (
qa_model_id          INT4                 not null,
qa_model_name        TEXT                 not null,
qa_model_desc        TEXT                 null,
qa_model_order       INT4                 not null DEFAULT 0,
CONSTRAINT PK_QA_MODEL PRIMARY KEY (qa_model_id)
);

COMMENT ON TABLE qa_model is
'Contains Quality Assurance models. A model is simply a hypothetical QA profile which defines the QA requirements
for that profile. It provides a kind of template for assigning default QA Steps etc. There are three simple models
which have been invented to begin with: Small, Medium and Large (referring to project size).';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_model TO general;

/*==============================================================*/
/* Table: qa_model_documents                                    */
/*==============================================================*/
CREATE TABLE qa_model_documents (
qa_model_id          INT4                 not null,
qa_document_id       INT4                 not null,
path_to_template     TEXT                 null,
path_to_example      TEXT                 null,
CONSTRAINT PK_QA_MODEL_DOCUMENTS PRIMARY KEY (qa_model_id, qa_document_id)
);

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_model_documents TO general;

/*==============================================================*/
/* Table: qa_model_step                                         */
/*==============================================================*/
CREATE TABLE qa_model_step (
qa_model_id          INT4                 not null,
qa_step_id           INT4                 not null,
CONSTRAINT PK_QA_MODEL_STEP PRIMARY KEY (qa_model_id, qa_step_id)
);

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_model_step TO general;

/*==============================================================*/
/* Table: qa_phase                                              */
/*==============================================================*/
CREATE TABLE qa_phase (
qa_phase             TEXT                 not null,
qa_phase_desc        TEXT                 null,
qa_phase_order       INT4                 not null DEFAULT 0,
CONSTRAINT PK_QA_PHASE PRIMARY KEY (qa_phase)
);

COMMENT ON TABLE qa_phase is
'Contains all of the Quality Assurance Phases available. A QA Phase is a logical grouping of QA Steps. Useful for display and reporting purposes.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_phase TO general;

/*==============================================================*/
/* Table: qa_project_approval                                   */
/*==============================================================*/
CREATE TABLE qa_project_approval (
qa_approval_id       SERIAL               not null,
qa_project_step_id   INT4                 not null,
qa_approval_type_id  INT4                 not null,
approval_status      TEXT                 null 
      CONSTRAINT CKC_APPROVAL_STATUS_QA_PROJE CHECK (approval_status is null or ( approval_status in ('p','y','n','s') )),
assigned_to_usr      INT4                 null,
assigned_datetime    TIMESTAMP            null,
approval_by_usr      INT4                 null,
approval_datetime    TIMESTAMP            null,
comment              TEXT                 null,
CONSTRAINT PK_QA_PROJECT_APPROVAL PRIMARY KEY (qa_approval_id)
);

COMMENT ON TABLE qa_project_approval is
'Contains Quality Assurance Approvals. A QA Approval record is associated with a given project QA Step and is only created when someone who is permitted to do so seeks to acquire an approval for the given QA Step. NB: QA Approval records are ''read-only'' to the QA application - they are an audit trail of approval activities. A QA user can create as many approvals for the same project, QA Step and Approval Type as they wish - they just keep adding to the approval audit trail.';

COMMENT ON COLUMN qa_project_approval.assigned_to_usr is
'The user that the approval process is assigned to - by the Project Manager. The approval is then approved by someone allocated to the project, or the project manager or the QA mentor (all are permitted to do it), however if the assigned user does not match the approved-by user, the approval has been explicitly over-ridden.';

COMMENT ON COLUMN qa_project_approval.approval_by_usr is
'The user who is updating this approval status.';

COMMENT ON COLUMN qa_project_approval.approval_datetime is
'Time and date that the approval status was changed to the current status.';

COMMENT ON COLUMN qa_project_approval.comment is
'Used to make brief comments on this approval.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_project_approval TO general;

/*==============================================================*/
/* Table: qa_project_step                                       */
/*==============================================================*/
CREATE TABLE qa_project_step (
qa_project_step_id   SERIAL               not null,
project_id           INT4                 not null,
request_id           INT4                 not null,
qa_step_id           INT4                 null,
qa_phase             TEXT                 not null,
qa_document_id       INT4                 null,
qa_step_desc         TEXT                 null,
qa_step_notes        TEXT                 null,
qa_step_order        INT4                 not null DEFAULT 0,
mandatory            BOOL                 not null DEFAULT false,
formal               BOOL                 not null DEFAULT true,
responsible_usr      INT4                 null,
responsible_datetime TIMESTAMP            null,
CONSTRAINT PK_QA_PROJECT_STEP PRIMARY KEY (qa_project_step_id)
);

COMMENT ON TABLE qa_project_step is
'The Project QA Step table contains the QA Steps defined for a given project. Each step is associated with a WRMS request record, which can be used to attach QA documents etc. and also for final signoff of the task once all the required approvals have been acquired.';

COMMENT ON COLUMN qa_project_step.project_id is
'This is the foreign key to the WRMS record for this QA Step. This WRMS record is used during the processing of this QA Step, for attaching docs, making notes etc. in the usual WRMS fashion.';

COMMENT ON COLUMN qa_project_step.responsible_usr is
'This user is assigned to the QA Step as the person responsible for delivering it and getting it approved. The person is selected from those allocated to the project.';

COMMENT ON COLUMN qa_project_step.responsible_datetime is
'The datetime that the user responsible for this step was assigned to it.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_project_step TO general;

/*==============================================================*/
/* Table: qa_project_step_approval                              */
/*==============================================================*/
CREATE TABLE qa_project_step_approval (
qa_project_step_id   INT4                 not null,
qa_approval_type_id  INT4                 not null,
last_approval_status TEXT                 null 
      CONSTRAINT CKC_LAST_APPROVAL_STA_QA_PROJE CHECK (last_approval_status is null or ( last_approval_status in ('p','y','n','s') )),
CONSTRAINT PK_QA_PROJECT_STEP_APPROVAL PRIMARY KEY (qa_project_step_id, qa_approval_type_id)
);

COMMENT ON TABLE qa_project_step_approval is
'This contains the list of approval types which are required for a given project QA step. It starts off as the default types as expressed by the ''qa_approval'' table, but may be subsequently modified by the project manager to add or subtract approval types. The presence of one of these records indicates that the given approval type is required for the project QA step. Note that this record also holds a denormalised value of the last approval status registered for this type.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_project_step_approval TO general;

/*==============================================================*/
/* Table: qa_step                                               */
/*==============================================================*/
CREATE TABLE qa_step (
qa_step_id           INT4                 not null,
qa_phase             TEXT                 not null,
qa_document_id       INT4                 null,
qa_step_desc         TEXT                 null,
qa_step_notes        TEXT                 null,
qa_step_order        INT4                 not null DEFAULT 0,
mandatory            BOOL                 not null DEFAULT false,
formal               BOOL                 not null DEFAULT true,
enabled              BOOL                 not null DEFAULT true,
CONSTRAINT PK_QA_STEP PRIMARY KEY (qa_step_id)
);

COMMENT ON TABLE qa_step is
'Contains all of the Quality Assurance Steps that are allowed in a project. A QA Step is a task which needs to be achieved as part of the QA process, and must be QA approved.';

GRANT SELECT,INSERT,DELETE,UPDATE ON qa_step TO general;

/*==============================================================*/
/* Table: request_project                                       */
/*==============================================================*/
CREATE TABLE request_project (
request_id           INT4                 not null,
project_manager      INT4                 null,
qa_mentor            INT4                 null,
qa_model_id          INT4                 null,
qa_phase             TEXT                 null,
CONSTRAINT PK_REQUEST_PROJECT PRIMARY KEY (request_id)
);

COMMENT ON TABLE request_project is
'Contains the master records for projects. Every project has a master WRMS record associated with it, and this table contains pointers to those.';

COMMENT ON COLUMN request_project.project_manager is
'The user who is the designated project manager for this project.';

COMMENT ON COLUMN request_project.qa_mentor is
'The user who is designated as the person to help out and guide the project team in quality assurance matters.';

COMMENT ON COLUMN request_project.qa_model_id is
'The initial choice by the project creator, of the model that the project is closest to in size.';

COMMENT ON COLUMN request_project.qa_phase is
'The current phase that the project is in. Updated whenever approval action takes place. Can be used as a means of high-level project progress viewing.';

GRANT SELECT,INSERT,DELETE,UPDATE ON request_project TO general;

ALTER TABLE qa_approval
   ADD CONSTRAINT fk_qa_approval_step FOREIGN KEY (qa_step_id)
      REFERENCES qa_step (qa_step_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_approval
   ADD CONSTRAINT fk_qa_approval_type FOREIGN KEY (qa_approval_type_id)
      REFERENCES qa_approval_type (qa_approval_type_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_model_documents
   ADD CONSTRAINT fk_documents_model FOREIGN KEY (qa_model_id)
      REFERENCES qa_model (qa_model_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_model_documents
   ADD CONSTRAINT fk_model_documents FOREIGN KEY (qa_document_id)
      REFERENCES qa_document (qa_document_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_model_step
   ADD CONSTRAINT fk_qa_model_step FOREIGN KEY (qa_step_id)
      REFERENCES qa_step (qa_step_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_model_step
   ADD CONSTRAINT fk_qa_model_step_model FOREIGN KEY (qa_model_id)
      REFERENCES qa_model (qa_model_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_step FOREIGN KEY (qa_project_step_id)
      REFERENCES qa_project_step (qa_project_step_id)
      ON DELETE cascade ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_type FOREIGN KEY (qa_approval_type_id)
      REFERENCES qa_approval_type (qa_approval_type_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_usr FOREIGN KEY (approval_by_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_behalf_of_usr FOREIGN KEY (assigned_to_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_proj_step_project FOREIGN KEY (project_id)
      REFERENCES request_project (request_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_document FOREIGN KEY (qa_document_id)
      REFERENCES qa_document (qa_document_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_phase FOREIGN KEY (qa_phase)
      REFERENCES qa_phase (qa_phase)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_responsible_usr FOREIGN KEY (responsible_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_stepref FOREIGN KEY (qa_step_id)
      REFERENCES qa_step (qa_step_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_wrms_id FOREIGN KEY (request_id)
      REFERENCES request (request_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step_approval
   ADD CONSTRAINT fk_qa_proj_step_app_step FOREIGN KEY (qa_approval_type_id)
      REFERENCES qa_approval_type (qa_approval_type_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step_approval
   ADD CONSTRAINT fk_qa_proj_step_app_type FOREIGN KEY (qa_project_step_id)
      REFERENCES qa_project_step (qa_project_step_id)
      ON DELETE cascade ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_step
   ADD CONSTRAINT fk_qa_step_document FOREIGN KEY (qa_document_id)
      REFERENCES qa_document (qa_document_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_step
   ADD CONSTRAINT fk_qa_step_phase FOREIGN KEY (qa_phase)
      REFERENCES qa_phase (qa_phase)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE request_project
   ADD CONSTRAINT fk_project_phase FOREIGN KEY (qa_phase)
      REFERENCES qa_phase (qa_phase)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE request_project
   ADD CONSTRAINT fk_qa_mentor_usr FOREIGN KEY (qa_mentor)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE request_project
   ADD CONSTRAINT fk_qa_project_mgr_usr FOREIGN KEY (project_manager)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE request_project
   ADD CONSTRAINT fk_req_proj_qa_model FOREIGN KEY (qa_model_id)
      REFERENCES qa_model (qa_model_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE request_project
   ADD CONSTRAINT fk_REQUEST__FK_REQ_PR_REQUEST FOREIGN KEY (request_id)
      REFERENCES request (request_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

