--
-- QAMS CORE DATA DUMP
--

--
-- Data for Name: qa_phase; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Plan', 'Plan', 40);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Build', 'Build', 50);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Integrate', 'Integrate', 60);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Test', 'Test', 70);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Install', 'Install', 80);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Concept', 'Concept', 10);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Define', 'Define', 20);
INSERT INTO qa_phase (qa_phase, qa_phase_desc, qa_phase_order) VALUES ('Design', 'Design', 30);


--
-- Data for Name: qa_approval_type; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_approval_type (qa_approval_type_id, qa_approval_type_desc) VALUES (2, 'Client Approval');
INSERT INTO qa_approval_type (qa_approval_type_id, qa_approval_type_desc) VALUES (1, 'Internal Peer Review');
INSERT INTO qa_approval_type (qa_approval_type_id, qa_approval_type_desc) VALUES (5, 'QA Auditor Signoff');
INSERT INTO qa_approval_type (qa_approval_type_id, qa_approval_type_desc) VALUES (3, 'Director Signoff');
INSERT INTO qa_approval_type (qa_approval_type_id, qa_approval_type_desc) VALUES (4, 'Sysadmin Signoff');


--
-- Data for Name: qa_model; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_model (qa_model_id, qa_model_name, qa_model_desc, qa_model_order) VALUES (3, 'Large', 'Quality assurance model for a medium-sized project of more than 10 person-weeks.', 30);
INSERT INTO qa_model (qa_model_id, qa_model_name, qa_model_desc, qa_model_order) VALUES (1, 'Small', 'Quality assurance model for small projects of approximately 1 - 3 person-weeks.', 10);
INSERT INTO qa_model (qa_model_id, qa_model_name, qa_model_desc, qa_model_order) VALUES (2, 'Medium', 'Quality assurance model for a medium-sized project of 4 - 10 person-weeks.', 20);


--
-- Data for Name: qa_document; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (13, 'Test Plan', 'A test plan document which has been produced for its associated QA Step according to the Verification and Validation Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (4, 'Functional Specification', 'The functional specification is a ''lite'' version of a full Requirements Specification. It should contain a description of all the known key functions at the early stages of the project, and is the springboard for the Requirements Specification document.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (6, 'Feasibility Study', 'A feasibility study should look thoroughly at technical, cost and other issues which might bear on a go/no-go decision on the project.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (7, 'Risk Analysis', 'A risk analysis should identify all risks which might affect the project and explore each one thoroughly.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (10, 'Maintenance Plan', 'The maintenance plan contains all of the details of how the system will be maintained once it has been installed/delivered.

Note that this document is a superset of the Maintainance Manual, and may even physically contain that manual as a sub-section/appendix.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (14, 'Disaster Recovery Plan', 'The disaster recovery plan contains details of the procedures and tools to be used in all of the anticipated critical failure modes of the system.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (29, 'Installation Plan', 'The installation plan should cover all of the steps required to install the system, an installation schedule, details of resources required, and any applicable costs.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (15, 'Maintenance Manual', 'The maintenance manual is a subset/off-shoot of the Maintenance Plan. It contains the nitty-gritty information required for System Administrators and Maintainers to maintain and/or trouble-shoot the system in production.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (20, 'Post-installation Report', 'The post-installation report is essentially a de-brief of the project at the end, describing what went right and what went wrong. Anything is fair game for comment, including QA processes, project management, development tools, environment etc.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (5, 'Preliminary Design Document', 'The preliminary design is a ''lite'' version of a detailed design document. It should express how the functional elements described in the Functional Specification could be built.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (2, 'Preliminary Project Plan', 'The preliminary project plan is the first stab at estimating resources required, and cost. It should also provide a proposed delivery schedule.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (11, 'Project Plan', 'The project plan uses all of the documentation on requirements, concept, functional spec. etc. in order to fully resource, cost, and schedule the project. It should provide a delivery schedule, and a work breakdown structure (eg. Gantt chart) of the project tasks.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (19, 'Training Material', 'Training material is generically any kind of documentation or media which is produced in order to facilitate education in the use of or maintenance of the system being built.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (16, 'User Manual', 'A user manual is documentation aimed at the end-users of the system being built, and is provided to teach them how to use it fully.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (1, 'Quality Assurance Plan', 'The quality assurance plan contains details of how Quality Assurance is going to be applied to the project. As a minimum, it provides a schedule of documents to be delivered, and a schedule of Quality Assurance Steps, which will require formal approvals.

Note: QAMS provides this facility built into its QA Step definition interface, therefore there is no separate paper-based QA Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (18, 'Integration Test Results', 'A document which contains test results produced according to the Verification and Validation Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (37, 'Site Acceptance Test Results', 'A document which contains test results produced according to the Verification and Validation Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (38, 'Customer Acceptance Test Results', 'A document which contains test results produced according to the Verification and Validation Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (39, 'Regression Test Results', 'A document which contains test results produced according to the Verification and Validation Plan.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (17, 'Code Review', 'A code review is a report on various aspects of the coding which has been done in the build phase. It should contain a section describing what is being reviewed, and how the review was done. As well as confirming the areas which pass muster, the review should report on any major problems, and make suggestions as to how these might be resolved.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (9, 'Detailed Design Document', 'The detailed design document takes the requirements and describes how these will be implemented. It should also describe how the system environment will be built, and provide details of all interfaces to any external systems that it relies on.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (8, 'Requirements Specification', 'The requirements specification is the repository of all of the key things that the client requires in the finished system. It also forms the basis of the project test plans.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (12, 'Verification and Validation Plan', 'A Verification and Validation plan is a document which describes the way the system has to be tested. This encompasses all testing, for eaxmple - unit testing, module testing, integration testing, site acceptance, customer acceptance etc. The VVP should contain the necessary detail as to content and procedures to be adopted for testing.');
INSERT INTO qa_document (qa_document_id, qa_document_title, qa_document_desc) VALUES (3, 'Concept Documentation', 'The concept documentation describes the initial requirements and aspirations of the project in a high level way. It compliments the preliminary functional requirements documentation by providing more context, and explaining in high level terms why the project is being undertaken.');


--
-- Data for Name: qa_model_documents; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 1, '', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 9, '/qadoc/templates/3_design/T_Design_SP.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 17, '', '/qadoc/examples/5_build/Code_Review_SP.sxw');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 8, '/qadoc/templates/2_define/T_SRS_SP.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (2, 8, '/qadoc/templates/2_define/T_SRS.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (3, 8, '/qadoc/templates/2_define/T_SRS.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 12, '/qadoc/templates/4_plan/T_VVP_SP.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (1, 3, '/qadoc/templates/1_concept/T_CONCEPT.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (2, 3, '/qadoc/templates/1_concept/T_CONCEPT.sxw', '');
INSERT INTO qa_model_documents (qa_model_id, qa_document_id, path_to_template, path_to_example) VALUES (3, 3, '/qadoc/templates/1_concept/T_CONCEPT.sxw', '');


--
-- Data for Name: qa_step; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (1, 'Concept', NULL, 'Quality Plan', 'Review the quality plan to ensure that it is appropriate for this project. Does it cover all the steps you think are necessary, given the project size? Conversely, does it perhaps contain too many?

Note: the Quality Plan is not a separate document, but instead is defined by the Quality Assurance Steps which are set up in QAMS for this project.', 10, true, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (2, 'Concept', 2, 'Preliminary Project Plan', 'Review preliminary project plan. The plan should contain solid estimates of resources required, and cost. It should also provide an initially proposed delivery schedule.', 30, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (3, 'Concept', 3, 'Concept Documentation', 'Does the concept documentation adequately describe what the project is all about and why it is being done?', 20, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (4, 'Concept', 4, 'Functional Specification', 'Does the functional specification contain a description of all the known key functions at this early stage, and is it good enough to support good project plan estimates? Is it an adequate start for a full requirements specification?', 40, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (7, 'Concept', 7, 'Risk Analysis', 'First of all, does the risk analysis fully identify all of the risks facing the project? Does it properly adress each of them?', 70, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (5, 'Concept', 5, 'Preliminary Design', 'Does the preliminiary design cover all of the functional requirements, as per the functional requirements specification, and would it provide a good platform for a detailed design?', 50, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (6, 'Concept', 6, 'Feasibility Study', 'Does the feasibility study cover all of the critical areas of concern, for example cost, performance, technical difficulties etc? Does it answer all of the questions that need to be answered in order to decide whether to go ahead with the project?', 60, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (8, 'Define', 8, 'Requirements Specification', 'Does the requirements specification cover all of the key functions that are required? Is it written in such a way that the main statements in it are testable? Would it support the writing of a detailed design document, and all of the test plans required by the project?', 80, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (9, 'Design', 9, 'Detailed Design', 'Does the design address all of the function points described in the requirements documentation? Does it provide developers with enough information to implement the system? Does it contain enough detail to enable the maintenance plan and maintenance manuals to be written?', 90, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (10, 'Plan', 10, 'Preliminary Maintenance Plan', 'Does the preliminary maintenance plan contain all of the relevant sections which need to be covered? Does it look like it will deliver enough information for maintainers to do their job when the system is running live?', 100, true, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (11, 'Plan', 11, 'Project Plan', 'Does the project plan contain sensible estimates for resources, and delivery schedule? Does it have enough detail? Does what the plan says tally with all the available information - eg. specifications, designs etc? In the end, the question is: is the plan achievable?', 110, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (12, 'Plan', 12, 'Validation & Verification Plan', 'Does the VVP cover all of the aspects of testing which the QA Plan says are to be undertaken in the project? Does it provide enough detail for test plan authors, and testers to produce documents and do testing respectively?', 120, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (13, 'Plan', 13, 'Acceptance Test Plan', 'Does the acceptance test plan fully cover all of the function points described in the requirements documentation? Can the test plan be given to a client, in the expectation they can go through all of the tests to verify that the system does what is required?', 130, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (14, 'Plan', 14, 'Disaster Recovery Plan', 'Does the DRP adequately cover all of the necessary failure modesof the system? Does it provide a clear and implementable plan for each one?', 140, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (15, 'Build', 15, 'Maintenance Manual', 'Does the maintenance manual cover everything that a maintainer needs to know? Eg. does it provide enough information on the system operating environment(s), application software etc? Does it provide an adequate trouble-shooting guide? Does it cover the process for system upgrades and change management?', 150, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (16, 'Build', 16, 'User Manual', 'Does the user manual cover everything about the system which a user needs to know to operate it? Is it well laid out, clear and easy to follow? Has user opinion on the manual been canvased, and has that feedback been actioned?', 160, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (17, 'Build', NULL, 'Project Tracking Review', 'Are the project tracking procedures adequate, and are they being followed?', 170, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (18, 'Build', 17, 'Code Review', 'Does the code meet the criteria and methodology described in the Code Review document and QA Plan?', 180, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (19, 'Build', NULL, 'Unit Testing', 'Does the unit testing in the project measure up to the plan described in the Verification & Validation Plan. Are the unit tests implemented well and are they ensuring requirements are met?', 190, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (20, 'Build', NULL, 'Module Testing', 'Does the module testing in the project measure up to the plan described in the Verification & Validation Plan. Are the module tests implemented well and are they ensuring requirements are met?', 200, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (21, 'Integrate', 18, 'Integration Testing', 'Do the integration test results show that the system has been fully integrated, and has ''passed''?', 210, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (23, 'Integrate', 37, 'Site Acceptance Testing', 'Do the site acceptance test results show that the system has ''passed''?', 230, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (24, 'Integrate', 29, 'Installation Plan', 'Does the installation plan cover all aspects of the installation of the system? Does it consider basics, such as timeframes, resourcing, costs?  Is it achievable?', 240, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (25, 'Test', 38, 'Customer Acceptance Testing', 'Do the customer acceptance test results show that the system has ''passed''?', 250, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (26, 'Install', NULL, 'Installed System', 'Is the system properly installed in its production environment?', 260, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (27, 'Install', 10, 'Maintenance Plan', 'Does the final maintenance plan contain everything that System Administrators and Maintainers need to keep the system running? Does it contain procedures for upgrade/change of the system?', 270, false, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (28, 'Install', 20, 'Post-installation Review', 'Was a sufficiently in-depth post-installation review held? Did the ensuing report cover all of the required topics, and provide an adequate de-brief of what went right and what went wrong?', 280, true, true, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (29, 'Build', 39, 'Regression Testing', 'Is the regression testing being implemented as defined in the Verification & Validation Plan? Are the unit tests implemented well and are they ensuring requirements are met?', 195, false, false, true);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (100, 'Concept', NULL, '(Custom Concept step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (101, 'Define', NULL, '(Custom Define step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (102, 'Design', NULL, '(Custom Design step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (103, 'Plan', NULL, '(Custom Plan step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (104, 'Build', NULL, '(Custom Build step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (105, 'Integrate', NULL, '(Custom Integrate step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (106, 'Test', NULL, '(Custom Test step)', '(replace with notes on this step)', 999, false, true, false);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_document_id, qa_step_desc, qa_step_notes, qa_step_order, mandatory, enabled, formal) VALUES (107, 'Install', NULL, '(Custom Install step)', '(replace with notes on this step)', 999, false, true, false);


--
-- Data for Name: qa_model_step; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 1);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 2);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 3);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 4);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 5);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 8);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 9);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 10);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 11);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 12);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 13);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 15);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 17);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 18);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 19);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 29);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 20);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 21);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 23);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 24);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 25);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 26);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 27);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (3, 28);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 1);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 2);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 3);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 9);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 15);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 27);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (1, 28);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 1);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 2);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 3);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 4);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 8);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 9);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 11);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 12);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 13);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 15);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 18);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 21);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 25);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 26);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 27);
INSERT INTO qa_model_step (qa_model_id, qa_step_id) VALUES (2, 28);


--
-- Data for Name: qa_approval; Type: TABLE DATA; Schema: public; Owner: paul
--

INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (3, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (3, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (2, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (2, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (4, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (4, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (5, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (6, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (7, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (8, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (8, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (9, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (10, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (11, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (11, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (12, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (12, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (13, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (13, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (14, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (14, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (15, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (16, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (16, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (18, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (21, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (26, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (27, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (28, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (19, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (1, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (1, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (23, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (23, 1, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (25, 2, 0);
INSERT INTO qa_approval (qa_step_id, qa_approval_type_id, qa_approval_order) VALUES (29, 1, 0);


--
-- SET SEQUENCES
--
SELECT pg_catalog.setval('qa_approval_type_qa_approval_type_id_seq', (SELECT MAX(qa_approval_type_id FROM qa_approval), true);
SELECT pg_catalog.setval('qa_document_qa_document_id_seq', (SELECT MAX(qa_document_id FROM qa_document), true);
SELECT pg_catalog.setval('qa_model_qa_model_id_seq', (SELECT MAX(qa_model_id) FROM qa_model), true);
SELECT pg_catalog.setval('qa_step_qa_step_id_seq', (SELECT MAX(qa_step_id) FROM qa_step), true);


--
-- QAMS CORE DATA END
--
