BEGIN;

SELECT check_wrms_revision(1,99,20);  -- Will fail if this revision doesn't exist, or a later one does
SELECT new_wrms_revision(1,99,21, 'Roti' );

-- Don't accept systems with a NULL description
ALTER TABLE work_system ALTER COLUMN system_desc SET NOT NULL;

-- Don't accept systems that have a blank system code or description
ALTER TABLE work_system ADD CONSTRAINT CKC_NON_BLANK_SYSTEM_CODE CHECK (length(system_code) > 0);
ALTER TABLE work_system ADD CONSTRAINT CKC_NON_BLANK_SYSTEM_DESC CHECK (length(system_desc) > 0);

COMMIT;
