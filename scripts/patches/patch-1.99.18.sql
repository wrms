BEGIN;

SELECT check_wrms_revision(1,99,17);  -- Will fail if this revision doesn't exist, or a later one does
SELECT new_wrms_revision(1,99,18, 'Flapjacks' );

ALTER TABLE work_system ADD COLUMN long_description TEXT;


-- QAMS ENHANCMENTS

-- Clearer step descriptions
UPDATE qa_step SET qa_step_desc='Quality Plan' where qa_step_id=1;
UPDATE qa_step SET qa_step_desc='Preliminary Project Plan' where qa_step_id=2;
UPDATE qa_step SET qa_step_desc='Concept Documentation' where qa_step_id=3;
UPDATE qa_step SET qa_step_desc='Functional Specification' where qa_step_id=4;
UPDATE qa_step SET qa_step_desc='Preliminary Design' where qa_step_id=5;
UPDATE qa_step SET qa_step_desc='Feasibility Study' where qa_step_id=6;
UPDATE qa_step SET qa_step_desc='Risk Analysis' where qa_step_id=7;
UPDATE qa_step SET qa_step_desc='Requirements Specification' where qa_step_id=8;
UPDATE qa_step SET qa_step_desc='Detailed Design' where qa_step_id=9;
UPDATE qa_step SET qa_step_desc='Preliminary Maintenance Plan' where qa_step_id=10;
UPDATE qa_step SET qa_step_desc='Project Plan' where qa_step_id=11;
UPDATE qa_step SET qa_step_desc='Validation & Verification Plan' where qa_step_id=12;
UPDATE qa_step SET qa_step_desc='Acceptance Test Plan' where qa_step_id=13;
UPDATE qa_step SET qa_step_desc='Disaster Recovery Plan' where qa_step_id=14;
UPDATE qa_step SET qa_step_desc='Maintenance Manual' where qa_step_id=15;
UPDATE qa_step SET qa_step_desc='User Manual' where qa_step_id=16;
UPDATE qa_step SET qa_step_desc='Project Tracking Review' where qa_step_id=17;
UPDATE qa_step SET qa_step_desc='Code Review' where qa_step_id=18;
UPDATE qa_step SET qa_step_desc='Unit Testing' where qa_step_id=19;
UPDATE qa_step SET qa_step_desc='Module Testing' where qa_step_id=20;
UPDATE qa_step SET qa_step_desc='Integration Testing' where qa_step_id=21;
UPDATE qa_step SET qa_step_desc='Site Acceptance Testing' where qa_step_id=23;
UPDATE qa_step SET qa_step_desc='Installation Plan' where qa_step_id=24;
UPDATE qa_step SET qa_step_desc='Customer Acceptance Testing' where qa_step_id=25;
UPDATE qa_step SET qa_step_desc='Installed System' where qa_step_id=26;
UPDATE qa_step SET qa_step_desc='Maintenance Plan' where qa_step_id=27;
UPDATE qa_step SET qa_step_desc='Post-installation Review' where qa_step_id=28;
UPDATE qa_step SET qa_step_desc='Regression Testing' where qa_step_id=29;

COMMIT;

BEGIN;

-- Additional QA Step table field
ALTER TABLE qa_step ADD COLUMN formal boolean;
ALTER TABLE qa_step ALTER COLUMN formal SET DEFAULT TRUE;
UPDATE qa_step SET formal=TRUE;

COMMIT;

BEGIN;

ALTER TABLE qa_step ALTER COLUMN formal SET NOT NULL;

-- Add non-formal qa_step records
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (100, 'Concept', '(Custom Concept step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (101, 'Define', '(Custom Define step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (102, 'Design', '(Custom Design step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (103, 'Plan', '(Custom Plan step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (104, 'Build', '(Custom Build step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (105, 'Integrate', '(Custom Integrate step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (106, 'Test', '(Custom Test step)', '(replace with notes on this step)', 999, FALSE);
INSERT INTO qa_step (qa_step_id, qa_phase, qa_step_desc, qa_step_notes, qa_step_order, formal) VALUES (107, 'Install', '(Custom Install step)', '(replace with notes on this step)', 999, FALSE);

COMMIT;

BEGIN;

-- NEW SCHEMA
-- Data will be transferred from the old tables to these, then the
-- old tables will be removed.

ALTER TABLE qa_project_step_approval RENAME TO qa_project_step_approval_old;
ALTER TABLE qa_project_approval RENAME TO qa_project_approval_old;
ALTER TABLE qa_project_step RENAME TO qa_project_step_old;

CREATE TABLE qa_project_step (
qa_project_step_id   SERIAL               not null,
project_id           INT4                 not null,
request_id           INT4                 not null,
qa_step_id           INT4                 null,
qa_phase             TEXT                 not null,
qa_document_id       INT4                 null,
qa_step_desc         TEXT                 null,
qa_step_notes        TEXT                 null,
qa_step_order        INT4                 not null DEFAULT 0,
mandatory            BOOL                 not null DEFAULT false,
formal               BOOL                 not null DEFAULT true,
responsible_usr      INT4                 null,
responsible_datetime TIMESTAMP            null,
CONSTRAINT PKEY_QA_PROJECT_STEP PRIMARY KEY (qa_project_step_id)
);

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_proj_step_project FOREIGN KEY (project_id)
      REFERENCES request_project (request_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_document FOREIGN KEY (qa_document_id)
      REFERENCES qa_document (qa_document_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_phase FOREIGN KEY (qa_phase)
      REFERENCES qa_phase (qa_phase)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_responsible_usr FOREIGN KEY (responsible_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_wrms_id FOREIGN KEY (request_id)
      REFERENCES request (request_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step
   ADD CONSTRAINT fk_qa_proj_step_stepref FOREIGN KEY (qa_step_id)
      REFERENCES qa_step (qa_step_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE qa_project_step_approval (
qa_project_step_id   INT4                 not null,
qa_approval_type_id  INT4                 not null,
last_approval_status TEXT                 null 
      CONSTRAINT CKC_LAST_APPROVAL_STA_QA_PROJE CHECK (last_approval_status is null or ( last_approval_status in ('p','y','n','s') )),
CONSTRAINT PKEY_QA_PROJECT_STEP_APPROVAL PRIMARY KEY (qa_project_step_id, qa_approval_type_id)
);

ALTER TABLE qa_project_step_approval
   ADD CONSTRAINT fk_qa_proj_step_app_type FOREIGN KEY (qa_project_step_id)
      REFERENCES qa_project_step (qa_project_step_id)
      ON DELETE cascade ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_step_approval
   ADD CONSTRAINT fk_qa_proj_step_app_step FOREIGN KEY (qa_approval_type_id)
      REFERENCES qa_approval_type (qa_approval_type_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;


CREATE TABLE qa_project_approval (
qa_approval_id       SERIAL               not null,
qa_project_step_id   INT4                 not null,
qa_approval_type_id  INT4                 not null,
approval_status      TEXT                 null 
      CONSTRAINT CKC_APPROVAL_STATUS_QA_PROJE CHECK (approval_status is null or ( approval_status in ('p','y','n','s') )),
assigned_to_usr      INT4                 null,
assigned_datetime    TIMESTAMP            null,
approval_by_usr      INT4                 null,
approval_datetime    TIMESTAMP            null,
comment              TEXT                 null,
CONSTRAINT PKEY_QA_PROJECT_APPROVAL PRIMARY KEY (qa_approval_id)
);

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_step FOREIGN KEY (qa_project_step_id)
      REFERENCES qa_project_step (qa_project_step_id)
      ON DELETE cascade ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_type FOREIGN KEY (qa_approval_type_id)
      REFERENCES qa_approval_type (qa_approval_type_id)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_approval_usr FOREIGN KEY (approval_by_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE qa_project_approval
   ADD CONSTRAINT fk_qa_proj_behalf_of_usr FOREIGN KEY (assigned_to_usr)
      REFERENCES usr (user_no)
      ON DELETE restrict ON UPDATE restrict
      DEFERRABLE INITIALLY DEFERRED;


COMMIT;
