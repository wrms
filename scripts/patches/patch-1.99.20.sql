BEGIN;

SELECT check_wrms_revision(1,99,19);  -- Will fail if this revision doesn't exist, or a later one does
SELECT new_wrms_revision(1,99,20, 'Pita' );

-- Create table to store permissions
CREATE TABLE permission (
  role_no INTEGER NOT NULL REFERENCES roles(role_no) ON DELETE CASCADE,
  action TEXT[] NOT NULL,
  access_test TEXT,
  negate_test BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (role_no, action)
);

CREATE INDEX permission_action_idx ON permission using btree(action);
GRANT SELECT ON permission TO general;

-- Base permissions
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (1, ARRAY['db'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (1, ARRAY['admin'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (2, ARRAY['db'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (3, ARRAY['db', 'request', 'create'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (3, ARRAY['db', 'request', 'update'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (7, ARRAY['db', 'favourite_timesheet'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (7, ARRAY['db', 'request'], NULL, TRUE);
INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (7, ARRAY['db', 'request_timesheet'], NULL, TRUE);

-- Permission checking and manipulation functions

CREATE OR REPLACE FUNCTION test_condition_function(fname TEXT, user_no INTEGER, action TEXT[]) RETURNS BOOLEAN AS '
  DECLARE
    result BOOLEAN;
  BEGIN
    -- Execute the function
    EXECUTE (''SELECT '' || quote_ident(fname) || ''('' || user_no::text  || '', '' || quote_literal(array_to_string(action, ''/'')) || '');'') INTO result;
    RETURN result;
  END;
' LANGUAGE 'plpgsql' STABLE;

CREATE OR REPLACE FUNCTION parent_permissions(perm TEXT[]) RETURNS SETOF TEXT[] AS '
  DECLARE
    result TEXT[];
  BEGIN
    FOR idx IN array_lower(perm, 1)..array_upper(perm, 1) LOOP
      result := perm[1:idx];
      RETURN NEXT result;
    END LOOP;
  END;
' LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION auth_set_permitted(rn INTEGER, act TEXT[]) RETURNS VOID AS '
  BEGIN
    PERFORM 1 FROM permission WHERE role_no = rn and action = act;
    IF FOUND THEN
      UPDATE permission SET access_test = NULL, negate_test = TRUE WHERE role_no = rn and action = act;
    ELSE
      INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (rn, act, NULL, TRUE);
    END IF;
  END;
' LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION auth_set_permitted(TEXT, TEXT[]) RETURNS VOID AS 'select auth_set_permitted((SELECT role_no FROM roles WHERE role_name = $1), $2);' LANGUAGE 'SQL' VOLATILE;

CREATE OR REPLACE FUNCTION auth_set_prohibited(rn INTEGER, act TEXT[]) RETURNS VOID AS '
  BEGIN
    PERFORM 1 FROM permission WHERE role_no = rn and action = act;
    IF FOUND THEN
      UPDATE permission SET access_test = NULL, negate_test = FALSE WHERE role_no = rn and action = act;
    ELSE
      INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (rn, act, NULL, FALSE);
    END IF;
  END;
' LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION auth_set_prohibited(TEXT, TEXT[]) RETURNS VOID AS 'select auth_set_prohibited((SELECT role_no FROM roles WHERE role_name = $1), $2);' LANGUAGE 'SQL' VOLATILE;

CREATE OR REPLACE FUNCTION auth_set_conditional(rn INTEGER, act TEXT[], testfn TEXT, negate BOOLEAN) RETURNS VOID AS '
  DECLARE
    int_type oid;
    text_type oid;
    arg_vec_text text;
    arg_vec oidvector;
    bool_type oid;
  BEGIN
    SELECT INTO int_type oid FROM pg_type WHERE typname = ''int4'';
    SELECT INTO text_type oid FROM pg_type WHERE typname = ''text'';
    SELECT INTO bool_type oid FROM pg_type WHERE typname = ''bool'';
    
    arg_vec_text := text(int_type) || '' '' || text(text_type);
    EXECUTE ''SELECT '''''' || arg_vec_text || ''''''::oidvector;'' INTO arg_vec;
    
    SELECT 1 from pg_proc WHERE proname = testfn AND prorettype = bool_type AND proargtypes = arg_vec;
    
    IF NOT FOUND THEN
      RAISE WARNING ''Warning: Could not find a function %(INT, TEXT)'', testfn;
    END IF;
    
    PERFORM 1 FROM permission WHERE role_no = rn and action = act;
    IF FOUND THEN
      UPDATE permission SET access_test = foid, negate_test = negate WHERE role_no = rn and action = act;
    ELSE
      INSERT INTO permission (role_no, action, access_test, negate_test) VALUES (rn, act, testfn, negate);
    END IF;
  END;
' LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION auth_set_conditional(TEXT, TEXT[], TEXT, BOOLEAN) RETURNS VOID AS 'select auth_set_conditional((SELECT role_no FROM roles WHERE role_name = $1), $2, $3, $4);' LANGUAGE 'SQL' VOLATILE;

CREATE OR REPLACE FUNCTION list_permitted_users(act TEXT[]) RETURNS SETOF usr AS '
  DECLARE
    role_perm RECORD;
    result RECORD;
    permitted BOOLEAN;
  BEGIN
    FOR role_perm IN SELECT role_no, max(action) AS closest FROM permission NATURAL JOIN roles WHERE action <= act GROUP BY role_no LOOP
      
      IF role_perm.closest IN (SELECT * FROM parent_permissions(act)) THEN
        -- Re-get the full record
        SELECT INTO role_perm * FROM permission WHERE role_no = role_perm.role_no AND action = role_perm.closest;
        
        IF role_perm.access_test IS NULL AND NOT role_perm.negate_test THEN
          CONTINUE;
        END IF;
        
        FOR result IN SELECT DISTINCT usr.* FROM usr JOIN role_member USING (user_no) WHERE role_member.role_no = role_perm.role_no LOOP
          IF role_perm.access_test IS NULL THEN
            permitted := FALSE;
          ELSE
            permitted := test_condition_function(role_perm.access_test, result.user_no, act);
          END IF;
          
          IF (permitted AND NOT role_perm.negate_test) OR (NOT permitted AND role_perm.negate_test) THEN
            RETURN NEXT result;
          END IF;
        END LOOP;
      END IF;
    END LOOP;
  END;
' LANGUAGE 'plpgsql' STABLE;

CREATE OR REPLACE FUNCTION user_can(uno INT, act TEXT[]) RETURNS BOOLEAN AS '
  DECLARE
    role_perm RECORD;
    permitted BOOLEAN;
  BEGIN
    FOR role_perm IN SELECT role_no, max(action) AS closest FROM permission NATURAL JOIN roles JOIN role_member USING (role_no) WHERE role_member.user_no = uno AND action <= act GROUP BY role_no LOOP
      
      IF role_perm.closest IN (SELECT * FROM parent_permissions(act)) THEN
        -- Re-get the full record
        SELECT INTO role_perm * FROM permission WHERE role_no = role_perm.role_no AND action = role_perm.closest;
        
        IF role_perm.access_test IS NULL AND NOT role_perm.negate_test THEN
          CONTINUE;
        END IF;
        
        IF role_perm.access_test IS NULL THEN
          permitted := FALSE;
        ELSE
          permitted := test_condition_function(role_perm.access_test, uno, act);
        END IF;
        
        IF (permitted AND NOT role_perm.negate_test) OR (NOT permitted AND role_perm.negate_test) THEN
          RETURN TRUE;
        END IF;
      END IF;
    END LOOP;
    RETURN FALSE;
  END;
' LANGUAGE 'plpgsql' STABLE;

CREATE OR REPLACE FUNCTION quote_doublequotes(s TEXT) RETURNS TEXT AS E'
  DECLARE
    val TEXT;
  BEGIN
    val := replace(s, E''\\\\'', E''\\\\\\\\'');
    val := replace(val, ''"'', E''\\\\"'');
    val := ''"'' || val || ''"'';
    return val;
  END;
' LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION user_role_permissions(uno INT) RETURNS TEXT AS '
  DECLARE
    json TEXT = ''{'';
    perm RECORD;
    last_rn TEXT = '''';
    first_perm BOOLEAN = FALSE;
    perm_value TEXT;
  BEGIN
    FOR perm IN SELECT role_name, action, access_test, negate_test FROM permission NATURAL JOIN roles JOIN role_member USING (role_no) WHERE role_member.user_no = uno ORDER BY role_name, action LOOP
      IF last_rn != perm.role_name THEN
        -- New role
        IF last_rn != '''' THEN
          json := json || ''},'';
        END IF;
        json := json || quote_doublequotes(perm.role_name) || '':{'';
        
        last_rn := perm.role_name;
        first_perm := TRUE;
      END IF;
      
      IF perm.access_test IS NOT NULL THEN
        IF perm.negate_test THEN
          perm_value := quote_doublequotes(''!'' || perm.access_test);
        ELSE
          perm_value := quote_doublequotes(perm.access_test);
        END IF;
      ELSE
        IF perm.negate_test THEN
          perm_value := ''true'';
        ELSE
          perm_value := ''false'';
        END IF;
      END IF;
      
      -- Add entry
      IF NOT first_perm THEN
        json := json || '','';
      END IF;
      first_perm := false;
      json := json || quote_doublequotes(array_to_string(perm.action, ''/'')) || '':'' || perm_value;
    END LOOP;
    IF last_rn != '''' THEN
      json := json || ''}'';
    END IF;
    json := json || ''}'';
    
    RETURN json;
  END;
' LANGUAGE 'plpgsql';

COMMIT;
