CREATE INDEX request_text_search_index ON request USING gin (tsvector_concat(to_tsvector('english'::regconfig, brief), to_tsvector('english'::regconfig, detailed)));
CREATE INDEX note_text_search_index ON request_note USING gin (to_tsvector('english'::regconfig, note_detail));
