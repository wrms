-- Patch for release 2.8
-- This adds extra fields to the requests, stores additional session
-- details and records a series of "favourite" timesheets for each user.

BEGIN;

SELECT check_wrms_revision(1,99,18);  -- Will fail if this revision doesn't exist, or a later one does
SELECT new_wrms_revision(1,99,19, 'Rewena Paraora' );

ALTER TABLE request ADD COLUMN invoice_to TEXT;

ALTER TABLE session ADD COLUMN acl TEXT DEFAULT '{}';
ALTER TABLE session ADD COLUMN unstructured_data TEXT;

CREATE TABLE favourite_timesheet (
  request_id INT4 REFERENCES request(request_id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  work_description TEXT NOT NULL,
  user_no INT4 REFERENCES usr(user_no) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  rank INT4 NOT NULL,
  PRIMARY KEY (user_no, request_id, work_description)
);
GRANT ALL ON favourite_timesheet TO general;

COMMIT;
