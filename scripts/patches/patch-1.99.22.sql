BEGIN;

SELECT check_wrms_revision(1,99,21);  -- Will fail if this revision doesn't exist, or a later one does
SELECT new_wrms_revision(1,99,22, 'Crumpet' );

-- Timesheet entries must take some time
ALTER TABLE request_timesheet ALTER COLUMN work_quantity SET NOT NULL;
ALTER TABLE request_timesheet ALTER COLUMN work_quantity SET DEFAULT 0.0;

COMMIT;
