#!/bin/bash

remote=repo
local=local
refs="master invoices"

# Forcefully set our branches to the local ones
git fetch $local +refs/heads/*:refs/heads/* &> /dev/null

# Fetch in remote branches
git fetch $remote +refs/heads/*:refs/remotes/${remote}/* &> /dev/null

# Merge remote in to local and push to both
for ref in $refs
do
  git checkout -f $ref &> /dev/null
  git clean -q -f
  git merge ${remote}/${ref} &> /dev/null
  if [ $? -ne 0 ]; then echo "Failed to merge ${ref}"; fi
  git push $local ${ref}:${ref} &> /dev/null
  git push $remote ${ref}:${ref} &> /dev/null
done

git push --tags $local &> /dev/null
git push --tags $remote &> /dev/null

git checkout -f master &> /dev/null # Return to master